//
//  AppDelegate.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 14.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)resetToInitialView:(BOOL)fromUserLogout;

@end

