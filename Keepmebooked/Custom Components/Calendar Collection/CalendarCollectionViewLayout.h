//
//  CalendarCollectionViewLayout.h
//  Calendar View
//
//  Created by Shelonin Dmitry on 08.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CCVCompletionLoadingBlock)(void);



static const CGFloat CCVNumberOfSecondsInDay = 60. * 60. * 24.;



static const CGFloat CCVLDefaultCellWidth = 64;
static const CGFloat CCVLDefaultCellHeight = 48;



@class CalendarCollectionViewLayout;
@class CalendarCollectionView;



@interface CCIndexPath : NSObject

/**
 - row ~ NSIndexPath::section
 - column ~ NSIndexPath::row
 */
@property (nonatomic, readonly) NSInteger row;
@property (nonatomic, readonly) NSInteger column;

- (instancetype)initWithRow:(NSInteger)row andColumm:(NSInteger)column;
- (NSIndexPath *)asIndexPath;

+ (instancetype)initWithIndexPath:(NSIndexPath *)indexPath;

@end



@interface CCObjectInfo : NSObject

@property  NSDate * beginDateTime;
@property NSTimeInterval duration;

@end



@protocol CalendarCollectionViewLayoutDataSource <NSObject>

@required
//without top fixed panel
- (NSInteger)numberOfRowsForCalendarCollectionView:(CalendarCollectionView *)cv;
/**
 @param row row from 0 to numberOfRows. Without top fixed panel and left fixed panel
 */
- (NSInteger)calendarCollectionView:(CalendarCollectionView *)cv numberOfObjectsInRow:(NSInteger)row;

- (CCObjectInfo *)calendarCollectionView:(CalendarCollectionView *)cv objectInfoForCellIndexPath:(CCIndexPath *)indexPath;

//design
- (UICollectionViewCell *)topLeftFixedCellForCalendarCollectionView:(CalendarCollectionView *)cv;
- (UICollectionViewCell *)calendarCollectionView:(CalendarCollectionView *)cv cellForTopFixedPanelWithDate:(NSDate *)date andColumnIndex:(NSInteger)column;
- (UICollectionViewCell *)calendarCollectionView:(CalendarCollectionView *)cv cellForLeftFixedPanelWithRow:(NSInteger)row;
- (UICollectionViewCell *)calendarCollectionView:(CalendarCollectionView *)cv cellForIndexPath:(CCIndexPath *)indexPath;

- (void)loadDataToEarlierForCalendarCollectionView:(CalendarCollectionView *)cv withCompletionBlock:(CCVCompletionLoadingBlock)completion;
- (void)loadDataToLaterForCalendarCollectionView:(CalendarCollectionView *)cv withCompletionBlock:(CCVCompletionLoadingBlock)completion;

@end

@protocol CalendarCollectionViewLayoutDelegate <NSObject>

- (void)calendarCollectionView:(CalendarCollectionView *)cv didSelectObjectAtIndexPath:(CCIndexPath *)indexPath;
- (void)calendarCollectionView:(CalendarCollectionView *)cv currentDate:(NSDate *)date;

@end



@interface CalendarCollectionViewLayout : UICollectionViewLayout

@property BOOL hasLeftFixedPanel;
@property BOOL hasTopFixedPanel;

@property (nonatomic) CGFloat topFixedPanelHeight;
@property (nonatomic) CGFloat topFixedPanelCellWidth;

@property (nonatomic) CGFloat leftFixedPanelWidth;
@property (nonatomic) CGFloat leftFixedPanelCellHeight;

@property (weak) id<CalendarCollectionViewLayoutDataSource> dataSource;


@property (strong, nonatomic) NSDate *earlierDate;
@property (strong, nonatomic) NSDate *laterDate;

//with top left cell
- (NSInteger)numberOfItemsInTopFixedPanel;
//day from zero
- (NSDate *)dateForDayNumber:(NSUInteger)day;
- (CGFloat)xOfDateCell:(NSDate *)date;
- (void)setContentOffsetToDate:(NSDate *)date animated:(BOOL)animated;

- (NSUInteger)numberOfDayForXPoint:(CGFloat)x;
- (NSDate *)currentDate;

@end
