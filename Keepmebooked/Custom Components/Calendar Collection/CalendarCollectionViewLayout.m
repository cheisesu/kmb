//
//  CalendarCollectionViewLayout.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 08.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CalendarCollectionViewLayout.h"


@implementation CCIndexPath

- (instancetype)initWithRow:(NSInteger)row andColumm:(NSInteger)column {
    if ((self = [CCIndexPath new])) {
        _row = row;
        _column = column;
    }
    
    return self;
}

- (NSIndexPath *)asIndexPath {
    return [NSIndexPath indexPathForRow:_column inSection:_row];
}

+ (instancetype)initWithIndexPath:(NSIndexPath *)indexPath {
    return [[CCIndexPath alloc] initWithRow:indexPath.section andColumm:indexPath.row];
}

@end

/*////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

@implementation CCObjectInfo
@end

/*////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

@interface CalendarCollectionViewLayout()
@property (nonatomic, assign) CGSize contentSize;

@property NSUInteger numberOfDaysInTopFixedPanel;
@end



@implementation CalendarCollectionViewLayout
{
    CGRect oldRect;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.earlierDate = nil;
        self.laterDate = nil;
        
        self.topFixedPanelHeight = CCVLDefaultCellHeight;
        self.topFixedPanelCellWidth = CCVLDefaultCellWidth;
        self.leftFixedPanelWidth = CCVLDefaultCellWidth;
        self.leftFixedPanelCellHeight = CCVLDefaultCellHeight;
        
        self.numberOfDaysInTopFixedPanel = self.numberOfDaysSince1970;
        
        [self initTopFixedPanel];
    }
    
    return self;
}

- (NSUInteger)dayNumberForDate:(NSDate *)date {
    NSTimeInterval interval = [date timeIntervalSince1970];
    NSUInteger numberOfDaysSince1970 = (interval - CCVNumberOfSecondsInDay) / CCVNumberOfSecondsInDay + 1;
    
    return numberOfDaysSince1970;
}

- (void)setContentOffsetToDate:(NSDate *)date animated:(BOOL)animated{
    NSUInteger day = [self dayNumberForDate:date];
    UICollectionViewLayoutAttributes *attribute = [self attributesForDayCell:day];
    
    CGPoint offset = CGPointMake(attribute.frame.origin.x, self.collectionView.contentOffset.y);
    offset.x -= self.leftFixedPanelWidth;
    if (offset.x < 0)
        offset.x = 0;
    
    [self.collectionView setContentOffset:offset animated:animated];
}

//day from zero
- (CGFloat)getXForDayCell:(NSUInteger)day {
    return _leftFixedPanelWidth + day * _topFixedPanelCellWidth;
}

//day from zero
- (NSDate *)dateForDayNumber:(NSUInteger)day {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:day * CCVNumberOfSecondsInDay];
    
    return date;
}

- (UICollectionViewLayoutAttributes *)attributesForTopLeftFixedCell {
    UICollectionViewLayoutAttributes *attribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    attribute.frame = CGRectMake(self.collectionView.contentOffset.x, self.collectionView.contentOffset.y, _leftFixedPanelWidth, _topFixedPanelHeight);
    attribute.zIndex = 3;
    
    return attribute;
}

//day from zero
- (UICollectionViewLayoutAttributes *)attributesForDayCell:(NSUInteger)day {
    //dayNumber + 1 because of day's cells are from 1st index
    CCIndexPath *ccIndexPath = [[CCIndexPath alloc] initWithRow:0 andColumm:day + 1];
    UICollectionViewLayoutAttributes *attribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[ccIndexPath asIndexPath]];
    attribute.frame = CGRectMake([self getXForDayCell:day], self.collectionView.contentOffset.y, _topFixedPanelCellWidth, _topFixedPanelHeight);
    attribute.zIndex = 2;
    
    return attribute;
}

//with top panel
- (CGFloat)getYForRow:(NSInteger)row {
    return _topFixedPanelHeight + (row - 1) * _leftFixedPanelCellHeight;
}

//row from zero
- (UICollectionViewLayoutAttributes *)attributesForLeftFixedCell:(NSInteger)row {
    CCIndexPath *ccIndexPath = [[CCIndexPath alloc] initWithRow:row + 1 andColumm:0];
    UICollectionViewLayoutAttributes *attribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[ccIndexPath asIndexPath]];
    attribute.frame = CGRectMake(self.collectionView.contentOffset.x, [self getYForRow:row + 1], _leftFixedPanelWidth, _leftFixedPanelCellHeight);
    attribute.zIndex = 1;
    
    CGSize contentSize = self.contentSize;
    contentSize.height = MAX(contentSize.height, attribute.frame.origin.y + attribute.frame.size.height);
    
    self.contentSize = contentSize;
    
    return attribute;
}

- (CGFloat)countWidthForDuration:(CGFloat)duration {
    CGFloat numberOfDays = duration / CCVNumberOfSecondsInDay;
    CGFloat width = numberOfDays * _topFixedPanelCellWidth;
    
    return width;
}

- (CGFloat)getXForObject:(CCObjectInfo *)objectInfo {
    CGFloat numberOfDays = [objectInfo.beginDateTime timeIntervalSince1970] / CCVNumberOfSecondsInDay;
    CGFloat x = _leftFixedPanelWidth + numberOfDays * _topFixedPanelCellWidth;
    
    return x;
}

//row from zero
- (UICollectionViewLayoutAttributes *)attributesForObjectCell:(CCIndexPath *)indexPath {
    CCObjectInfo *objectInfo = [self.dataSource calendarCollectionView:(CalendarCollectionView *)self.collectionView objectInfoForCellIndexPath:indexPath];
    CCIndexPath *ccIndexPath = [[CCIndexPath alloc] initWithRow:indexPath.row + 1 andColumm:indexPath.column + 1];
    UICollectionViewLayoutAttributes *attribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[ccIndexPath asIndexPath]];
    
    attribute.frame = CGRectMake([self getXForObject:objectInfo], [self getYForRow:indexPath.row + 1], [self countWidthForDuration:objectInfo.duration], _leftFixedPanelCellHeight);
    
    return attribute;
}

- (CGFloat)countWidthForInfinityDays {
    return _leftFixedPanelWidth + self.numberOfDaysInTopFixedPanel * _topFixedPanelCellWidth;
}

- (void)initTopFixedPanel {
    self.contentSize = CGSizeMake(self.countWidthForInfinityDays, self.topFixedPanelHeight);
}

#pragma mark - Collection view layout methods

- (void)prepareLayout {
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    if (!(oldRect.origin.x == newBounds.origin.x &&
          oldRect.origin.y == newBounds.origin.y &&
          oldRect.size.width == newBounds.size.width &&
          oldRect.size.height == newBounds.size.height)) {
        oldRect = newBounds;
        
        return YES;
    }
    
    return !NO;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    return [[self attributesForElementsInRect:rect] allObjects];
}

- (nullable UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CCIndexPath *ccIndexPath = [CCIndexPath initWithIndexPath:indexPath];
    if (ccIndexPath.row == 0) {
        if (ccIndexPath.column == 0) {
            return self.attributesForTopLeftFixedCell;
        }
        //column > 0 => need attribute for day
        //column - 1 because of attributesForDayCell needs day from zero
        UICollectionViewLayoutAttributes *attributes = [self attributesForDayCell:ccIndexPath.column - 1];
        
        return attributes;
    } else { //other rows
        if (ccIndexPath.column == 0) {
            return [self attributesForLeftFixedCell:ccIndexPath.row - 1];
        } else {
            return [self attributesForObjectCell:[[CCIndexPath alloc] initWithRow:ccIndexPath.row - 1 andColumm:ccIndexPath.column - 1]];
        }
    }
    
    return nil;
}

- (CGSize)collectionViewContentSize {
    return self.contentSize;
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    UICollectionViewLayoutAttributes *attribute = [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    attribute.alpha = 0.;
    
    return attribute;
}

#pragma mark - Pagination
- (CGFloat)pageHorizontalWidth {
    return self.collectionView.bounds.size.width - _leftFixedPanelWidth;
}

- (CGFloat)pageVerticalWidth {
    return self.collectionView.bounds.size.height - _topFixedPanelHeight;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
{
    //horizontal
    CGFloat rawHorizontalPageValue = self.collectionView.contentOffset.x / self.pageHorizontalWidth;
    CGFloat currentHorizontalPage = (velocity.x > 0.) ? floor(rawHorizontalPageValue) : ceil(rawHorizontalPageValue);
    CGFloat nextHorizontalPage = (velocity.x > 0.) ? ceil(rawHorizontalPageValue) : floor(rawHorizontalPageValue);
    
    BOOL pannedHorizLessThanAPage = fabs(1 + currentHorizontalPage - rawHorizontalPageValue) > 0.5;
    BOOL flickedHoriz = fabs(velocity.x) > self.flickVelocity;
    if (pannedHorizLessThanAPage && flickedHoriz) {
        proposedContentOffset.x = nextHorizontalPage * self.pageHorizontalWidth;
    } else {
        proposedContentOffset.x = round(rawHorizontalPageValue) * self.pageHorizontalWidth;
    }
    
    //vertical
    CGFloat rawVerticalPageValue = self.collectionView.contentOffset.y / self.pageVerticalWidth;
    CGFloat currentVerticalPage = (velocity.y > 0.) ? floor(rawVerticalPageValue) : ceil(rawVerticalPageValue);
    CGFloat nextVerticalPage = (velocity.y > 0.) ? ceil(rawVerticalPageValue) : floor(rawVerticalPageValue);
    
    BOOL pannedVertLessThanAPage = fabs(1 + currentVerticalPage - rawVerticalPageValue) > 0.5;
    BOOL flickedVert = fabs(velocity.y) > self.flickVelocity;
    if (pannedVertLessThanAPage && flickedVert) {
        proposedContentOffset.y = nextVerticalPage * self.pageVerticalWidth;
    } else {
        proposedContentOffset.y = round(rawVerticalPageValue) * self.pageVerticalWidth;
    }
    
    return proposedContentOffset;
}

- (CGFloat)flickVelocity {
    return 0.3;
}

#pragma mark - inherited methods

- (NSUInteger)numberOfDayForXPoint:(CGFloat)x {
    x -= self.leftFixedPanelWidth;
    
    NSUInteger numberOfDay = (x - self.topFixedPanelCellWidth) / self.topFixedPanelCellWidth + 1;
    numberOfDay = MAX(0, numberOfDay);
    numberOfDay = MIN(self.numberOfDaysInTopFixedPanel - 1, numberOfDay);
    return numberOfDay;
}

- (NSSet *)attributesForElementsInRect:(CGRect)rect {
    NSMutableSet *result = [NSMutableSet new];
    
    [result addObject:self.attributesForTopLeftFixedCell];
    NSUInteger firstDay = [self numberOfDayForXPoint:rect.origin.x];
    NSUInteger lastDay = [self numberOfDayForXPoint:rect.origin.x + rect.size.width];
    for (NSUInteger day = firstDay; day <= lastDay; day++) {
        UICollectionViewLayoutAttributes *attribute = [self attributesForDayCell:day];
        [result addObject:attribute];
    }
    
    NSInteger numberOfRows = [self.dataSource numberOfRowsForCalendarCollectionView:(CalendarCollectionView *)self.collectionView];
    for (NSInteger row = 0; row < numberOfRows; row++) {
        UICollectionViewLayoutAttributes *attribute = [self attributesForLeftFixedCell:row];
        if (CGRectIntersectsRect(rect, attribute.frame) || CGRectContainsRect(rect, attribute.frame)) {
            [result addObject:attribute];
        }
    }
    
    for (NSInteger row = 0; row < numberOfRows; row++) {
        NSInteger numberOfObjectsInRow = [self.dataSource calendarCollectionView:(CalendarCollectionView *)self.collectionView numberOfObjectsInRow:row];
        
        for (NSInteger object = 0; object < numberOfObjectsInRow; object++) {
            CCIndexPath *ccIndexPath = [[CCIndexPath alloc] initWithRow:row andColumm:object];
            UICollectionViewLayoutAttributes *attribute = [self attributesForObjectCell:ccIndexPath];
            if (CGRectIntersectsRect(rect, attribute.frame) || CGRectContainsRect(rect, attribute.frame)) {
                [result addObject:attribute];
            }
        }
    }
    
    return result;
}

- (CGFloat)xOfDateCell:(NSDate *)date {
    NSUInteger dayNaumber = [self dayNumberForDate:date];
    CGFloat x = [self getXForDayCell:dayNaumber];
    
    return x;
}

- (NSInteger)numberOfItemsInTopFixedPanel {
    return self.numberOfDaysInTopFixedPanel + 1;
}

//must call only when first init
- (NSUInteger)numberOfDaysSince1970 {
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    //count number of days
    NSUInteger numberOfDaysSince1970 = ((interval - 1.) / CCVNumberOfSecondsInDay + 1.);
    numberOfDaysSince1970 *= 2;
    
    return numberOfDaysSince1970;
}

- (NSDate *)currentDate {
    //counting current offset
    CGPoint offset = self.collectionView.contentOffset;
    offset.x += self.leftFixedPanelWidth;
    NSInteger dayNumber = [self numberOfDayForXPoint:offset.x];
    //getting the date
    NSDate *date = [self dateForDayNumber:dayNumber];
    
    return date;
}

#pragma mark - Setters

- (void)setLeftFixedPanelWidth:(CGFloat)leftFixedPanelWidth {
    if (_leftFixedPanelWidth != leftFixedPanelWidth) {
        _leftFixedPanelWidth = leftFixedPanelWidth;
        self.contentSize = CGSizeMake(self.countWidthForInfinityDays, self.topFixedPanelHeight);
    }
}

- (void)setTopFixedPanelHeight:(CGFloat)topFixedPanelHeight {
    if (_topFixedPanelHeight != topFixedPanelHeight) {
        _topFixedPanelHeight = topFixedPanelHeight;
        self.contentSize = CGSizeMake(self.countWidthForInfinityDays, self.topFixedPanelHeight);
    }
}

- (void)setTopFixedPanelCellWidth:(CGFloat)topFixedPanelCellWidth {
    if (_topFixedPanelCellWidth != topFixedPanelCellWidth) {
        _topFixedPanelCellWidth = topFixedPanelCellWidth;
        self.contentSize = CGSizeMake(self.countWidthForInfinityDays, self.topFixedPanelHeight);
    }
}

- (void)setLeftFixedPanelCellHeight:(CGFloat)leftFixedPanelCellHeight {
    if (_leftFixedPanelCellHeight != leftFixedPanelCellHeight) {
        _leftFixedPanelCellHeight = leftFixedPanelCellHeight;
        self.contentSize = CGSizeMake(self.countWidthForInfinityDays, self.topFixedPanelHeight);
    }
}

- (void)setContentSize:(CGSize)contentSize {
    _contentSize = contentSize;
    [self.collectionView setContentSize:contentSize];
}

@end