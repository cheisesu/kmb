//
//  CalendarCollectionView.h
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarCollectionViewLayout.h"

IB_DESIGNABLE
@interface CalendarCollectionView : UICollectionView

@property (weak) IBOutlet id<CalendarCollectionViewLayoutDataSource> calendarDataSource;
@property (weak) IBOutlet id<CalendarCollectionViewLayoutDelegate> calendarDelegate;

@property IBInspectable UIColor *linesColor;

@property (nonatomic) IBInspectable CGFloat topFixedPanelHeight;
@property (nonatomic) IBInspectable CGFloat topFixedPanelCellWidth;

@property (nonatomic) IBInspectable CGFloat leftFixedPanelWidth;
@property (nonatomic) IBInspectable CGFloat leftFixedPanelCellHeight;

@end
