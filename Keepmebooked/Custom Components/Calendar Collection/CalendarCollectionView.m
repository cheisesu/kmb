//
//  CalendarCollectionView.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CalendarCollectionView.h"
#import "CalendarCollectionViewLayout.h"

@implementation CalendarCollectionView

- (instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {    
    [self.backgroundColor setFill];
    UIRectFill(rect);
    
    [self.linesColor setStroke];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    NSArray *indexPaths = [self indexPathsForVisibleItems];
    CGFloat y = 0;
    CGFloat w = 0;
    CGFloat x = -1;
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)self.collectionViewLayout;
    for (NSIndexPath *indexPath in indexPaths) {
        UICollectionViewLayoutAttributes *attribute = [self layoutAttributesForItemAtIndexPath:indexPath];
        
        if (attribute.zIndex == 2) {
            if (attribute.frame.origin.x < layout.leftFixedPanelWidth + self.contentOffset.x - 1 || attribute.frame.origin.x + attribute.frame.size.width > layout.leftFixedPanelWidth + self.contentOffset.x + fabs(rect.origin.x) + rect.size.width)
            continue;
            
            [path moveToPoint:CGPointMake(attribute.frame.origin.x, -self.contentSize.height - rect.size.height)];
            [path addLineToPoint:CGPointMake(attribute.frame.origin.x, 3. * self.contentSize.height + rect.size.height)];
            
            w = attribute.frame.origin.x + attribute.frame.size.width;
        }
    }
    
    w = self.bounds.size.width;
    w -= layout.leftFixedPanelWidth;
    
    //horizontal line
    x = self.contentOffset.x + layout.leftFixedPanelWidth;
    if (x < layout.leftFixedPanelWidth + self.contentOffset.x)
        x = layout.leftFixedPanelWidth + self.contentOffset.x;
    
    if (x < layout.leftFixedPanelWidth)
        x = layout.leftFixedPanelWidth;
    
    y = self.contentOffset.y + self.topFixedPanelHeight;
    if (y && w) {
        [path moveToPoint:CGPointMake(x, y)];
        [path addLineToPoint:CGPointMake(x + w, y)];
    }
    
    [path closePath];
    [path stroke];
}

@end
