//
//  CalendarCollectionViewController.h
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarCollectionViewLayout.h"

@interface CalendarCollectionViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
{
    __weak CalendarCollectionView *view;
}

@property (weak) IBOutlet CalendarCollectionView *collectionView;

@end
