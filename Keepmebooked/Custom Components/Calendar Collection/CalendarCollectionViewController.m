//
//  CalendarCollectionViewController.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CalendarCollectionViewController.h"
#import "CalendarCollectionView.h"

@interface CalendarCollectionViewController ()

@end

@implementation CalendarCollectionViewController
{
    CGPoint pointOfBeginScroll;
    
    BOOL wasEndScrolling;
    BOOL wasExitedFromCompletionRight;
    BOOL wasExitedFromCompletionLeft;
    BOOL wasUntouch;
}

@synthesize collectionView = view;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    wasEndScrolling = YES;
    wasExitedFromCompletionRight = YES;
    wasExitedFromCompletionLeft = YES;
    wasUntouch = NO;
    
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)view.collectionViewLayout;
    layout.dataSource = view.calendarDataSource;
    
    layout.topFixedPanelCellWidth = view.topFixedPanelCellWidth;
    layout.topFixedPanelHeight = view.topFixedPanelHeight;
    layout.leftFixedPanelCellHeight = view.leftFixedPanelCellHeight;
    layout.leftFixedPanelWidth = view.leftFixedPanelWidth;
    
    [view setNeedsDisplay];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    [view setNeedsDisplay];
}

#pragma mark <UICollectionViewDataSource>

//number of lines
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //(numberOfRowsForCalendarCollectionViewLayout) returns without top fixed panel
    NSInteger numberOfSections = [view.calendarDataSource numberOfRowsForCalendarCollectionView:(CalendarCollectionView *)collectionView] + 1;
    
    return numberOfSections;
}

//section - is line
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //section is with top panel
    //section == 0 - top panel
    //return without left panel
    if (section == 0) { //top fixed row
        CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)view.collectionViewLayout;
        
        return [layout numberOfItemsInTopFixedPanel];
    } else { //other row
        NSInteger numberOfObjects = [view.calendarDataSource calendarCollectionView:(CalendarCollectionView *)collectionView numberOfObjectsInRow:section - 1];
        return numberOfObjects + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CCIndexPath *ccIndexPath = [CCIndexPath initWithIndexPath:indexPath];
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)view.collectionViewLayout;
    
    if (ccIndexPath.row == 0 && ccIndexPath.column == 0) { //only top left fixed cell
        return [view.calendarDataSource topLeftFixedCellForCalendarCollectionView:(CalendarCollectionView *)collectionView];
        
    } else if (ccIndexPath.row == 0 && ccIndexPath.column > 0) { //column > 0, => need info about day
        NSDate *date = [layout dateForDayNumber:ccIndexPath.column - 1];
        
        return [view.calendarDataSource calendarCollectionView:(CalendarCollectionView *)collectionView cellForTopFixedPanelWithDate:date andColumnIndex:ccIndexPath.column - 1];
        
    } else if (ccIndexPath.row > 0 && ccIndexPath.column == 0) {//row > 0 => need info about left cell
        return [view.calendarDataSource calendarCollectionView:(CalendarCollectionView *)collectionView cellForLeftFixedPanelWithRow:ccIndexPath.row - 1];
        
    } else if (ccIndexPath.row > 0 && ccIndexPath.column > 0) {
        ccIndexPath = [[CCIndexPath alloc] initWithRow:ccIndexPath.row - 1 andColumm:ccIndexPath.column - 1];
        //for content objects
        return [view.calendarDataSource calendarCollectionView:(CalendarCollectionView *)collectionView cellForIndexPath:ccIndexPath];
    }
    
    return nil;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(nonnull UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CCIndexPath *ccIndexPath = [CCIndexPath initWithIndexPath:indexPath];
    if (ccIndexPath.row == 0 ||
        ccIndexPath.column == 0)
        return;
    
    ccIndexPath = [[CCIndexPath alloc] initWithRow:ccIndexPath.row - 1 andColumm:ccIndexPath.column - 1];
    
    if ([view.calendarDataSource respondsToSelector:@selector(calendarCollectionView:didSelectObjectAtIndexPath:)]) {
        [view.calendarDelegate calendarCollectionView:(CalendarCollectionView *)collectionView didSelectObjectAtIndexPath:ccIndexPath];
    }
}

#pragma mark - Scroll view delegate methods

- (void)scrollViewWillBeginDragging:(nonnull UIScrollView *)scrollView {
    pointOfBeginScroll = view.contentOffset;
    
    wasUntouch = NO;
}

- (void)scrollViewDidScroll:(nonnull UIScrollView *)scrollView {
    [view setNeedsDisplay];
    
    if (scrollView.contentOffset.x < 0) {
        scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
        
        return;
    } else if (scrollView.contentOffset.x > scrollView.contentSize.width - self.collectionView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.contentSize.width - self.collectionView.bounds.size.width, scrollView.contentOffset.y);
        
        return;
    }

    CGPoint currentPoint = view.contentOffset;
    int direction = 0;
    if (currentPoint.x > pointOfBeginScroll.x) {//to the right, scroll to left
        direction = 1;
    } else if (currentPoint.x < pointOfBeginScroll.x) {//to the left, scroll to right
        direction = -1;
    }
    
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)self.collectionView.collectionViewLayout;
    NSDate *currentDate = layout.currentDate;
    [view.calendarDelegate calendarCollectionView:view currentDate:currentDate];

    if (direction == 1 && wasEndScrolling) {
        wasEndScrolling = NO;
        [view.calendarDataSource loadDataToLaterForCalendarCollectionView:view withCompletionBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{

                wasExitedFromCompletionRight = YES;
                [view setNeedsDisplay];
            });
        }];
    } else if (direction == -1 && wasEndScrolling) {
        wasEndScrolling = NO;
        [view.calendarDataSource loadDataToEarlierForCalendarCollectionView:view withCompletionBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                wasExitedFromCompletionRight = YES;
                [view setNeedsDisplay];
            });
        }];
    }
    
    if (scrollView.contentOffset.y < 0) {
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, 0);
        
        return;
    } else if (scrollView.contentOffset.y > scrollView.contentSize.height - self.collectionView.bounds.size.height) {
        float y = scrollView.contentSize.height;
        y -= self.collectionView.bounds.size.height;
        y = MAX(0, y);
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, y);
        
        return;
    }
}

- (void)scrollViewDidEndDecelerating:(nonnull UIScrollView *)scrollView {
    wasEndScrolling = YES;
}

- (void)scrollViewDidEndDragging:(nonnull UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    wasUntouch = YES;
}

@end
