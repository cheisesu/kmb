//
//  DashboardCollectionViewCell.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DashboardCollectionViewCell.h"

@implementation DashboardCollectionViewCell

- (void)awakeFromNib {
    self.layer.cornerRadius = 4.;
    self.layer.borderColor = [[UIColor colorFromRGB:219 alpha:255] CGColor];
    self.layer.borderWidth = 1.;
}

- (void)prepareForReuse {
    self.numberLabel.text = @"0";
    self.titleLabel.text = @"";
}

@end
