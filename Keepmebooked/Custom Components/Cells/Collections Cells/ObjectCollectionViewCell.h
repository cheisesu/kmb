//
//  ObjectCollectionViewCell.h
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarCollectionViewLayout.h"
#import "KMBCalendarBooking.h"

@class BackgroundView;

IB_DESIGNABLE
@interface ObjectCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *objectDescriptionLabel;

@property IBInspectable UIColor *contentColor;

@property (nonatomic) KMBCalendarBooking *bookingInfo;
@property (nonatomic, weak) CalendarCollectionViewLayout *layout;

@end