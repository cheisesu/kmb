//
//  ObjectCollectionViewCell.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "ObjectCollectionViewCell.h"
#import "KMBCalendarBookingStyle.h"
#import "NSDate+Utils.h"

@implementation ObjectCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"ObjectCollectionViewCell Init with frame");
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [[self.contentColor colorWithAlphaComponent:.8] setFill];
    
    CGRect contentRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    contentRect.origin.y += 8;
    contentRect.size.height -= 16;

    UIRectFill(contentRect);
    
    UIBezierPath *line = [UIBezierPath bezierPath];
    line.lineWidth = 2;
    
    UIColor *lineColor = [self.contentColor colorWithAlphaComponent:1];
    [lineColor setStroke];
    [line moveToPoint:CGPointMake(contentRect.origin.x + 1, contentRect.origin.y)];
    [line addLineToPoint:CGPointMake(contentRect.origin.x + 1, contentRect.origin.y + contentRect.size.height)];
    
    [line closePath];
    [line stroke];
    
#if !TARGET_INTERFACE_BUILDER
    UIBezierPath *lines = [UIBezierPath bezierPath];
    lines.lineWidth = 1;
    lineColor = [self.contentColor colorWithAlphaComponent:.9];
    [lineColor setStroke];
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDateComponents* beginDateTimeComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:_bookingInfo.checkInDate.toLocalTime];
    [beginDateTimeComponents setCalendar:calendar];
    NSDate *tempDate = [beginDateTimeComponents date];
    tempDate = [tempDate dateByAddingTimeInterval:CCVNumberOfSecondsInDay - 10. * 60. * 60.];
    NSTimeInterval firstInterval = [tempDate timeIntervalSinceDate:_bookingInfo.checkInDate.toLocalTime];
    
    CGFloat x = firstInterval * self.layout.topFixedPanelCellWidth / CCVNumberOfSecondsInDay - 3.;
    while (x <= contentRect.origin.x + contentRect.size.width) {
        [lines moveToPoint:CGPointMake(x + contentRect.origin.x, contentRect.origin.y)];
        [lines addLineToPoint:CGPointMake(x + contentRect.origin.x, contentRect.origin.y + contentRect.size.height)];
        
        x += self.layout.topFixedPanelCellWidth;
    }
    
    [lines closePath];
    [lines stroke];
#endif
}

- (void)prepareForReuse {
    self.objectDescriptionLabel.text = @"";
    self.bookingInfo = nil;
    self.layout = nil;
    
    [self layoutIfNeeded];
    [self setNeedsDisplay];
    
    NSLog(@"ObjectCollectionViewCell prepare for reuse");
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    [self layoutIfNeeded];
    [self setNeedsDisplay];
}

- (void)setBookingInfo:(KMBCalendarBooking *)bookingInfo {
    if (_bookingInfo != bookingInfo) {
        _bookingInfo = bookingInfo;
        
        if (_bookingInfo) {
            self.objectDescriptionLabel.text = _bookingInfo.label;
            self.contentColor = _bookingInfo.style.backgroundUIColor;
            
            [self setNeedsDisplay];
        }
    }
}

@end