//
//  DateCollectionViewCell.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DateCollectionViewCell.h"

@implementation DateCollectionViewCell

- (void)prepareForReuse {
    self.dayLabel.text = @"";
    self.descritionOfDayLabel.text = @"";
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.separatorColor setStroke];
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:rect.origin];
    [path addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [path closePath];
    [path stroke];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    [self setNeedsDisplay];
}

@end
