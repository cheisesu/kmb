//
//  TopLeftCollectionViewCell.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "TopLeftCollectionViewCell.h"

@implementation TopLeftCollectionViewCell

- (void)prepareForReuse {
    [self setNeedsDisplay];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    [self setNeedsDisplay];
}

@end
