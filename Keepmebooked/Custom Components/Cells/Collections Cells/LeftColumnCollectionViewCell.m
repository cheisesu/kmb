//
//  LeftColumnCollectionViewCell.m
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "LeftColumnCollectionViewCell.h"

@implementation LeftColumnCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"LeftColumnCollectionViewCell Init with frame");
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}

- (void)prepareForReuse {
    self.roomLabel.text = @"";
    
    [self setNeedsDisplay];
    NSLog(@"LeftColumnCollectionViewCell prepare for reuse");
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    [self setNeedsDisplay];
}

@end
