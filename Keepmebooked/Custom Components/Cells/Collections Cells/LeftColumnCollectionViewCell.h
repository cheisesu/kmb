//
//  LeftColumnCollectionViewCell.h
//  Calendar View
//
//  Created by Shelonin Dmitry on 10.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftColumnCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *roomLabel;

@end
