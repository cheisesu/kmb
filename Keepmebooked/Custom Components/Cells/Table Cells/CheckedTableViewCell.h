//
//  CheckedTableViewCell.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 04.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckedTableViewCell : UITableViewCell
@property (nonatomic) BOOL checked;

@property (weak) IBOutlet UILabel *titleLabel;
@property (weak) IBOutlet UIImageView *checkedImageView;
@end
