//
//  CheckAvailabilityTableViewCell.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckAvailabilityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *roomLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceForLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@end
