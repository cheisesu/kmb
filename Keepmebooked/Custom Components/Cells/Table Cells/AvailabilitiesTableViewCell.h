//
//  AvailabilitiesTableViewCell.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 27.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvailabilitiesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *roomLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
