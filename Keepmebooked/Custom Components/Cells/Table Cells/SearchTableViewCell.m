//
//  SearchTableViewCell.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (void)prepareForReuse {
    self.guestNameLabel.text = @"";
    self.checkInDateLabel.text = @"";
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
