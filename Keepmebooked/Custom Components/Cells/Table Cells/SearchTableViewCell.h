//
//  SearchTableViewCell.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell
@property (weak) IBOutlet UILabel *guestNameLabel;
@property (weak) IBOutlet UILabel *checkInDateLabel;
@end
