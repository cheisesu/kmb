//
//  CheckedTableViewCell.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 04.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CheckedTableViewCell.h"

@implementation CheckedTableViewCell
{
    UIImage *checkedImage;
    UIImage *uncheckedImage;
}

- (void)prepareForReuse {
    self.titleLabel.text = @"";
    self.checked = NO;
    
    if (_checked) {
        self.checkedImageView.image = checkedImage;
    } else {
        self.checkedImageView.image = uncheckedImage;
    }
    
    [self setNeedsDisplay];
}

- (void)awakeFromNib {
    checkedImage = [UIImage imageNamed:@"checked"];
    uncheckedImage = [UIImage imageNamed:@"unchecked"];
    self.titleLabel.text = @"";
    self.checked = NO;
    
    if (_checked) {
        self.checkedImageView.image = checkedImage;
    } else {
        self.checkedImageView.image = uncheckedImage;
    }
    
    [self setNeedsDisplay];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setChecked:(BOOL)checked {
    if (_checked != checked) {
        _checked = checked;
        
        if (_checked) {
            self.checkedImageView.image = checkedImage;
        } else {
            self.checkedImageView.image = uncheckedImage;
        }
        
        [self setNeedsDisplay];
    }
}

@end
