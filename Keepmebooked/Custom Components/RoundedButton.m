//
//  RoundedButton.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "RoundedButton.h"

@implementation RoundedButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = self.borderRadius;
}

@end
