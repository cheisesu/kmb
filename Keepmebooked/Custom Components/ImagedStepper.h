//
//  ImagedStepper.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImagedStepper;

@protocol ImagedStepperDelegate <NSObject>

- (void)imagedStepperDecrement:(ImagedStepper *)imagedStepper;
- (void)imagedStepperIncrement:(ImagedStepper *)imagedStepper;
- (void)imagedStepperTitleTouched:(ImagedStepper *)imagedStepper;

@end

IB_DESIGNABLE
@interface ImagedStepper : UIView

@property (weak) IBOutlet id<ImagedStepperDelegate> delegate;

@property (nonatomic) IBInspectable UIImage *image;
@property (nonatomic) IBInspectable CGFloat minValue;
@property (nonatomic) IBInspectable CGFloat maxValue;
@property IBInspectable CGFloat stepValue;
@property (nonatomic) IBInspectable CGFloat currentValue;
//must contains only 1 parameter
@property IBInspectable NSString *titleFormat;

@end
