//
//  DetailSectionHeaderView.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 18.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DetailSectionHeaderView.h"

@implementation DetailSectionHeaderView

- (IBAction)addButtonTouched:(id)sender {
    if ([self.delegate respondsToSelector:@selector(addButtonTouchedForDetailSectionHeaderView:)]) {
        [self.delegate addButtonTouchedForDetailSectionHeaderView:self];
    }
}

@end
