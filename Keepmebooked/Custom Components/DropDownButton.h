//
//  DropDownButton.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 15.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface DropDownButton : UIButton

@property (getter = isDowned) IBInspectable BOOL downed;
@property (getter = isOnBorders) IBInspectable BOOL onBorders;

@property IBInspectable CGFloat leftInset;
@property IBInspectable CGFloat rightInset;

@end
