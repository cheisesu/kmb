//
//  LeftImagedTextField.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "LeftImagedTextField.h"

@implementation LeftImagedTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName:self.defaultTint}];
    
    UIImage *image = self.leftImage;
    if (!self.text.length) {
        image = self.leftEmptyImage;
    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeCenter;
    self.leftView = imageView;
    
    [self addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.bottomLineColor setStroke];
    if (self.text.length) {
        [self.bottomLineActiveColor setStroke];
    }
    UIBezierPath *line = [UIBezierPath new];
    [line moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [line addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)];
    [line closePath];
    
    [line stroke];
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect result = CGRectMake(0, 0, self.frame.size.height, self.frame.size.height);
    
    return result;
}

- (void)setDefaultTint:(UIColor *)defaultTint {
    if (_defaultTint != defaultTint) {
        _defaultTint = defaultTint;
        
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName:_defaultTint}];
    }
}

- (void)textFieldDidChange:(LeftImagedTextField *)sender {
    UIImage *image = sender.leftImage;
    if (!sender.text.length) {
        image = sender.leftEmptyImage;
    }
    
    UIImageView *imageView = (UIImageView *)sender.leftView;
    imageView.image = image;
    
    [self setNeedsDisplay];
}

- (void)setText:(NSString *)text {
    [super setText:text];
    
    UIImage *image = self.leftImage;
    if (!self.text.length) {
        image = self.leftEmptyImage;
    }
    
    UIImageView *imageView = (UIImageView *)self.leftView;
    imageView.image = image;
    
    [self setNeedsDisplay];
}

@end
