//
//  ImagedStepper.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "ImagedStepper.h"

@interface ImagedStepper()
@property (weak, nonatomic) IBOutlet UIButton *decrementButton;
@property (weak, nonatomic) IBOutlet UIButton *incrementButton;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;

@property UIView *view;
@end

@implementation ImagedStepper

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self load];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self load];
    }
    return self;
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    
    self.view.frame = self.bounds;
    [self.view layoutIfNeeded];
}

- (void)load {
    Class selfClass = [self class];
    NSBundle *bundle = [NSBundle bundleForClass:selfClass];
    NSArray *views = [bundle loadNibNamed:@"ImagedStepper" owner:self options:nil];
    self.view = views.firstObject;
    
    [self addSubview:self.view];
    self.view.frame = self.bounds;
}

- (void)setImage:(UIImage *)image {
    if (_image != image) {
        _image = image;
        
        [self.titleButton setImage:_image forState:UIControlStateNormal];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self updateButtonTitle];
    [self layoutIfNeeded];
}

- (void)updateButtonTitle {
    if (self.titleFormat.length == 0)
        self.titleFormat = @"%@";
    
    [self.titleButton setTitle:[NSString stringWithFormat:self.titleFormat, @(self.currentValue)] forState:UIControlStateNormal];
}

- (void)setCurrentValue:(CGFloat)currentValue {
    if (_currentValue != currentValue) {
        if (currentValue < self.minValue) {
            _currentValue = self.minValue;
        } else if (currentValue > self.maxValue) {
            _currentValue = self.maxValue;
        } else {
            _currentValue = currentValue;
        }
        
        [self updateButtonTitle];
    }
}

- (void)setMaxValue:(CGFloat)maxValue {
    if (_maxValue != maxValue) {
        _maxValue = maxValue;
        
        if (self.currentValue > maxValue) {
            self.currentValue = maxValue;
        }
    }
}

- (void)setMinValue:(CGFloat)minValue {
    if (_minValue != minValue) {
        _minValue = minValue;
        
        if (self.currentValue < minValue) {
            self.currentValue = minValue;
        }
    }
}

#pragma mark - Actions

- (IBAction)decrementButtonTouched:(id)sender {
    self.currentValue -= self.stepValue;
    if (self.currentValue < self.minValue) {
        self.currentValue = self.minValue;
        
        return;
    }
    
    [self updateButtonTitle];
    
    if ([self.delegate respondsToSelector:@selector(imagedStepperDecrement:)]) {
        [self.delegate imagedStepperDecrement:self];
    }
}

- (IBAction)incrementButtonTouched:(id)sender {
    self.currentValue += self.stepValue;
    if (self.currentValue > self.maxValue) {
        self.currentValue = self.maxValue;
        
        return;
    }
    
    [self updateButtonTitle];
    
    if ([self.delegate respondsToSelector:@selector(imagedStepperIncrement:)]) {
        [self.delegate imagedStepperIncrement:self];
    }
}

- (IBAction)titleButtonTouched:(id)sender {
    if ([self.delegate respondsToSelector:@selector(imagedStepperTitleTouched:)]) {
        [self.delegate imagedStepperTitleTouched:self];
    }
}

@end
