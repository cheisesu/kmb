//
//  RoundedButton.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface RoundedButton : UIButton

@property IBInspectable CGFloat borderRadius;

@end
