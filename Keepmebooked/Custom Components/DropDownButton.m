//
//  DropDownButton.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 15.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DropDownButton.h"

@implementation DropDownButton

- (void)updatePositions {
    UILabel *titleLabel = self.titleLabel;
    CGRect labelFrame = titleLabel.frame;
    UIImageView *imageView = self.imageView;
    CGRect imageFrame = imageView.frame;
    
    if (self.onBorders) {
        labelFrame.origin.x = self.leftInset;
        imageFrame.origin.x = self.frame.size.width - imageFrame.size.width - self.rightInset;
        labelFrame.size.width = imageFrame.origin.x - self.leftInset;
    } else {
        labelFrame.origin.x -= imageFrame.size.width / 2.;
        imageFrame.origin.x = labelFrame.origin.x + labelFrame.size.width + imageFrame.size.width / 2.;
    }
    
    imageView.frame = imageFrame;
    titleLabel.frame = labelFrame;
    
    CGFloat angle = M_PI;
    if (self.isDowned) {
        angle = 0;
    }
    
    imageView.transform = CGAffineTransformMakeRotation(angle);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updatePositions];
}

@end
