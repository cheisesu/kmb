//
//  BorderedButton.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 05.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "BorderedButton.h"

@implementation BorderedButton

- (void)awakeFromNib {
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.layer.borderWidth = self.borderWidth;
        self.layer.borderColor = [self.borderColor CGColor];
        self.layer.cornerRadius = self.borderRadius;
    } else {
        self.layer.borderWidth = 0;
        self.layer.borderColor = nil;
        self.layer.cornerRadius = self.borderRadius;
    }
}

- (void)updatePositions {
    UILabel *titleLabel = self.titleLabel;
    CGRect labelFrame = titleLabel.frame;
    UIImageView *imageView = self.imageView;
    CGRect imageFrame = imageView.frame;
    
    imageFrame.origin.x = imageFrame.origin.y;
    imageView.frame = imageFrame;
    
    labelFrame.origin.x = 2. * imageFrame.origin.x + imageFrame.size.width;
    labelFrame.size.width = self.frame.size.width - labelFrame.origin.x - imageFrame.origin.x;
    titleLabel.frame = labelFrame;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updatePositions];
}

@end
