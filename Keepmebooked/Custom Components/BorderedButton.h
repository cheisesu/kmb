//
//  BorderedButton.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 05.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BorderedButton : UIButton
@property (nonatomic) IBInspectable CGFloat borderRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColor;
@end
