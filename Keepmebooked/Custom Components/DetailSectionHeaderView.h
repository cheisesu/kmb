//
//  DetailSectionHeaderView.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 18.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailSectionHeaderView;

@protocol DetailSectionHeaderViewDelegate <NSObject>

- (void)addButtonTouchedForDetailSectionHeaderView:(DetailSectionHeaderView *)view;

@end

@interface DetailSectionHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UIView *separator;
@property (weak, nonatomic) IBOutlet UIView *fullSeparator;

@property NSInteger identifier;

@property (weak) IBOutlet id<DetailSectionHeaderViewDelegate> delegate;

@end
