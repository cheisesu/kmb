//
//  LeftImagedTextField.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface LeftImagedTextField : UITextField
@property IBInspectable UIImage *leftImage;
@property IBInspectable UIImage *leftEmptyImage;
@property (nonatomic) IBInspectable UIColor *defaultTint;
@property IBInspectable UIColor *bottomLineColor;
@property IBInspectable UIColor *bottomLineActiveColor;
@end
