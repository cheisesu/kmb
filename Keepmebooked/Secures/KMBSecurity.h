//
//  KMBSecurity.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBSecurity : NSObject

+ (instancetype)userSecurity;

- (NSString *)keyChainPasswordWithLogin:(NSString *)login;
- (BOOL)setKeychainPassword:(NSString *)password forLogin:(NSString *)login;

@end
