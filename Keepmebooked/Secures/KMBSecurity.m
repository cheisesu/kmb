//
//  KMBSecurity.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBSecurity.h"

@implementation KMBSecurity

+ (instancetype)userSecurity {
    static KMBSecurity *userSecurity = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        userSecurity = [[KMBSecurity alloc] init];
    });
    
    return userSecurity;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSMutableDictionary *)searchDictionaryWithIdentifier:(NSString *)identifier {
    
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    
    NSString *serviceName = [NSBundle mainBundle].bundleIdentifier;
    [searchDictionary setObject:serviceName forKey:(__bridge id)kSecAttrService];
    
    return searchDictionary;
}

- (NSString *)keyChainPasswordWithLogin:(NSString *)login {
    
    NSMutableDictionary *searchDictionary = [self searchDictionaryWithIdentifier:login];
    
    // Add search attributes
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    
    // Add search return types
    [searchDictionary setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    CFDictionaryRef cfresult = NULL;
    SecItemCopyMatching((__bridge_retained CFDictionaryRef)searchDictionary, (CFTypeRef *)&cfresult);
    
    return [[NSString alloc] initWithData:(__bridge_transfer NSData *)cfresult encoding:NSUTF8StringEncoding];
}

- (BOOL)setKeychainPassword:(NSString *)password forLogin:(NSString *)login {
    
    NSMutableDictionary *dictionary = [self searchDictionaryWithIdentifier:login];
    
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(__bridge id)kSecValueData];
    
    if (SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL) == errSecSuccess) {
        return YES;
    }
    return NO;
}

@end
