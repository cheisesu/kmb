//
//  AppDelegate.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 14.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "AppDelegate.h"
#import "KMBRequest.h"
#import "KMBLoginResponse.h"
#import "KMBUser.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Analytics/Analytics.h>
#import <Intercom/Intercom.h>

static NSString *SegmentWriteKey = @"1767lglbwo";
static NSString *IntercomApiKey = @"ios_sdk-375c6b44227ee87a3228eecde87ded724d6c2d1d";
static NSString *InterComAppId = @"lo5q8udq";

@interface AppDelegate ()
@property UIViewController *popupView;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.popupView = nil;
    
    UIFont *font = [UIFont fontWithName:@"AvenirNextLTPro-Regular" size:18.0f];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName:[UIColor whiteColor],
                                                           NSFontAttributeName:font,
                                                           } forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName:[UIColor whiteColor],
                                                           NSFontAttributeName:font,
                                                           } forState:UIControlStateSelected];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName:[UIColor whiteColor],
                                                           NSFontAttributeName:font,
                                                           } forState:UIControlStateHighlighted];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName:[UIColor whiteColor],
                                                           NSFontAttributeName:font,
                                                           }];
    [Fabric with:@[[Crashlytics class]]];
    [SEGAnalytics debug:YES];
    [SEGAnalytics setupWithConfiguration:[SEGAnalyticsConfiguration configurationWithWriteKey:SegmentWriteKey]];
    [Intercom setApiKey:IntercomApiKey forAppId:InterComAppId];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)resetToInitialView:(BOOL)fromUserLogout {
    for (UIView* view in self.window.subviews)
    {
        [view removeFromSuperview];
    }
    
    if (!fromUserLogout) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot log in again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    UIViewController* initialScene = [self.window.rootViewController.storyboard instantiateInitialViewController];
    self.window.rootViewController = initialScene;
}

- (void)showModalPopup:(UIViewController *)popup {
    if (self.popupView) {
        [self.popupView.view removeFromSuperview];
    }
}

@end
