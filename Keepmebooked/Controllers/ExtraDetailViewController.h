//
//  ExtraDetailViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 29.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMBExtraInfo.h"
#import "KMBBookingInfo.h"

@class ExtraDetailViewController;

@protocol ExtraDetailProtocol <NSObject>
- (void)extraDetailDone:(ExtraDetailViewController *)controller;
@end

@interface ExtraDetailViewController : UIViewController
@property (weak) id<ExtraDetailProtocol> delegate;

@property (nonatomic) KMBExtraInfo *extraInfo;
@property KMBBookingInfo *bookingInfo;

@property BOOL isPrice;
@end
