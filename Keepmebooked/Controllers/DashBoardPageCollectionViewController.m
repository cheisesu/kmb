//
//  DashBoardPageCollectionViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 19.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DashBoardPageCollectionViewController.h"

#import "DashboardCollectionViewCell.h"
#import "AvailabilitiesTableViewController.h"
#import "StayingTonightTableViewController.h"
#import "TurnAroundsTableViewController.h"
#import "CheckInsViewController.h"
#import "CheckOutsViewController.h"
#import "NewBookingsTableViewController.h"
#import "KMBDashboard.h"
#import "KMBUser.h"
#import "KMBStayingTonight.h"
#import "KMBTurnaroundInfo.h"
#import "KMBBookingIdRoom.h"

#import "AppDelegate.h"

NSString *titles[] = {
    @"Check out",
    @"Check in",
    @"Stays",
    @"Turnarounds",
    @"Availabilities",
    @"New Bookings",
};

static NSInteger DBPCCheckOutsIndex = 0;
static NSInteger DBPCCheckInsIndex = 1;
static NSInteger DBPCStayingTonightsIndex = 2;
static NSInteger DBPCTurnaroundsIndex = 3;
static NSInteger DBPCAvailabilitiesIndex = 4;
static NSInteger DBPCNewBookingsIndex = 5;

@interface DashBoardPageCollectionViewController () <UICollectionViewDelegateFlowLayout>
@property NSMutableArray *data;
@property NSDate *date;
@property BOOL isAddedObserver;

@property BOOL wasErrorLoadCategories;
@end

@implementation DashBoardPageCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageIndex = NSNotFound;
    self.pageTitle = @"";
    
    [self initData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initData {
    self.data = @[
                  @[].mutableCopy,
                  @[].mutableCopy,
                  @[].mutableCopy,
                  @[].mutableCopy,
                  @[].mutableCopy,
                  @[].mutableCopy,
                  ].mutableCopy;
    
    if (!self.isAddedObserver) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hotelDidSwitchedNotification:) name:KMBSwitchHotelNotification object:nil];
        
        self.isAddedObserver = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadInfo];
    [[SEGAnalytics sharedAnalytics] screen:@"Dashboard" properties: @{
                                                                       }];
}

- (void)loadInfo {
    self.wasErrorLoadCategories = NO;
    
    [self initData];
    
    self.date = [NSDate date];
    if (self.pageIndex == 1) {
        self.date = [self.date dateByAddingTimeInterval:24 * 60 * 60];
    }
    
    [self loadAvailabilities:self.date];
    [self loadStayingTonight:self.date];
    [self loadTurnarounds:self.date];
    [self loadCheckIns:self.date];
    [self loadCheckOuts:self.date];
    [self loadNewBookings:self.date];
}

- (void)loadStayingTonight:(NSDate *)date {
    __block DashBoardPageCollectionViewController *_self = self;
    [[KMBDashboard dashboard] loadStayingTonightForDate:date withView:nil withCompletionBlock:^(id result) {
        NSArray *temp = [result mutableCopy];
        for (KMBStayingTonight *st in temp) {
            [_self.data[DBPCStayingTonightsIndex] addObject:st];
        }

        [_self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:DBPCStayingTonightsIndex inSection:0]]];
    } withFailedBlock:^(NSError *error) {
        //TODO: условие должно выполниться как единое целое
        if (!_self.wasErrorLoadCategories) {
            _self.wasErrorLoadCategories = YES;
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (void)loadAvailabilities:(NSDate *)date {
    __block DashBoardPageCollectionViewController *_self = self;
    [[KMBDashboard dashboard] loadAvailabilitiesForDate:date withView:nil withCompletionBlock:^(id result) {
        _self.data[DBPCAvailabilitiesIndex] = [result mutableCopy];
        
        [_self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:DBPCAvailabilitiesIndex inSection:0]]];
    } withFailedBlock:^(NSError *error) {
        //TODO: условие должно выполниться как единое целое
        if (!_self.wasErrorLoadCategories) {
            _self.wasErrorLoadCategories = YES;
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (void)loadTurnarounds:(NSDate *)date {
    __block DashBoardPageCollectionViewController *_self = self;
    [[KMBDashboard dashboard] loadTurnaroundsForDate:date withView:nil withCompletionBlock:^(id result) {
        _self.data[DBPCTurnaroundsIndex] = [result mutableCopy];
        
        [_self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:DBPCTurnaroundsIndex inSection:0]]];
    } withFailedBlock:^(NSError *error) {
        //TODO: условие должно выполниться как единое целое
        if (!_self.wasErrorLoadCategories) {
            _self.wasErrorLoadCategories = YES;
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (void)loadCheckIns:(NSDate *)date {
    __block DashBoardPageCollectionViewController *_self = self;
    [[KMBDashboard dashboard] loadCheckInsForDate:date withView:nil withCompletionBlock:^(id result) {
        _self.data[DBPCCheckInsIndex] = [result mutableCopy];
        
        [_self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:DBPCCheckInsIndex inSection:0]]];
    } withFailedBlock:^(NSError *error) {
        //TODO: условие должно выполниться как единое целое
        if (!_self.wasErrorLoadCategories) {
            _self.wasErrorLoadCategories = YES;
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (void)loadCheckOuts:(NSDate *)date {
    __block DashBoardPageCollectionViewController *_self = self;
    [[KMBDashboard dashboard] loadCheckOutsForDate:date withView:nil withCompletionBlock:^(id result) {
        _self.data[DBPCCheckOutsIndex] = [result mutableCopy];
        
        [_self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:DBPCCheckOutsIndex inSection:0]]];
    } withFailedBlock:^(NSError *error) {
        //TODO: условие должно выполниться как единое целое
        if (!_self.wasErrorLoadCategories) {
            _self.wasErrorLoadCategories = YES;
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (void)loadNewBookings:(NSDate *)date {
    __block DashBoardPageCollectionViewController *_self = self;
    [[KMBDashboard dashboard] loadNewBookingsForDate:date withView:nil withCompletionBlock:^(id result) {
        _self.data[DBPCNewBookingsIndex] = [result mutableCopy];
        
        [_self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:DBPCNewBookingsIndex inSection:0]]];
    } withFailedBlock:^(NSError *error) {
        //TODO: условие должно выполниться как единое целое
        if (!_self.wasErrorLoadCategories) {
            _self.wasErrorLoadCategories = YES;
            
            [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCheckoutsSegue"]) {
        CheckOutsViewController *dst = segue.destinationViewController;
        dst.checkOuts = self.data[DBPCCheckOutsIndex];
        dst.dashBoardPageIndex = self.pageIndex + 1;
        
    } else if ([segue.identifier isEqualToString:@"showCheckinsSegue"]) {
        CheckInsViewController *dst = segue.destinationViewController;
        dst.checkIns = self.data[DBPCCheckInsIndex];
        dst.dashBoardPageIndex = self.pageIndex + 1;
        
    } else if ([segue.identifier isEqualToString:@"showStayingTonightSegue"]) {
        StayingTonightTableViewController *dst = segue.destinationViewController;
        dst.stayingsTonight = self.data[DBPCStayingTonightsIndex];
        dst.dashBoardPageIndex = self.pageIndex + 1;
        
    } else if ([segue.identifier isEqualToString:@"showTurnAroundsSegue"]) {
        TurnAroundsTableViewController *dst = segue.destinationViewController;
        dst.turnarounds = self.data[DBPCTurnaroundsIndex];
        dst.dashBoardPageIndex = self.pageIndex + 1;
        
    } else if ([segue.identifier isEqualToString:@"showAvailabilitiesSegue"]) {
        AvailabilitiesTableViewController *dst = segue.destinationViewController;
        dst.availabilities = self.data[DBPCAvailabilitiesIndex];
        dst.dashBoardPageIndex = self.pageIndex + 1;
        
    } else if ([segue.identifier isEqualToString:@"showNewBookingsSegue"]) {
        NewBookingsTableViewController *dst = segue.destinationViewController;
        dst.bookings = self.data[DBPCNewBookingsIndex];
        dst.dashBoardPageIndex = self.pageIndex + 1;
        
    }
}

#pragma mark - Collection view data source methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DashboardCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DashboardCell" forIndexPath:indexPath];
    
    NSArray *numArray = self.data[indexPath.row];
    cell.numberLabel.text = [NSString stringWithFormat:@"%ld", (unsigned long)numArray.count];
    cell.titleLabel.text = titles[indexPath.row];
    
    return cell;
}

#pragma mark - Collection view flow layout delegate methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize viewSize = self.view.frame.size;
    CGFloat width = viewSize.width / 2. - 10. - 20.;
    CGFloat height = (viewSize.height - 10. - 10.) / 3. - 10. - 10.;
    height = MAX(height, 63);
    
    return CGSizeMake(width, height);
}

#pragma mark - Collection view delegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: { //Check outs
            [self performSegueWithIdentifier:@"showCheckoutsSegue" sender:self];
            
            break;
        }
        case 1: { //Check ins
            [self performSegueWithIdentifier:@"showCheckinsSegue" sender:self];
            
            break;
        }
        case 2: { //Staying tonight
            [self performSegueWithIdentifier:@"showStayingTonightSegue" sender:self];
            
            break;
        }
        case 3: { //Turnarounds
            [self performSegueWithIdentifier:@"showTurnAroundsSegue" sender:self];
            
            break;
        }
        case 4: { //availabilities
            [self performSegueWithIdentifier:@"showAvailabilitiesSegue" sender:self];
            
            break;
        }
        case 5: { //New bookings
            [self performSegueWithIdentifier:@"showNewBookingsSegue" sender:self];
            
            break;
        }
    }
}

#pragma mark - Notifications handlers 

- (void)hotelDidSwitchedNotification:(NSNotification *)notif {
    [self loadInfo];
}

@end
