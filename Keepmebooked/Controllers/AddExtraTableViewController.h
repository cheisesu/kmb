//
//  AddExtraTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 29.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMBBookingInfo.h"

@class AddExtraTableViewController;

@protocol AddExtraDelegate <NSObject>

- (void)addExtraDidHidden:(AddExtraTableViewController *)controller;

@end

@interface AddExtraTableViewController : UITableViewController
@property (weak) id<AddExtraDelegate> delegate;
@property KMBBookingInfo *bookingInfo;
@property KMBBookingRoomInfo *roomInfo;
@property BOOL hasChanged;
@property NSMutableArray *deletedExtras;
@property NSMutableArray *addedExtras;
@property NSMutableArray *editedExtras;

@end
