//
//  AvailabilitiesTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "AvailabilitiesTableViewController.h"

#import "AvailabilitiesTableViewCell.h"
#import "KMBAvailability.h"
#import "BookingContactViewController.h"
#import "BookingDetailViewController.h"
#import "KMBDashboard.h"
#import "MBProgressHUD.h"

@interface AvailabilitiesTableViewController () <BookingContactDelegate, BookingDetailDelegate>

@end

@implementation AvailabilitiesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Availabilities" properties: @{}];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadAvailabilities{
    UIView *view = nil;
    if (!self.refreshControl.isRefreshing) {
        view = self.view;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSDate *date = [NSDate date];
    if (self.dashBoardPageIndex == 2) {
        date = [date dateByAddingTimeInterval:24 * 60 * 60];
    }
    __block AvailabilitiesTableViewController *_self = self;
    [[KMBDashboard dashboard] loadAvailabilitiesForDate:date withView:view withCompletionBlock:^(id result) {
        [_self.availabilities removeAllObjects];
        NSArray *availabilities = [result mutableCopy];
        for (KMBAvailability *availability in availabilities) {
            [_self.availabilities addObject:availability];
        }
        [_self.tableView reloadData];
        [_self.refreshControl endRefreshing];
    } withFailedBlock:^(NSError *error) {
        [_self.refreshControl endRefreshing];
    }];
}

- (void)refreshAction {
    [self loadAvailabilities];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.availabilities.count == 0) {
        return 1;
    }
    return self.availabilities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No availabilities";
        
        return cell;
    }
    
    AvailabilitiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AvailabilityCell" forIndexPath:indexPath];
    
    KMBAvailability *info = self.availabilities[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        NSString *room = [info.text substringToIndex:range.location];
        NSString *price = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.priceLabel.text = price;
    } else {
        cell.roomLabel.text = info.text;
        cell.priceLabel.text = @"";
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //showBookingContactAvailabilitySegue
    if ([segue.identifier isEqualToString:@"showBookingContactAvailabilitySegue"]) {
        UINavigationController *destNav = segue.destinationViewController;
        BookingContactViewController *dst = destNav.viewControllers.firstObject;
        dst.delegate = self;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        KMBAvailability *availability = [self.availabilities[indexPath.row] copy];
//        if ([availability.addBooking.startDate isEqualToDate:availability.addBooking.endDate]) {
//            availability.addBooking.endDate = [availability.addBooking.startDate dateByAddingTimeInterval:24 * 60 * 60];
//        }
        dst.availability = availability;
    } else if ([segue.identifier isEqualToString:@"showBookingDetailAvailabilitySegue"]) {
        UINavigationController *dstNav = segue.destinationViewController;
        BookingDetailViewController *detail = dstNav.viewControllers.firstObject;
        
        detail.showCloseButton = YES;
        detail.delegate = self;
        detail.bookingId = [sender[@"bookingId"] integerValue];
        detail.roomName = sender[@"roomName"];
    }
}

#pragma mark - Booking contact delegate methods

- (void)bookingContactCancel:(BookingContactViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)bookingContactSave:(BookingContactViewController *)controller andBookingId:(NSInteger)bookingId{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self loadAvailabilities];
}

- (void)bookingContactSaveAndView:(BookingContactViewController *)controller andBookingId:(NSInteger)bookingId andRoomName:(NSString *)roomName{
    [self dismissViewControllerAnimated:YES completion:^{
        NSMutableDictionary *sender = @{@"bookingId":@(bookingId)}.mutableCopy;
        if (roomName) {
            [sender setObject:roomName forKey:@"roomName"];
        }
        [self performSegueWithIdentifier:@"showBookingDetailAvailabilitySegue" sender:sender];
        
        [self loadAvailabilities];
    }];//closed BookingContactViewController
}

#pragma mark Booking detail delegate methods

- (void)bookingDetailDidClosed:(BookingDetailViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
