//
//  MarkUnavailableTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "MarkUnavailableTableViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "CheckedTableViewCell.h"

@interface MarkUnavailableTableViewController()
@end

@implementation MarkUnavailableTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self loadAvailableRooms];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Mark as unavailable" properties:@{}];
}

- (void)backButtonTouched {
    if ([self.delegate respondsToSelector:@selector(markUnavailableCancel:)]) {
        [self.delegate markUnavailableCancel:self];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    
}

- (void)loadAvailableRooms {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block MarkUnavailableTableViewController *_self = self;
    [[KMBBookings bookings] loadAvailableRoomsForNumberOfGuests:self.numberOfGuests startDate:self.startDate endDate:self.endDate withView:self.view withCompletionBlock:^(id result) {
        _self.availabilities = [result mutableCopy];
        [_self.tableView reloadData];
    }];
}

- (KMBAvailability *)findUnavailabile:(KMBAvailability *)availability {
    for (KMBAvailability *unavailable in [KMBBookings bookings].unavailableRooms) {
        if (unavailable.addBooking.roomId == availability.addBooking.roomId) {
            return unavailable;
        }
    }
    
    return nil;
}

- (BOOL)isUnavailable:(KMBAvailability *)availability {
    return ([self findUnavailabile:availability] != nil);
}

#pragma mark - Actions

- (IBAction)doneButtonTouched:(id)sender {
    if ([self.delegate respondsToSelector:@selector(markUnavailableDone:)]) {
        [self.delegate markUnavailableDone:self];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.availabilities.count == 0) {
        return 1;
    }
    return self.availabilities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No available rooms";
        
        return cell;
    }
    
    CheckedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AvailableRoomCell" forIndexPath:indexPath];

    KMBAvailability *info = self.availabilities[indexPath.row];
    cell.titleLabel.text = info.text;
    cell.checked = [self isUnavailable:info];
    
    return cell;
}

#pragma mark - Table view delegate methodss

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0)
        return;
    
    CheckedTableViewCell *cell = (CheckedTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    KMBAvailability *info = self.availabilities[indexPath.row];
    KMBAvailability *current = [self findUnavailabile:info];
    if (current != nil) {//unavaiable - current checked=yes, make available - checked = no
        [[KMBBookings bookings].unavailableRooms removeObject:current];
    } else {
        [[KMBBookings bookings].unavailableRooms addObject:info];
    }
    
    cell.checked = !(current != nil);
}

@end
