//
//  CheckOutsViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CheckOutsViewController.h"

#import "ChecksTableViewCell.h"
#import "NewBookingViewController.h"
#import "KMBCheckInOut.h"
#import "BookingDetailViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBBookingCheckInOut.h"
#import "KMBDashboard.h"
#import "RoundedButton.h"

@interface CheckOutsViewController () <UITableViewDataSource, UITableViewDelegate, NewBookingDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet RoundedButton *checkEveryoneOutButton;
@property UIRefreshControl *refreshControl;
@end

@implementation CheckOutsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Check out" properties: @{
                                                                      }];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)checkOut {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block CheckOutsViewController *_self = self;
    NSMutableArray *ids = [NSMutableArray new];
    for (KMBCheckInOut *info in self.checkOuts) {
        [ids addObject:@(info.bookingId)];
    }
    [[KMBBookings bookings] checkOutBookingsByIds:ids withView:self.view withCompletionBlock:^(id result) {
        BOOL all = YES;
        for (KMBBookingCheckInOut *info in result) {
            if (![info.status isEqualToString:@"checked_out"]) {
                all = NO;
                
                break;
            }
        }
        
        if (all) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check out" message:@"All bookings checked out" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            [_self loadCheckOuts];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check out" message:@"Not all bookings checked out" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)loadCheckOuts {
    UIView *view = nil;
    if (!self.refreshControl.isRefreshing) {
        view = self.view;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSDate *date = [NSDate date];
    if (self.dashBoardPageIndex == 2) {
        date = [date dateByAddingTimeInterval:24 * 60 * 60];
    }
    
    __block CheckOutsViewController *_self = self;
    [[KMBDashboard dashboard] loadCheckOutsForDate:date withView:view withCompletionBlock:^(id result) {
        [_self.checkOuts removeAllObjects];
        NSArray *temp = [result mutableCopy];
        for (KMBCheckInOut *inout in temp) {
            [_self.checkOuts addObject:inout];
        }
        
        _self.checkEveryoneOutButton.enabled = !(_self.checkOuts.count == 0);
        [_self.tableView reloadData];
        [_self.refreshControl endRefreshing];
    } withFailedBlock:^(NSError *error) {
        [_self.refreshControl endRefreshing];
    }];
}

- (void)refreshAction {
    [self loadCheckOuts];
}

- (IBAction)checkOutAllButtonTouched:(id)sender {
    if (self.checkOuts.count) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check out" message:@"Do you realy want to check everyone out?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
        [alert show];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"checkOutsAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"showCheckoutDetailSegue"]) {
        BookingDetailViewController *dst = segue.destinationViewController;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        KMBCheckInOut *info = self.checkOuts[indexPath.row];
        dst.bookingId = info.bookingId;
        NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
        if (range.length > 0) {
            dst.roomName = [info.text substringToIndex:range.location];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.checkOuts.count == 0) {
        return 1;
    }
    return self.checkOuts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.checkOuts.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No check outs";
        
        return cell;
    }
    
    ChecksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChecksCell" forIndexPath:indexPath];

    KMBCheckInOut *info = self.checkOuts[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        NSString *room = [info.text substringToIndex:range.location];
        NSString *name = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.nameLabel.text = name;
    } else {
        cell.roomLabel.text = info.text;
    }

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Alert view delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        [self checkOut];
    }
}

@end
