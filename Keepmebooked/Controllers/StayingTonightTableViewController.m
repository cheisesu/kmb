//
//  StayingTonightTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "StayingTonightTableViewController.h"

#import "ChecksTableViewCell.h"
#import "KMBStayingTonight.h"
#import "BookingDetailViewController.h"
#import "KMBDashboard.h"
#import "MBProgressHUD.h"
#import "NewBookingViewController.h"

@interface StayingTonightTableViewController () <NewBookingDelegate>

@end

@implementation StayingTonightTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Stays" properties: @{
                                                                   }];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showStayingTonightDetailSegue"]) {
        BookingDetailViewController *dst = segue.destinationViewController;
        
        UITableViewCell *cell = sender;
        NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
        KMBStayingTonight *stayingTonightInfo = self.stayingsTonight[cellIndexPath.row];

        dst.bookingId = stayingTonightInfo.bookingId;
        NSRange range = [stayingTonightInfo.text rangeOfString:@" - " options:NSBackwardsSearch];
        if (range.length > 0) {
            dst.roomName = [stayingTonightInfo.text substringToIndex:range.location];
        }
    } else if ([segue.identifier isEqualToString:@"stayingTonightAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
    }
}

- (void)loadStayingTonight {
    UIView *view = nil;
    if (!self.refreshControl.isRefreshing) {
        view = self.view;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSDate *date = [NSDate date];
    if (self.dashBoardPageIndex == 2) {
        date = [date dateByAddingTimeInterval:24 * 60 * 60];
    }
    
    __block StayingTonightTableViewController *_self = self;
    [[KMBDashboard dashboard] loadStayingTonightForDate:date withView:view withCompletionBlock:^(id result) {
        [_self.stayingsTonight removeAllObjects];
        NSArray *temp = [result mutableCopy];
        for (KMBStayingTonight *st in temp) {
            [_self.stayingsTonight addObject:st];
        }
        [_self.tableView reloadData];
        [_self.refreshControl endRefreshing];
    } withFailedBlock:^(NSError *error) {
        [_self.refreshControl endRefreshing];
    }];
}

- (void)refreshAction {
    [self loadStayingTonight];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.stayingsTonight.count == 0) {
        return 1;
    }
    return self.stayingsTonight.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.stayingsTonight.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No staying tonight";
        
        return cell;
    }
    
    ChecksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChecksCell" forIndexPath:indexPath];
    
    KMBStayingTonight *info = self.stayingsTonight[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        NSString *room = [info.text substringToIndex:range.location];
        NSString *name = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.nameLabel.text = name;
    } else {
        cell.roomLabel.text = info.text;
        cell.nameLabel.text = @"";
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
