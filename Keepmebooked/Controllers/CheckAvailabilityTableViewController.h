//
//  CheckAvailabilityTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckAvailabilityTableViewController : UITableViewController
@property NSInteger numberOfGuests;
@property NSDate *startDate;
@property NSDate *endDate;
//@property NSMutableArray *unavailable;
@end
