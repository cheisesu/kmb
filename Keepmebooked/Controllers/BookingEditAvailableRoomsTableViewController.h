//
//  BookingEditAvailableRoomsTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 11.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMBNewBooking.h"

@class BookingEditAvailableRoomsTableViewController;

@protocol BookingEditAvailableRoomsDelegate <NSObject>
- (void)bookingEditAvailableRooms:(BookingEditAvailableRoomsTableViewController *)controller didSelectRoomAvailability:(KMBNewBooking *)availabilityRoom andName:(NSString *)name;
@end

@interface BookingEditAvailableRoomsTableViewController : UITableViewController
@property NSInteger numberOfGuests;
@property NSDate *startDate;
@property NSDate *endDate;

@property (weak) id<BookingEditAvailableRoomsDelegate> delegate;
@end
