//
//  NewBookingViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "NewBookingViewController.h"
#import "ImagedStepper.h"
#import "UIColor+Brightness.h"
#import "MarkUnavailableTableViewController.h"
#import "CheckAvailabilityTableViewController.h"
#import "RoundedButton.h"

@interface NewBookingViewController() <ImagedStepperDelegate, MarkUnavailableDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerBottomConstraint;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet RoundedButton *checkInButton;
@property (weak, nonatomic) IBOutlet RoundedButton *checkOutButton;
@property (weak, nonatomic) IBOutlet UIView *datePickerTopView;
@property (weak, nonatomic) IBOutlet ImagedStepper *numberOfGuestsImagedStepper;

@property NSInteger pickerType;

@property NSDate *checkInDate;
@property NSDate *checkOutDate;

@property NSDateFormatter *titleDateFormatter;

@property NSMutableArray *unavailableRooms;
@end

@implementation NewBookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pickerType = NBPickerTypeNo;
    
    self.unavailableRooms = [NSMutableArray new];
    
    self.pickerBottomConstraint.constant = -self.datePickerView.bounds.size.height;
    self.datePickerView.hidden = YES;
    
    self.titleDateFormatter = [NSDateFormatter new];
    [self.titleDateFormatter setDateFormat:@"EEE dd LLL"];
    
    self.checkInDate = [NSDate date];
    self.checkOutDate = [[NSDate date] dateByAddingTimeInterval:24 * 60 * 60];
    [self updateDateButtonTitles];
    
    self.datePickerTopView.layer.borderColor = [[self.datePickerTopView.backgroundColor darker:.3] CGColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Add booking" properties: @{}];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showMarkUnavaiableRoomsSegue"]) {
        MarkUnavailableTableViewController *dst = segue.destinationViewController;
        
        dst.delegate = self;
        dst.startDate = self.checkInDate;
        dst.endDate = self.checkOutDate;
        dst.numberOfGuests = self.numberOfGuestsImagedStepper.currentValue;
//        dst.unavailable = self.unavailableRooms;
    } else if ([segue.identifier isEqualToString:@"showCheckAvailabilitySegue"]) {
        CheckAvailabilityTableViewController *dst = segue.destinationViewController;
        dst.startDate = self.checkInDate;
        dst.endDate = self.checkOutDate;
        dst.numberOfGuests = self.numberOfGuestsImagedStepper.currentValue;
//        dst.unavailable = self.unavailableRooms;
    }
}

#pragma mark - Inherit methods

- (void)showDatePicker {
    self.datePickerView.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.datePickerView.alpha = 1.;
        self.pickerBottomConstraint.constant = 0;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        ;
    }];
}

- (void)hideDatePicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.datePickerView.alpha = 0;
        self.pickerBottomConstraint.constant = -self.datePickerView.bounds.size.height;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.datePickerView.hidden = YES;
    }];
}

- (void)updateDateButtonTitles {
    [self.checkInButton setTitle:[self.titleDateFormatter stringFromDate:self.checkInDate] forState:UIControlStateNormal];
    [self.checkOutButton setTitle:[self.titleDateFormatter stringFromDate:self.checkOutDate] forState:UIControlStateNormal];
}

#pragma mark - Actions

- (IBAction)closeButtonTouched:(id)sender {
    if ([self.delegate respondsToSelector:@selector(newBookingCancel:)]) {
        [self.delegate newBookingCancel:self];
    }
}

- (IBAction)checkInButtonTouched:(id)sender {
    if (self.datePickerView.hidden) {
        self.datePicker.date = self.checkInDate;
        
        [self showDatePicker];
    } else {
        if (self.pickerType == NBPickerTypeCheckOut) {
            self.checkOutDate = self.datePicker.date;
            
            self.datePicker.date = self.checkInDate;
            
            [self updateDateButtonTitles];
        }
    }
    
    self.pickerType = NBPickerTypeCheckIn;
    self.checkInButton.selected = YES;
    self.checkOutButton.selected = NO;
}

- (IBAction)checkOutButtonTouched:(id)sender {
    if (self.datePickerView.hidden) {
        self.datePicker.date = self.checkOutDate;
        
        [self showDatePicker];
    } else {
        if (self.pickerType == NBPickerTypeCheckIn) {
            self.checkInDate = self.datePicker.date;
            
            self.datePicker.date = self.checkOutDate;
            
            [self updateDateButtonTitles];
        }
    }
    
    self.pickerType = NBPickerTypeCheckOut;
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = YES;
}

- (IBAction)datePickerDoneButtonTouched:(id)sender {
    switch (self.pickerType) {
        case NBPickerTypeCheckIn:
            self.checkInDate = self.datePicker.date;
            break;
            
        case NBPickerTypeCheckOut:
            self.checkOutDate = self.datePicker.date;
            break;
            
        default:
            break;
    }
    
    self.pickerType = NBPickerTypeNo;
    [self updateDateButtonTitles];
    [self hideDatePicker];
    
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = NO;
}

- (IBAction)datePickerCancelButtonTouched:(id)sender {
    [self hideDatePicker];
    
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = NO;
}

#pragma mark - Imaged stepper delegate methods

- (void)imagedStepperIncrement:(ImagedStepper *)imagedStepper {
    
}

- (void)imagedStepperDecrement:(ImagedStepper *)imagedStepper {
    
}

- (void)imagedStepperTitleTouched:(ImagedStepper *)imagedStepper {
    
}

#pragma mark - Mark unavailable delegate methods

- (void)markUnavailableDone:(MarkUnavailableTableViewController *)controller {
//    self.unavailableRooms = controller.unavailable;
}

- (void)markUnavailableCancel:(MarkUnavailableTableViewController *)controller {
}

@end
