//
//  BookingEditAvailableRoomsTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 11.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "BookingEditAvailableRoomsTableViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBAvailability.h"
#import "CheckAvailabilityTableViewCell.h"

@interface BookingEditAvailableRoomsTableViewController ()
@property NSMutableArray *availabilities;
@end

@implementation BookingEditAvailableRoomsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAvailableRooms];
}

- (KMBAvailability *)findUnavailabile:(KMBAvailability *)availability {
    for (KMBAvailability *unavailable in [KMBBookings bookings].unavailableRooms) {
        if (unavailable.addBooking.roomId == availability.addBooking.roomId) {
            return unavailable;
        }
    }
    
    return nil;
}

- (void)loadAvailableRooms {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block BookingEditAvailableRoomsTableViewController *_self = self;
    [[KMBBookings bookings] loadAvailableRoomsForNumberOfGuests:self.numberOfGuests startDate:self.startDate endDate:self.endDate withView:self.view withCompletionBlock:^(id result) {
        _self.availabilities = [NSMutableArray new];
        for (KMBAvailability *availability in result) {
            if (![self findUnavailabile:availability])
                [_self.availabilities addObject:availability];
        }
        
        [_self.tableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.layer.cornerRadius = 6;
}

- (void)chooseRoomWithAvailability:(KMBAvailability *)availability {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block BookingEditAvailableRoomsTableViewController *_self = self;
    [[KMBBookings bookings] chooseRoomByAvailability:availability withView:self.view withCompletionBlock:^(id result) {
        KMBNewBooking *booking = [result firstObject];
        
        if ([_self.delegate respondsToSelector:@selector(bookingEditAvailableRooms:didSelectRoomAvailability:andName:)]) {
            NSRange range = [availability.text rangeOfString:@" - " options:NSBackwardsSearch];
            NSString *name;
            if (range.length > 0) {
                name = [availability.text substringToIndex:range.location];
            } else {
                name = availability.text;
            }
            [_self.delegate bookingEditAvailableRooms:self didSelectRoomAvailability:booking andName:name];
            
            [_self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.availabilities.count == 0) {
        return 1;
    }
    return self.availabilities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No available rooms";
        
        return cell;
    }
    
    CheckAvailabilityTableViewCell *cell;
    
    KMBAvailability *info = self.availabilities[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CheckAvailabilityCell" forIndexPath:indexPath];
        
        NSString *room = [info.text substringToIndex:range.location];
        NSString *price = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.priceForLabel.text = price;
        cell.totalPriceLabel.text = [NSString stringWithFormat:@"%@", info.total];
    } else {
        cell.roomLabel.text = info.text;
    }
    
    cell.totalPriceLabel.text = [NSString stringWithFormat:@"%@", info.total];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0)
        return 44;
    
    return 103;
}

#pragma mark - Table view delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0)
        return;
    
    KMBAvailability *availability = self.availabilities[indexPath.row];
    [self chooseRoomWithAvailability:availability];
}

@end
