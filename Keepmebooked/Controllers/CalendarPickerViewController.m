//
//  CalendarPickerViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 15.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CalendarPickerViewController.h"
#import <FSCalendar/FSCalendar.h>

@interface CalendarPickerViewController ()
@property (strong) FSCalendar *calendarView;
@end

@implementation CalendarPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.calendarView = (FSCalendar *)self.view;
    self.calendarView.appearance.weekdayTextColor = [UIColor colorFromRed:63 green:157 blue:217 alpha:255];
    self.calendarView.appearance.headerTitleColor = [UIColor colorFromRed:63 green:157 blue:217 alpha:255];
    self.calendarView.appearance.todayColor = [UIColor colorFromRed:63 green:157 blue:217 alpha:255];
    self.calendarView.appearance.selectionColor = [UIColor colorFromRed:197 green:225 blue:244 alpha:255];
    
    self.calendarView.appearance.headerTitleTextSize = 18;
    self.calendarView.appearance.autoAdjustTitleSize = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
