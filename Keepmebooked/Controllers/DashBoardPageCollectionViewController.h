//
//  DashBoardPageCollectionViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 19.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashBoardPageCollectionViewController : UICollectionViewController

@property NSInteger pageIndex;
@property (strong) NSString *pageTitle;

- (void)loadInfo;

@end
