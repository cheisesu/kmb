//
//  BookingContactViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "BookingContactViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBNewBooking.h"
#import "KMBNewBookingSuccess.h"
#import "UITextField+Shake.h"

@interface BookingContactViewController()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottomConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *guestNameTextView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextView;

@property KMBNewBooking *booking;
@end

@implementation BookingContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotificationHandler:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotificationHandler:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotificationHandler:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [self chooseRoom];
    
    NSRange range = [self.availability.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        self.title = [self.availability.text substringToIndex:range.location];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    
}

- (void)chooseRoom {
    __block BookingContactViewController *_self = self;
    
    [self chooseRoomWithCompletion:^(id result) {
        _self.booking = [result firstObject];
    }];
}

- (void)chooseRoomWithCompletion:(KMBCompletionBlock)completion {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[KMBBookings bookings] chooseRoomByAvailability:self.availability withView:self.view withCompletionBlock:completion];
}

- (void)save:(BOOL)needView {
    if (self.guestNameTextView.text.length < 3) {
        [self.guestNameTextView shake];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Guest's  name must be at least 3 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.booking.guest = [KMBGuestInfo new];
    self.booking.guest.fullName = self.guestNameTextView.text;
    self.booking.guest.email = self.emailTextView.text;
    self.booking.guest.phoneCell = self.phoneNumberTextView.text;
    
    __block BookingContactViewController *_self = self;
    [[KMBBookings bookings] createBooking:self.booking withView:self.view withCompletionBlock:^(id result) {
        KMBNewBookingSuccess *newBooking = [result firstObject];
        
        NSString *roomName = nil;
        NSRange range = [_self.availability.text rangeOfString:@" - " options:NSBackwardsSearch];
        if (range.length > 0) {
            roomName = [_self.availability.text substringToIndex:range.location];
        }
        
        if (!needView) {
            if ([_self.delegate respondsToSelector:@selector(bookingContactSave:andBookingId:)]) {
                [_self.delegate bookingContactSave:_self andBookingId:newBooking.bookingId];
            }
        } else {
            if ([_self.delegate respondsToSelector:@selector(bookingContactSaveAndView:andBookingId:andRoomName:)]) {
                [_self.delegate bookingContactSaveAndView:_self andBookingId:newBooking.bookingId andRoomName:roomName];
            }
        }
    } withFailedBlock:^(NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New booking" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        if ([error.localizedDescription rangeOfString:@"Booking"].length > 0)
            alert.message = error.localizedDescription;
        
        [alert show];
    }];
}

#pragma mark - Actions

- (IBAction)closeButtonTouched:(id)sender {
    [self.view endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(bookingContactCancel:)]) {
        [self.delegate bookingContactCancel:self];
    }
}

- (IBAction)saveButtonTouched:(id)sender {
    [self.view endEditing:YES];
    
    __block BookingContactViewController *_self = self;
    
    if (!self.booking) {
        [self chooseRoomWithCompletion:^(id result) {
            _self.booking = [result firstObject];
            
            [_self save:NO];
        }];
    } else {
        [self save:NO];
    }
}

- (IBAction)saveAndViewButtonTouched:(id)sender {
    [self.view endEditing:YES];
    
    __block BookingContactViewController *_self = self;
    
    if (!self.booking) {
        [self chooseRoomWithCompletion:^(id result) {
            _self.booking = [result firstObject];
            
            [_self save:YES];
        }];
    } else {
        [self save:YES];
    }
}

#pragma mark - Notifications

- (void)keyboardWillShowNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollBottomConstraint.constant = size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHideNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollBottomConstraint.constant = 0;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillChangeFrameNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollBottomConstraint.constant = size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

@end
