//
//  BookingDetailViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BookingDetailViewController;

@protocol BookingDetailDelegate <NSObject>

- (void)bookingDetailDidClosed:(BookingDetailViewController *)controller;

@end

@interface BookingDetailViewController : UIViewController
@property (weak) id<BookingDetailDelegate> delegate;
@property BOOL showCloseButton;

@property NSInteger bookingId;
@property NSString *roomName;
@end
