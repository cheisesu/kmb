//
//  CheckInsViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CheckInsViewController.h"

#import "ChecksTableViewCell.h"
#import "NewBookingViewController.h"
#import "KMBCheckInOut.h"
#import "BookingDetailViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBBookingCheckInOut.h"
#import "KMBDashboard.h"
#import "RoundedButton.h"

@interface CheckInsViewController () <NewBookingDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet RoundedButton *checkEveryoneInButton;
@property UIRefreshControl *refreshControl;
@end

@implementation CheckInsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Check in" properties: @{
                                                                      }];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkIn {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block CheckInsViewController *_self = self;
    NSMutableArray *ids = [NSMutableArray new];
    for (KMBCheckInOut *info in self.checkIns) {
        [ids addObject:@(info.bookingId)];
    }
    [[KMBBookings bookings] checkInBookingsByIds:ids withView:self.view withCompletionBlock:^(id result) {
        BOOL all = YES;
        for (KMBBookingCheckInOut *info in result) {
            if (![info.status isEqualToString:@"checked_in"]) {
                all = NO;
                
                break;
            }
        }
        if (all) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check in" message:@"All bookings checked in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            [_self loadCheckIns];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check in" message:@"Not all bookings checked in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)loadCheckIns {
    UIView *view = nil;
    if (!self.refreshControl.isRefreshing) {
        view = self.view;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSDate *date = [NSDate date];
    if (self.dashBoardPageIndex == 2) {
        date = [date dateByAddingTimeInterval:24 * 60 * 60];
    }
    
    __block CheckInsViewController *_self = self;
    [[KMBDashboard dashboard] loadCheckInsForDate:date withView:view withCompletionBlock:^(id result) {
        [_self.checkIns removeAllObjects];
        NSArray *temp = [result mutableCopy];
        for (KMBCheckInOut *inout in temp) {
            [_self.checkIns addObject:inout];
        }
        _self.checkEveryoneInButton.enabled = _self.checkIns.count != 0;
        [_self.tableView reloadData];
        [_self.refreshControl endRefreshing];
    } withFailedBlock:^(NSError *error) {
        [_self.refreshControl endRefreshing];
    }];
}

- (void)refreshAction {
    [self loadCheckIns];
}

- (IBAction)checkInAllButtonTouched:(id)sender {
    if (self.checkIns.count)
        [self checkIn];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"checkInsAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"showCheckinsDetailSegue"]) {
        BookingDetailViewController *dst = segue.destinationViewController;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        KMBCheckInOut *info = self.checkIns[indexPath.row];
        dst.bookingId = info.bookingId;
        NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
        if (range.length > 0) {
            dst.roomName = [info.text substringToIndex:range.location];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.checkIns.count == 0) {
        return 1;
    }
    return self.checkIns.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.checkIns.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No check ins";
        
        return cell;
    }
    
    ChecksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChecksCell" forIndexPath:indexPath];
    
    KMBCheckInOut *info = self.checkIns[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        NSString *room = [info.text substringToIndex:range.location];
        NSString *price = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.nameLabel.text = price;
    } else {
        cell.roomLabel.text = info.text;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
