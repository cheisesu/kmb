//
//  SettingsViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SettingsViewController;

@protocol SettingViewControllerDelegate <NSObject>

- (void)settingsViewController:(SettingsViewController *)svc didSelectNumberOfDays:(NSInteger)numberOfDays;

@end

@interface SettingsViewController : UIViewController

@property (weak) id<SettingViewControllerDelegate> delegate;

@end
