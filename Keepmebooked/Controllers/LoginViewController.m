//
//  LoginViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "LoginViewController.h"

#import "UITextField+Shake.h"

#import "RoundedButton.h"

#import "KMBUser.h"
#import "KMBSecurity.h"
#import "KMBLoginResponse.h"
#import "KMBCommon.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *animatingIndicator;

@property NSInteger bottomConstraintConstant;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bottomConstraintConstant = self.bottomConstraint.constant;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotificationHandler:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotificationHandler:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotificationHandler:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Login" properties:@{}];
    
    NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    if (email) {
        self.loginTextField.text = email;
        
        NSString *password = [[KMBSecurity userSecurity] keyChainPasswordWithLogin:email];
        if (password) {
            self.passwordTextField.text = password;
            
            [self logIn:email password:password];
        }
    }
}

- (void)logIn:(NSString *)email password:(NSString *)password {
    [self showAnimateComponent];
    
    self.loginTextField.enabled = NO;
    self.passwordTextField.enabled = NO;
    
    __block LoginViewController* _self = self;
    [KMBUser logIn:email withPassword:password withCompletionBlock:^(NSError *error, id result) {
        if (!error) {
            KMBLoginResponse *response = (KMBLoginResponse *)result;
            NSString *password = [[KMBSecurity userSecurity] keyChainPasswordWithLogin:response.email];
            if (password == nil || ![password isEqualToString:response.password]) {
                [[KMBSecurity userSecurity] setKeychainPassword:response.password forLogin:response.email];
            }
            
            [_self performSegueWithIdentifier:@"showMainScreenSegue" sender:_self];
        } else {
            if (error.code == KMBRequestErrorLogin) {
                [_self.loginTextField shake];
                [_self.passwordTextField shake];
            }
            
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [errorAlert show];
            
            [_self hideAnimateComponent];
            self.loginTextField.enabled = YES;
            self.passwordTextField.enabled = YES;
        }
    }];
}

- (IBAction)passwordTextFieldTouch:(id)sender {
    NSString *email = self.loginTextField.text;
    if (email.length) {
        NSString *password = [[KMBSecurity userSecurity] keyChainPasswordWithLogin:email];
        if (password.length > 0) {
            self.passwordTextField.text = password;
        }
    }
}

- (IBAction)loginButtonTouched:(RoundedButton *)sender {
    BOOL result = YES;
    if (self.loginTextField.text.length <= 0) {
        result = NO;
        [self.loginTextField shake];
        
    }
    if (self.passwordTextField.text.length <= 0) {
        result = NO;
        [self.passwordTextField shake];
    }
    if (!result)
        return;
    
    [self logIn:self.loginTextField.text password:self.passwordTextField.text];
}

- (void)showAnimateComponent {
    self.animatingIndicator.hidden = NO;
    [UIView animateWithDuration:.3 animations:^{
        [self.animatingIndicator startAnimating];
        self.animatingIndicator.alpha = 1.;
        self.loginButton.alpha = 0.;
    } completion:^(BOOL finished) {
        if (finished) {
            self.loginButton.hidden = YES;
        }
    }];
}

- (void)hideAnimateComponent {
    self.loginButton.hidden = NO;
    [UIView animateWithDuration:.3 animations:^{
        self.animatingIndicator.alpha = 0.;
        self.loginButton.alpha = 1.;
    } completion:^(BOOL finished) {
        if (finished) {
            self.animatingIndicator.hidden = YES;
            [self.animatingIndicator stopAnimating];
        }
    }];
}

#pragma mark - Text field delegate methods

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        if (nextResponder == self.loginButton) {
//            [textField resignFirstResponder];
            [self loginButtonTouched:(RoundedButton *)self.loginButton];
            
            return NO;
        } 
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.passwordTextField) {
        NSString *email = self.loginTextField.text;
        if (email.length) {
            NSString *password = [[KMBSecurity userSecurity] keyChainPasswordWithLogin:email];
            if (password) {
                self.passwordTextField.text = password;
            }
        }
    }
}

#pragma mark - Notifications

- (void)keyboardWillShowNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.bottomConstraint.constant = 16 + size.height;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHideNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.bottomConstraint.constant = self.bottomConstraintConstant;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillChangeFrameNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.bottomConstraint.constant = 16 + size.height;
        
        [self.view layoutIfNeeded];
    }];
}

@end
