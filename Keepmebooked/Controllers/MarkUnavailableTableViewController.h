//
//  MarkUnavailableTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MarkUnavailableTableViewController;

@protocol MarkUnavailableDelegate <NSObject>

- (void)markUnavailableDone:(MarkUnavailableTableViewController *)controller;
- (void)markUnavailableCancel:(MarkUnavailableTableViewController *)controller;

@end

@interface MarkUnavailableTableViewController : UITableViewController

@property (weak) id<MarkUnavailableDelegate> delegate;
@property NSInteger numberOfGuests;
@property NSDate *startDate;
@property NSDate *endDate;

@property NSMutableArray *availabilities;
//@property (nonatomic) NSMutableArray *unavailable;

@end
