//
//  CheckAvailabilityTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CheckAvailabilityTableViewController.h"

#import "CheckAvailabilityTableViewCell.h"
#import "BookingContactViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBAvailability.h"
#import "BookingDetailViewController.h"

@interface CheckAvailabilityTableViewController () <BookingContactDelegate, BookingDetailDelegate>
@property NSMutableArray *availabilities;
@end

@implementation CheckAvailabilityTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self loadAvailableRooms];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Check availability" properties:@{}];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (KMBAvailability *)findUnavailabile:(KMBAvailability *)availability {
    for (KMBAvailability *unavailable in [KMBBookings bookings].unavailableRooms) {
        if (unavailable.addBooking.roomId == availability.addBooking.roomId) {
            return unavailable;
        }
    }
    
    return nil;
}

- (void)loadAvailableRooms {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block CheckAvailabilityTableViewController *_self = self;
    [[KMBBookings bookings] loadAvailableRoomsForNumberOfGuests:self.numberOfGuests startDate:self.startDate endDate:self.endDate withView:self.view withCompletionBlock:^(id result) {
        _self.availabilities = [NSMutableArray new];
        for (KMBAvailability *availability in result) {
            if (![self findUnavailabile:availability])
                [_self.availabilities addObject:availability];
        }
        [_self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showBookingContactCheckSegue"]) {
        UINavigationController *destNav = segue.destinationViewController;
        BookingContactViewController *dst = destNav.viewControllers.firstObject;
        dst.delegate = self;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        KMBAvailability *availability = self.availabilities[indexPath.row];
        dst.availability = availability;
    } else if ([segue.identifier isEqualToString:@"showBookingDetailNewBookingSegue"]) {
        UINavigationController *dstNav = segue.destinationViewController;
        BookingDetailViewController *detail = dstNav.viewControllers.firstObject;
        
        detail.showCloseButton = YES;
        detail.delegate = self;
        detail.bookingId = [sender[@"bookingId"] integerValue];
        detail.roomName = sender[@"roomName"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.availabilities.count == 0) {
        return 1;
    }
    return self.availabilities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No available rooms";
        
        return cell;
    }
    
    CheckAvailabilityTableViewCell *cell;
    
    KMBAvailability *info = self.availabilities[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CheckAvailabilityCell" forIndexPath:indexPath];
        
        NSString *room = [info.text substringToIndex:range.location];
        NSString *price = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.priceForLabel.text = price;
        cell.totalPriceLabel.text = [NSString stringWithFormat:@"%@", info.total];
    } else {
        cell.roomLabel.text = info.text;
    }
    
    cell.totalPriceLabel.text = [NSString stringWithFormat:@"%@", info.total];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.availabilities.count == 0)
        return 44;
    
    return 103;
}

#pragma mark - Booking contact delegate methods

- (void)bookingContactCancel:(BookingContactViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)bookingContactSave:(BookingContactViewController *)controller andBookingId:(NSInteger)bookingId{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self loadAvailableRooms];
}

- (void)bookingContactSaveAndView:(BookingContactViewController *)controller andBookingId:(NSInteger)bookingId andRoomName:(NSString *)roomName{
    [self dismissViewControllerAnimated:YES completion:^{
        NSMutableDictionary *sender = @{@"bookingId":@(bookingId)}.mutableCopy;
        if (roomName) {
            [sender setObject:roomName forKey:@"roomName"];
        }
        [self performSegueWithIdentifier:@"showBookingDetailNewBookingSegue" sender:sender];
        
        [self loadAvailableRooms];
    }]; //closed BookingContactViewController
}

#pragma mark Booking detail delegate methods

- (void)bookingDetailDidClosed:(BookingDetailViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
