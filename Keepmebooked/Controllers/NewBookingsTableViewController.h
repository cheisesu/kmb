//
//  NewBookingsTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 27.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBookingsTableViewController : UITableViewController
@property (strong) NSMutableArray *bookings;
//1 = today;
//2 = tomorrow
@property NSInteger dashBoardPageIndex;
@end
