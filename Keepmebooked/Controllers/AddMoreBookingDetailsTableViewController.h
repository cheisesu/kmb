//
//  AddMoreBookingDetailsTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 18.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMBBookingInfo.h"

@class AddMoreBookingDetailsTableViewController;

@protocol AddMoreBookingDetailsDelegate <NSObject>

- (void)addMoreBookingDetailsDidHidden:(AddMoreBookingDetailsTableViewController *)controller;

@end

@interface AddMoreBookingDetailsTableViewController : UITableViewController
@property (weak) id<AddMoreBookingDetailsDelegate> delegate;
@property BOOL hasChanged;

@property KMBBookingInfo *bookingInfo;
@property KMBBookingRoomInfo *roomInfo;
@end
