//
//  SearchTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "SearchTableViewController.h"
#import "BookingDetailViewController.h"
#import "SearchTableViewCell.h"
#import "KMBSearch.h"

@interface SearchTableViewController ()<UISearchBarDelegate, BookingDetailDelegate>
@property KMBSearchResult *searchResult;

@property NSString *currentSearchText;
@property NSInteger currentPage;

@property BOOL isLoading;
@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isLoading = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.layer.cornerRadius = 6;
    
    [[SEGAnalytics sharedAnalytics] screen:@"Search" properties:@{}];
}

- (void)nextPage {
    if (self.currentPage >= self.searchResult.totalPages)
        return;
    
    self.currentPage++;
    
    __block SearchTableViewController *_self = self;
    [[KMBSearch search] getResultWithPageNumber:self.currentPage forSearchResult:self.searchResult withCompletionBlock:^(id result) {
        KMBSearchResult *searchResult = result[0];
        [_self.searchResult.result addObjectsFromArray:searchResult.result];
        
        [_self.tableView reloadData];
        
        _self.isLoading = NO;
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchResult.result.count == 0) {
        return 1;
    }
    return self.searchResult.result.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchResult.result.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No results";
        
        return cell;
    }
    
    SearchTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    
    KMBSearchInfo *searchInfo = self.searchResult.result[indexPath.row];
    cell.guestNameLabel.text = searchInfo.guestName;
    cell.checkInDateLabel.text = [NSString stringWithFormat:@"%@", searchInfo.checkInDate.description];
    
    return cell;
}

#pragma mark - Search bar delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    __block SearchTableViewController *_self = self;
    self.currentSearchText = searchText;
    self.isLoading = YES;
    [[KMBSearch search] searchByKeyword:searchText withCompletionBlock:^(id result) {
        if ([searchText isEqualToString:_self.currentSearchText]) {
            _self.searchResult = result[0];
            _self.searchResult.searchText = searchText;
            _self.currentPage = 1;
            
            [_self.tableView reloadData];
            
            _self.isLoading = NO;
        }
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showBookingDetailSearchSegue"]) {
        UINavigationController *dstNav = segue.destinationViewController;
        BookingDetailViewController *detail = dstNav.viewControllers.firstObject;
        
        detail.showCloseButton = YES;
        detail.delegate = self;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        KMBSearchInfo *searchInfo = self.searchResult.result[indexPath.row];
        detail.bookingId = searchInfo.bookingId;
    }
}

#pragma mark Booking detail delegate methods

- (void)bookingDetailDidClosed:(BookingDetailViewController *)controller {
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.caller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Scroll delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y + scrollView.frame.size.height > scrollView.contentSize.height - scrollView.contentSize.height * .2 && !self.isLoading) {
        self.isLoading = YES;
        
        [self nextPage];
    }
}

@end
