//
//  BookingCalendarCollectionViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 14.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "BookingCalendarCollectionViewController.h"

#import "CalendarCollectionView.h"
#import "DateCollectionViewCell.h"
#import "LeftColumnCollectionViewCell.h"
#import "TopLeftCollectionViewCell.h"
#import "ObjectCollectionViewCell.h"

#import "DropDownButton.h"

#import <MBProgressHUD/MBProgressHUD.h>

#import "SettingsViewController.h"
#import "BookingDetailViewController.h"
#import "NewBookingViewController.h"

#import "KMBRequest.h"
#import "KMBUser.h"
#import "KMBLoginResponse.h"
#import "KMBCalendarRoom.h"
#import "KMBCalendarRooms.h"
#import "KMBCalendarBooking.h"
#import "KMBOptions.h"
#import "KMBRoomInfo.h"

#import "AppDelegate.h"
#import "JMGModaly.h"

#import "NSDate+Utils.h"
#import <Intercom/Intercom.h>

NSString *weekDays[] = {
    @"null",
    @"Sun",
    @"Mon",
    @"Tue",
    @"Wed",
    @"Thu",
    @"Fri",
    @"Sat",
};

@interface BookingCalendarCollectionViewController () <SettingViewControllerDelegate, NewBookingDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIView *calendarPickerViewContainer;

@property NSInteger numberOfDays;
@property NSInteger numberOfVisibleRows;

@property (nonatomic, strong) JMGModaly *modalSegue;

@end

@implementation BookingCalendarCollectionViewController
{
    NSMutableArray *rooms;
    
    NSString *monthButtonTitle;
    CGFloat leftFixedPanelWidth;
    
    BOOL isFirstScroll;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hotelDidSwitchedNotification:) name:KMBSwitchHotelNotification object:nil];
    
    [view registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [view registerNib:[UINib nibWithNibName:@"DateCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"DateCell"];
    [view registerNib:[UINib nibWithNibName:@"LeftColumnCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LeftColumnCell"];
    [view registerNib:[UINib nibWithNibName:@"TopLeftCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TopLeftCell"];
    [view registerNib:[UINib nibWithNibName:@"ObjectCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ObjectCell"];

    rooms = [NSMutableArray new];
    
    FSCalendar *calendarPicker = (FSCalendar *)[self.calendarPickerViewContainer.subviews firstObject];
    if (calendarPicker) {
        [self setMonthForTitle:calendarPicker.currentPage];
        calendarPicker.delegate = self;
    }
    
    isFirstScroll = YES;
    
    self.numberOfDays = 3;
    
    leftFixedPanelWidth = view.leftFixedPanelWidth;
    
    [self changeCellsSizesForNumberOfDays:self.numberOfDays needReload:NO];
    
    [self initialLoadRooms];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Calendar" properties: @{
                                                                      }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.collectionView setNeedsDisplay];
}

- (void)getBegin:(NSDate **)begin endDate:(NSDate **)end forDate:(NSDate *)date {
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* beginDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    [beginDateComp setCalendar:calendar];
    *begin = [beginDateComp date];
    *end = [*begin dateByAddingTimeInterval:(self.numberOfDays - 1) * CCVNumberOfSecondsInDay];
}

- (KMBCalendarRoom *)findRoomForId:(NSInteger)roomId {
    for (KMBCalendarRoom *room in rooms) {
        if (room.roomId == roomId)
            return room;
    }
    
    return nil;
}

- (void)initialLoadRooms {
    __block BookingCalendarCollectionViewController *_self = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSDate *begin = nil;
    NSDate *end = nil;
    [self getBegin:&begin endDate:&end forDate:[NSDate date]];
    begin = [begin dateByAddingTimeInterval:-7 * CCVNumberOfSecondsInDay];
    begin = [begin dateByAddingTimeInterval:7 * CCVNumberOfSecondsInDay];
    [[KMBCalendarRooms calendarRooms] loadRoomsFromDate:begin toDate:end withView:self.view withCompletionBlock:^(id result) {
        rooms = [result mutableCopy];
        
        [_self.collectionView reloadData];
        [_self changeCellsSizesForNumberOfDays:self.numberOfDays needReload:YES];
        
        CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)_self.collectionView.collectionViewLayout;
        [layout setContentOffsetToDate:[NSDate date] animated:YES];
        [_self.collectionView setNeedsDisplay];
        
        isFirstScroll = NO;
    }];
}

//returns inserted index
- (NSInteger)insertBooking:(KMBCalendarBooking *)booking toBarCollection:(NSMutableArray *)barCollection {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.bookingId = %ld", booking.bookingId];
    NSArray *filteredArray = [barCollection filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count == 0) {
        [barCollection addObject:booking];
        return barCollection.count - 1;
    }
    
    return -1;
}

- (void)loadRoomsForStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate {
    __block BookingCalendarCollectionViewController *_self = self;
    [[KMBCalendarRooms calendarRooms] loadRoomsFromDate:startDate toDate:endDate withView:nil withCompletionBlock:^(id result) {
        //TODO: synch romms' lines, but for now they are the same
        NSArray *tempRooms = [result mutableCopy];
        
        for (NSInteger roomIndex = 0; roomIndex < tempRooms.count; roomIndex++) {
            KMBCalendarRoom *tempRoom = tempRooms[roomIndex];
            KMBCalendarRoom *thisRoom = rooms[roomIndex];
            
            for (KMBCalendarBooking *booking in tempRoom.barCollection) {
                NSInteger insertedIndex = [self insertBooking:booking toBarCollection:thisRoom.barCollection];
                if (insertedIndex > -1) {
                    thisRoom.barCounter++;
                }
            }
        }
        [_self.collectionView reloadData];
        [_self.collectionView setNeedsDisplay];
        [_self.collectionView.collectionViewLayout invalidateLayout];
    }];
}

- (void)loadRoomsForDate:(NSDate *)date {
    __block BookingCalendarCollectionViewController *_self = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[KMBCalendarRooms calendarRooms] loadRoomsFromDate:date toDate:[date dateByAddingTimeInterval:(self.numberOfDays - 1) * CCVNumberOfSecondsInDay] withView:self.view withCompletionBlock:^(id result) {
        rooms = [result mutableCopy];
            
        BOOL hasBookings = NO;
        for (KMBCalendarRoom *room in rooms) {
            if (room.barCounter > 0) {
                hasBookings = YES;
                break;
            }
        }
        
        if (!hasBookings) {
            [rooms removeAllObjects];
        }
        
        [_self.collectionView reloadData];
        [_self.view layoutIfNeeded];
        [_self changeCellsSizesForNumberOfDays:self.numberOfDays needReload:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)monthSelectButtonTouched:(id)sender {
    [self showHideCalendar];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
}

- (void)showHideCalendar {
    __block BookingCalendarCollectionViewController *_self = self;
    __block BOOL willShow = NO;
    
    [UIView animateWithDuration:.3 animations:^{
        if (_self.calendarPickerViewContainer.hidden) {
            _self.calendarPickerViewContainer.hidden = NO;
            willShow = YES;
            _self.topConstraint.constant = 0;
            _self.monthSelectButton.downed = NO;
        } else {
            _self.topConstraint.constant = _self.calendarPickerViewContainer.frame.size.height;
            [_self.collectionView setNeedsDisplay];
            _self.monthSelectButton.downed = YES;
            
            FSCalendar *calendarPicker = (FSCalendar *)[_self.calendarPickerViewContainer.subviews firstObject];
            if (calendarPicker) {
                CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)view.collectionViewLayout;
                NSDate *date = layout.currentDate;
                calendarPicker.currentPage = date;
            }
        }
        
        [_self.view layoutIfNeeded];
        [_self.monthSelectButton layoutSubviews];
    } completion:^(BOOL finished) {
        if (finished) {
            if (!willShow)
                _self.calendarPickerViewContainer.hidden = YES;
            else
                [_self.collectionView setNeedsDisplay];
            
            [_self changeCellsSizesForNumberOfDays:_self.numberOfDays needReload:YES];
        }
    }];
}

- (void)hideCalendar {
    __block BookingCalendarCollectionViewController *_self = self;
    
    [UIView animateWithDuration:.3 animations:^{
        _self.topConstraint.constant = _self.calendarPickerViewContainer.frame.size.height;
        [_self.collectionView setNeedsDisplay];
        _self.monthSelectButton.downed = YES;
        
        [_self.view layoutIfNeeded];
        [_self.monthSelectButton layoutSubviews];
        
        FSCalendar *calendarPicker = (FSCalendar *)[_self.calendarPickerViewContainer.subviews firstObject];
        if (calendarPicker) {
            CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)view.collectionViewLayout;
            NSDate *date = layout.currentDate;
            calendarPicker.currentPage = date;
        }
    } completion:^(BOOL finished) {
        if (finished) {
            _self.calendarPickerViewContainer.hidden = YES;
            [_self changeCellsSizesForNumberOfDays:_self.numberOfDays needReload:YES];
        }
    }];
}

- (void)setMonthForTitle:(NSDate *)month {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"LLLL"];
    
    [self.monthSelectButton setTitle:[formatter stringFromDate:month] forState:UIControlStateNormal];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showSettingsSegue"]) {
        JMGModaly *popupSegue = (JMGModaly *)segue;
        self.modalSegue = popupSegue;
        
        CGRect destinationBounds = self.view.bounds;
        destinationBounds.size.width -= 40;
        
        SettingsViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.view.frame = destinationBounds;
        [dst.view layoutSubviews];
        
    } else if ([segue.identifier isEqualToString:@"showBookingDetailSegue"]) {
        BookingDetailViewController *dst = segue.destinationViewController;
        dst.bookingId = [sender[@"bookingId"] integerValue];
        dst.roomName = sender[@"roomName"];
        
    } else if ([segue.identifier isEqualToString:@"calendarViewAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
    }
}

- (void)changeCellsSizesForNumberOfDays:(NSInteger)numberOfDays needReload:(BOOL)needReload {
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)view.collectionViewLayout;
    if (numberOfDays > 3) {
        view.leftFixedPanelWidth = leftFixedPanelWidth - .2 * leftFixedPanelWidth;
    } else {
        view.leftFixedPanelWidth = leftFixedPanelWidth;
    }
    layout.leftFixedPanelWidth = view.leftFixedPanelWidth;
    
    NSInteger tempNumberOfDays = numberOfDays;
    NSInteger numOfElements = [layout numberOfItemsInTopFixedPanel];
    tempNumberOfDays = MIN(tempNumberOfDays, numOfElements - layout.hasLeftFixedPanel);
    
    if (tempNumberOfDays == 0)
        tempNumberOfDays = numberOfDays;
    
    CGFloat topCellsWidth = (self.view.frame.size.width - view.leftFixedPanelWidth) / tempNumberOfDays;
    CGFloat leftCellsHeight = (self.view.frame.size.height - view.topFixedPanelHeight);
    
    NSInteger numberOfVisibleRows = leftCellsHeight / view.leftFixedPanelCellHeight;
    numberOfVisibleRows = MIN(numberOfVisibleRows, rooms.count);
    if (numberOfVisibleRows != 0 && leftCellsHeight - (float)numberOfVisibleRows * view.leftFixedPanelCellHeight > 0) {
        leftCellsHeight /= (float)numberOfVisibleRows;
    } else {
        leftCellsHeight = view.leftFixedPanelCellHeight;
    }
    
    self.numberOfVisibleRows = numberOfVisibleRows;
    
    view.topFixedPanelCellWidth = topCellsWidth;
    layout.topFixedPanelCellWidth = topCellsWidth;
    layout.leftFixedPanelCellHeight = leftCellsHeight;
    
    self.numberOfDays = numberOfDays;
    
    [layout invalidateLayout];
}

- (NSDate *)lastVisibleDate {
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)self.collectionView.collectionViewLayout;
    NSInteger day = [layout numberOfDayForXPoint:self.collectionView.contentOffset.x + layout.leftFixedPanelWidth];
    NSDate *dayDate = [layout dateForDayNumber:day];
    dayDate = [dayDate dateByAddingTimeInterval:self.numberOfDays * CCVNumberOfSecondsInDay];
    return dayDate;
}

#pragma mark - Static

+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if (beginDate == nil || endDate == nil)
        return NO;
    
    if ([date compare:beginDate] == NSOrderedSame || [date compare:endDate] == NSOrderedSame)
        return YES;
    
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

#pragma mark - Calendar collection view layout delegate methods

- (void)calendarCollectionView:(CalendarCollectionView *)cv didSelectObjectAtIndexPath:(CCIndexPath *)indexPath {
    KMBCalendarRoom *room = rooms[indexPath.row];
    KMBCalendarBooking *booking = room.barCollection[indexPath.column];
    NSMutableDictionary *sender = @{@"bookingId":@(booking.bookingId)}.mutableCopy;
    if (room.name) {
        [sender setObject:room.name forKey:@"roomName"];
    }
    [self performSegueWithIdentifier:@"showBookingDetailSegue" sender:sender];
}

- (void)calendarCollectionView:(CalendarCollectionView *)cv currentDate:(NSDate *)date {
    FSCalendar *calendarPicker = (FSCalendar *)[self.calendarPickerViewContainer.subviews firstObject];
    if (calendarPicker) {
        calendarPicker.currentPage = date;
    }
}

#pragma mark - CalendarCollectionViewLayoutDataSource methods

- (NSInteger)numberOfRowsForCalendarCollectionView:(CalendarCollectionView *)cv {
    return rooms.count;
}

- (NSInteger)calendarCollectionView:(CalendarCollectionView *)cv numberOfObjectsInRow:(NSInteger)row {
    KMBCalendarRoom *room = rooms[row];
    return room.barCollection.count;
}

- (CCObjectInfo *)calendarCollectionView:(CalendarCollectionView *)cv objectInfoForCellIndexPath:(CCIndexPath *)indexPath {
    KMBCalendarRoom *room = rooms[indexPath.row];
    KMBCalendarBooking *booking = room.barCollection[indexPath.column];
    CCObjectInfo *objectInfo = [CCObjectInfo new];
    objectInfo.beginDateTime = [booking.checkInDate dateByAddingTimeInterval:14. * 60. * 60.];//TODO: booking.checkInDate.toLocalTime;
    NSDate *checkOut = [booking.checkOutDate dateByAddingTimeInterval:11. * 60. * 60.];
    objectInfo.duration = [checkOut timeIntervalSinceDate:objectInfo.beginDateTime];//[booking.checkOutDate timeIntervalSinceDate:booking.checkInDate];
    
    return objectInfo;
}

- (UICollectionViewCell *)topLeftFixedCellForCalendarCollectionView:(CalendarCollectionView *)cv {
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"TopLeftCell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    return cell;
}

- (UICollectionViewCell *)calendarCollectionView:(CalendarCollectionView *)cv cellForTopFixedPanelWithDate:(NSDate *)date andColumnIndex:(NSInteger)column{
    DateCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"DateCell" forIndexPath:[NSIndexPath indexPathForRow:column + 1 inSection:0]];
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDateComponents* dateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal fromDate:date];
    [dateComp setCalendar:calendar];
    
    cell.dayLabel.text = [NSString stringWithFormat:@"%@", @(dateComp.day)];
    long day = dateComp.weekday;
    cell.descritionOfDayLabel.text = weekDays[day];
    cell.separatorColor = view.linesColor;
    
    return cell;
}

- (UICollectionViewCell *)calendarCollectionView:(CalendarCollectionView *)cv cellForLeftFixedPanelWithRow:(NSInteger)row {
    LeftColumnCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"LeftColumnCell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:row + 1]];
    KMBCalendarRoom *room = rooms[row];
    cell.roomLabel.text = room.name;
    
    return cell;
}

- (UICollectionViewCell *)calendarCollectionView:(CalendarCollectionView *)cv cellForIndexPath:(CCIndexPath *)indexPath {
    ObjectCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ObjectCell" forIndexPath:[NSIndexPath indexPathForRow:indexPath.column + 1 inSection:indexPath.row + 1]];

    KMBCalendarRoom *roomInfo = rooms[indexPath.row];
    KMBCalendarBooking *barInfo = roomInfo.barCollection[indexPath.column];
    cell.bookingInfo = barInfo;
    cell.layout = (CalendarCollectionViewLayout *)cv.collectionViewLayout;
    
    [cell setNeedsDisplay];
    
    return cell;
}

- (void)loadDataToEarlierForCalendarCollectionView:(CalendarCollectionView *)cv withCompletionBlock:(nonnull CCVCompletionLoadingBlock)completion {
    if (isFirstScroll)
        return;
    
    //which days show on the screen
    NSDate *lastDate = self.lastVisibleDate;
    //next 7 days
    NSDate *endDate = [lastDate dateByAddingTimeInterval:-self.numberOfDays * CCVNumberOfSecondsInDay];
    NSDate *startDate = [endDate dateByAddingTimeInterval:-7 * CCVNumberOfSecondsInDay];

    [self loadRoomsForStartDate:startDate andEndDate:endDate];
    
    completion();
}

- (void)loadDataToLaterForCalendarCollectionView:(CalendarCollectionView *)cv withCompletionBlock:(nonnull CCVCompletionLoadingBlock)completion {
    if (isFirstScroll)
        return;
    
    //which days show on the screen
    NSDate *lastDate = self.lastVisibleDate;
    //next 7 days
    NSDate *startDate = lastDate;
    NSDate *endDate = [lastDate dateByAddingTimeInterval:7 * CCVNumberOfSecondsInDay];

    [self loadRoomsForStartDate:startDate andEndDate:endDate];
    
    completion();
}

#pragma mark - FSCalendar delegate methods

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date {
    NSCalendar* gcalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [gcalendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDateComponents* dateComp = [gcalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    [dateComp setCalendar:gcalendar];
    NSDateComponents* dateCompHour = [gcalendar components:NSCalendarUnitHour fromDate:date];
    date = [dateComp date];
    if (dateCompHour.hour > 0)
        date = [date dateByAddingTimeInterval:CCVNumberOfSecondsInDay];
    
    [self loadRoomsForStartDate:[date dateByAddingTimeInterval:-7 * CCVNumberOfSecondsInDay] andEndDate:date];
    [self loadRoomsForStartDate:date andEndDate:[date dateByAddingTimeInterval:7 * CCVNumberOfSecondsInDay]];
    
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)self.collectionView.collectionViewLayout;
    [layout setContentOffsetToDate:date animated:YES];
    
    [self hideCalendar];
}

- (void)calendarCurrentMonthDidChange:(FSCalendar *)calendar {
    [self setMonthForTitle:calendar.currentMonth];
}

- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date
{
    return YES;
}

- (IBAction)supportButtonTouched:(id)sender {
    [Intercom presentMessageComposer];
}

#pragma mark - Settings view controller delegate

- (void)settingsViewController:(SettingsViewController *)svc didSelectNumberOfDays:(NSInteger)numberOfDays {
    self.numberOfDays = numberOfDays;
    NSDate *date = self.lastVisibleDate;
    date = [date dateByAddingTimeInterval:-self.numberOfDays * CCVNumberOfSecondsInDay];
    
    switch (self.numberOfDays) {
        case 1:
            [[SEGAnalytics sharedAnalytics] screen:@"Calendar 1 day" properties: @{}];
            break;
        case 3:
            [[SEGAnalytics sharedAnalytics] screen:@"Calendar 3 day" properties: @{}];
            break;
        case 7:
            [[SEGAnalytics sharedAnalytics] screen:@"Calendar week" properties: @{}];
            break;
            
        default:
            break;
    }
    
    [self changeCellsSizesForNumberOfDays:numberOfDays needReload:NO];
    CalendarCollectionViewLayout *layout = (CalendarCollectionViewLayout *)self.collectionView.collectionViewLayout;
    [layout setContentOffsetToDate:date animated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Notifications handlers

- (void)hotelDidSwitchedNotification:(NSNotification *)notif {
    isFirstScroll = YES;
    
    [self initialLoadRooms];
}

@end
