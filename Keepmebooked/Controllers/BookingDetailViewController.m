//
//  BookingDetailViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "BookingDetailViewController.h"
#import "DropDownButton.h"
#import "KMBCalendarBooking.h"
#import "AddMoreBookingDetailsTableViewController.h"
#import "KMBBookings.h"
#import "AppDelegate.h"
#import "KMBUser.h"
#import "KMBBookingInfo.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBBookingCheckInOut.h"
#import "UITextField+Shake.h"
#import "KMBBookingUpdateSuccess.h"
#import "NSDate+Utils.h"
#import "RoundedButton.h"
#import "ImagedStepper.h"
#import "BorderedButton.h"
#import "NewBookingViewController.h"
#import "JMGModaly.h"
#import "BookingEditAvailableRoomsTableViewController.h"

@interface BookingDetailViewController () <UIAlertViewDelegate, AddMoreBookingDetailsDelegate, BookingEditAvailableRoomsDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailsTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentsHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkInDateViewConstraint;
@property (weak, nonatomic) IBOutlet DropDownButton *clientNameExpandableButton;
@property (weak, nonatomic) IBOutlet UIButton *checkInOutButton;
@property (weak, nonatomic) IBOutlet UILabel *totalCoastLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceDueLabel;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (weak, nonatomic) IBOutlet RoundedButton *addMoreDetailButton;
@property (weak, nonatomic) IBOutlet ImagedStepper *numberOfGuestsStepper;
@property (weak, nonatomic) IBOutlet UILabel *numberOfGuestsEditorLabel;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *numberOfGuestsLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkInDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *mailTextField;
@property (weak, nonatomic) IBOutlet UITextView *commentsTextView;

@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *mailLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;
@property (weak, nonatomic) IBOutlet BorderedButton *checkInButton;
@property (weak, nonatomic) IBOutlet BorderedButton *checkOutButton;
@property (weak, nonatomic) IBOutlet UILabel *checkOutLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkInLabel;
@property (weak, nonatomic) IBOutlet UIButton *roomNameButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property BOOL isCheckedIn;

@property KMBBookingInfo *bookingInfo;
@property CGFloat defaultDetailCellHeight;

@property BOOL isUpdating;

@property NSInteger pickerType;

@property NSDate *checkInDate;
@property NSDate *checkOutDate;

@property NSDateFormatter *titleDateFormatter;

@property JMGModaly *modalSegue;
@property BOOL isRoomShowsFromDone;
@property KMBNewBooking *selectedRoom;

@property UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property BOOL isControlsUpdated;
@end

@implementation BookingDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultDetailCellHeight = self.commentsHeightConstraint.constant;
    self.isCheckedIn = NO;
    self.detailsTopConstraint.constant = -self.detailsView.frame.size.height;
    self.isUpdating = NO;
    self.isRoomShowsFromDone = NO;
    
    self.pickerType = NBPickerTypeNo;
    self.pickerBottomConstraint.constant = -self.datePickerView.bounds.size.height;
    self.datePickerView.hidden = YES;
    
    self.titleDateFormatter = [NSDateFormatter new];
    [self.titleDateFormatter setDateFormat:@"EEE dd LLL"];
    
    self.selectedRoom = nil;
    
    if (self.showCloseButton) {
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close"] style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTouched:)];
        self.navigationItem.leftBarButtonItem = closeButton;
    } else {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotificationHandler:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotificationHandler:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotificationHandler:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [self loadBookingInfo];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(loadBookingInfo) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:self.refreshControl];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadBookingInfo {
    self.isControlsUpdated = NO;
    
    if (self.bookingId <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Wrong booking id was sended" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        if (self.showCloseButton) {
            [self closeButtonTouched:self];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        return;
    }
    
    UIView *view = nil;
    if (self.refreshControl.isRefreshing) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        view = self.view;
    }
    
    __block BookingDetailViewController *_self = self;
    [[KMBBookings bookings] loadBookingById:self.bookingId withView:view withCompletionBlock:^(id result) {
        _self.bookingInfo = ((NSArray *)result).firstObject;
        if (![_self.bookingInfo.bookingSummary.guestStatus isEqualToString:KMBBSGuestStatusCheckedIn]) {
            _self.checkInOutButton.backgroundColor = [UIColor colorFromRed:136 green:192 blue:87 alpha:255];
            [_self.checkInOutButton setTitle:@"CHECK IN" forState:UIControlStateNormal];
            _self.isCheckedIn = NO;
        } else {
            _self.checkInOutButton.backgroundColor = [UIColor colorFromRed:176 green:0 blue:0 alpha:255];
            [_self.checkInOutButton setTitle:@"CHECK OUT" forState:UIControlStateNormal];
            _self.isCheckedIn = YES;
        }
        
        [_self hideIndicator];
        [_self updateControlls];
    }];
    
    [self.refreshControl endRefreshing];
}

- (void)hideIndicator {
    self.checkInOutButton.hidden = NO;
    [UIView animateWithDuration:.3 animations:^{
        self.activityIndicator.alpha = 0.;
        self.checkInOutButton.alpha = 1.;
    } completion:^(BOOL finished) {
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];
    }];
}

- (void)updateCommentsLabelHeight {
    //Calculate the expected size based on the font and linebreak mode of your label
    // FLT_MAX here simply means no constraint in height
    CGSize maximumLabelSize = CGSizeMake(self.commentsLabel.frame.size.width, FLT_MAX);
    CGFloat newConstraintHeight = 2. * 16.;
    
    NSDictionary *attributes = @{NSFontAttributeName:self.commentsLabel.font};
    CGRect boundingRect = [self.commentsLabel.text boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    //adjust the label the the new height.
    CGRect newFrame = self.commentsLabel.frame;
    newFrame.size.height = boundingRect.size.height;
    self.commentsLabel.frame = newFrame;
    
    newConstraintHeight += newFrame.size.height;
    self.commentsHeightConstraint.constant = MAX(newConstraintHeight, self.defaultDetailCellHeight);
}

- (KMBBookingRoomInfo *)getRoomFromBooking {
    for (KMBBookingRoomInfo *roomInfo in self.bookingInfo.rooms) {
        if ([roomInfo.name isEqualToString:self.roomName]) {
            return roomInfo;
        }
    }
    
    return self.bookingInfo.rooms.firstObject;
}

- (void)showDatePicker {
    self.datePickerView.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.datePickerView.alpha = 1.;
        self.pickerBottomConstraint.constant = 0;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        ;
    }];
}

- (void)hideDatePicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.datePickerView.alpha = 0;
        self.pickerBottomConstraint.constant = -self.datePickerView.bounds.size.height;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.datePickerView.hidden = YES;
    }];
}

- (void)updateDateButtonTitles {
    [self.checkInButton setTitle:[self.titleDateFormatter stringFromDate:self.checkInDate] forState:UIControlStateNormal];
    [self.checkOutButton setTitle:[self.titleDateFormatter stringFromDate:self.checkOutDate] forState:UIControlStateNormal];
}

- (void)updateControlls {
    self.totalCoastLabel.text = [NSString stringWithFormat:@"%@%.2f", self.bookingInfo.currencyCode, self.bookingInfo.bookingSummary.totalCoast];
    self.taxLabel.text = [NSString stringWithFormat:@"%@%.2f", self.bookingInfo.currencyCode, self.bookingInfo.bookingSummary.tax];
    self.paidLabel.text = [NSString stringWithFormat:@"%@%.2f", self.bookingInfo.currencyCode, self.bookingInfo.bookingSummary.paid];
    self.balanceDueLabel.text = [NSString stringWithFormat:@"%@%.2f", self.bookingInfo.currencyCode, self.bookingInfo.bookingSummary.balanceDue];
    
    KMBBookingRoomInfo *roomInfo = [self getRoomFromBooking];
    NSString *roomName = roomInfo.name;
    
    self.roomNameLabel.text = roomName ? roomName : @"Unknown room name";
    [self.roomNameButton setTitle:self.roomNameLabel.text forState:UIControlStateNormal];
    self.numberOfGuestsLabel.text = [NSString stringWithFormat:@"%ld guests", (long)self.bookingInfo.bookingSummary.numberOfGuests];
    self.numberOfGuestsStepper.currentValue = (long)self.bookingInfo.bookingSummary.numberOfGuests;
    self.phoneNumberTextField.text = self.bookingInfo.guest.phoneCell;
    self.mailTextField.text = self.bookingInfo.guest.email;
    self.commentsTextView.text = self.bookingInfo.bookingSummary.note;
    
    self.phoneNumberLabel.text = self.phoneNumberTextField.text;
    self.mailLabel.text = self.mailTextField.text;
    self.commentsLabel.text = self.commentsTextView.text;
    
    [self updateCommentsLabelHeight];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    self.checkInDateLabel.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:self.bookingInfo.bookingSummary.checkIn.toLocalTime], [formatter stringFromDate:self.bookingInfo.bookingSummary.checkOut.toLocalTime]];
    
    self.checkInDate = self.bookingInfo.bookingSummary.checkIn.toLocalTime;
    self.checkOutDate = self.bookingInfo.bookingSummary.checkOut.toLocalTime;
    [self updateDateButtonTitles];
    
    self.title = roomInfo.name;
    [self.clientNameExpandableButton setTitle:self.bookingInfo.guest.fullName forState:UIControlStateNormal];
    
    [self.detailsView layoutIfNeeded];
    if (self.detailsTopConstraint.constant != 0)
        self.detailsTopConstraint.constant = -self.detailsView.frame.size.height;
    
    [self.view layoutIfNeeded];
    
    self.isControlsUpdated = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clientNameExpandableButtonTouched:(id)sender {
    __block BookingDetailViewController *_self = self;
    [UIView animateWithDuration:.3 animations:^{
        if (_self.detailsTopConstraint.constant != 0) {
            //show
            _self.detailsTopConstraint.constant = 0;
            _self.clientNameExpandableButton.downed = NO;
        } else {
            //hide
            _self.detailsTopConstraint.constant = -_self.detailsView.frame.size.height;
            _self.clientNameExpandableButton.downed = YES;
        }
        
        [_self.view layoutIfNeeded];
    }];
}

- (IBAction)checkInOutButtonTouched:(UIButton *)sender {
    if (self.isCheckedIn) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Confirm checkout" message:@"Do you want to check out this guest?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alertView show];
    } else {
        [self checkIn];
    }
}

- (void)checkIn {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block BookingDetailViewController *_self = self;
    [[KMBBookings bookings] checkInBookingsByIds:@[@(self.bookingInfo.bookingSummary.identifier)] withView:self.view withCompletionBlock:^(id result) {
        BOOL all = YES;
        for (KMBBookingCheckInOut *info in result) {
            if (![info.status isEqualToString:@"checked_in"]) {
                all = NO;
                
                break;
            }
        }
        if (all) {
            _self.checkInOutButton.backgroundColor = [UIColor colorFromRed:176 green:0 blue:0 alpha:255];
            [_self.checkInOutButton setTitle:@"CHECK OUT" forState:UIControlStateNormal];
            _self.isCheckedIn = YES;
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check in" message:@"Not all bookings checked in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)checkOut {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block BookingDetailViewController *_self = self;
    [[KMBBookings bookings] checkOutBookingsByIds:@[@(self.bookingInfo.bookingSummary.identifier)] withView:self.view withCompletionBlock:^(id result) {
        BOOL all = YES;
        for (KMBBookingCheckInOut  *info in result) {
            if (![info.status isEqualToString:@"checked_out"]) {
                all = NO;
                
                break;
            }
        }
        if (all) {
            _self.checkInOutButton.backgroundColor = [UIColor colorFromRed:136 green:192 blue:87 alpha:255];
            [_self.checkInOutButton setTitle:@"CHECK IN" forState:UIControlStateNormal];
            _self.isCheckedIn = NO;
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check out" message:@"Not all bookings checked out" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addMoreDetailsSegue"]) {
        AddMoreBookingDetailsTableViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.bookingInfo = self.bookingInfo;
        dst.roomInfo = [self getRoomFromBooking];
    } else if ([segue.identifier isEqualToString:@"showAvailableRoomsBookingDetailsSegue"]) {
        JMGModaly *popupSegue = (JMGModaly *)segue;
        self.modalSegue = popupSegue;
        
        [popupSegue setDismissBlock:^{
        }];
        
        BookingEditAvailableRoomsTableViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.startDate = self.checkInDate;
        dst.endDate = self.checkOutDate;
        dst.numberOfGuests = self.numberOfGuestsStepper.currentValue;
        
        CGRect destinationBounds = self.view.bounds;
        destinationBounds.size.width -= 40;
        
        dst.view.frame = destinationBounds;
        [dst.view layoutSubviews];
        
        self.isRoomShowsFromDone = YES;
    } else if ([segue.identifier isEqualToString:@"showAvailableRoomsBookingDetailsSelectSegue"]) {
        JMGModaly *popupSegue = (JMGModaly *)segue;
        self.modalSegue = popupSegue;
        
        [popupSegue setDismissBlock:^{
        }];
        
        BookingEditAvailableRoomsTableViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.startDate = self.checkInDate;
        dst.endDate = self.checkOutDate;
        dst.numberOfGuests = self.numberOfGuestsStepper.currentValue;
        
        CGRect destinationBounds = self.view.bounds;
        destinationBounds.size.width -= 40;
        
        dst.view.frame = destinationBounds;
        [dst.view layoutSubviews];
        
        self.isRoomShowsFromDone = NO;
    }
}

- (void)closeButtonTouched:(id)sender {
    if ([self.delegate respondsToSelector:@selector(bookingDetailDidClosed:)]) {
        [self.delegate bookingDetailDidClosed:self];
    }
}

- (void)prepareForEditing {
    self.clientNameExpandableButton.enabled = NO;
    self.addMoreDetailButton.enabled = NO;
    self.checkInOutButton.enabled = NO;
    
    self.phoneNumberTextField.hidden = NO;
    self.mailTextField.hidden = NO;
    self.commentsTextView.hidden = NO;
    self.numberOfGuestsEditorLabel.hidden = NO;
    self.numberOfGuestsStepper.hidden = NO;
    self.checkInButton.hidden = NO;
    self.checkOutButton.hidden = NO;
    self.checkInLabel.hidden = NO;
    self.checkOutLabel.hidden = NO;
    self.roomNameButton.hidden = NO;
    
    [UIView animateWithDuration:.2 animations:^{
        self.phoneNumberTextField.alpha = 1.;
        self.mailTextField.alpha = 1.;
        self.commentsTextView.alpha = 1.;
        self.numberOfGuestsEditorLabel.alpha = 1.;
        self.numberOfGuestsStepper.alpha = 1.;
        self.checkInButton.alpha = 1.;
        self.checkOutButton.alpha = 1.;
        self.checkInLabel.alpha = 1.;
        self.checkOutLabel.alpha = 1.;
        self.roomNameButton.alpha = 1.;
        
        self.phoneNumberLabel.alpha = 0.;
        self.mailLabel.alpha = 0.;
        self.commentsLabel.alpha = 0.;
        self.numberOfGuestsLabel.alpha = 0.;
        self.checkInDateLabel.alpha = 0.;
        self.roomNameLabel.alpha = 0.;
        
        self.commentsHeightConstraint.constant = 2. * self.defaultDetailCellHeight;
        self.checkInDateViewConstraint.constant = 2. * self.defaultDetailCellHeight;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.phoneNumberLabel.hidden = YES;
        self.mailLabel.hidden = YES;
        self.commentsLabel.hidden = YES;
        self.numberOfGuestsLabel.hidden = YES;
        self.checkInDateLabel.hidden = YES;
        self.roomNameLabel.hidden = YES;
    }];
}

- (void)prepareForCommit {
    self.phoneNumberLabel.hidden = NO;
    self.mailLabel.hidden = NO;
    self.commentsLabel.hidden = NO;
    self.numberOfGuestsLabel.hidden = NO;
    self.checkInDateLabel.hidden = NO;
    self.roomNameLabel.hidden = NO;
    
    [UIView animateWithDuration:.2 animations:^{
        self.phoneNumberTextField.alpha = 0.;
        self.mailTextField.alpha = 0.;
        self.commentsTextView.alpha = 0.;
        self.numberOfGuestsEditorLabel.alpha = 0.;
        self.numberOfGuestsStepper.alpha = 0.;
        self.checkInButton.alpha = 0.;
        self.checkOutButton.alpha = 0.;
        self.checkInLabel.alpha = 0.;
        self.checkOutLabel.alpha = 0.;
        self.roomNameButton.alpha = 0.;
        
        self.phoneNumberLabel.alpha = 1.;
        self.mailLabel.alpha = 1.;
        self.commentsLabel.alpha = 1.;
        self.numberOfGuestsLabel.alpha = 1.;
        self.checkInDateLabel.alpha = 1.;
        self.roomNameLabel.alpha = 1.;
        
        self.commentsHeightConstraint.constant = self.defaultDetailCellHeight;
        self.checkInDateViewConstraint.constant = self.defaultDetailCellHeight;
        
        [self updateControlls];
    } completion:^(BOOL finished) {
        self.phoneNumberTextField.hidden = YES;
        self.mailTextField.hidden = YES;
        self.commentsTextView.hidden = YES;
        self.numberOfGuestsEditorLabel.hidden = YES;
        self.numberOfGuestsStepper.hidden = YES;
        self.checkInButton.hidden = YES;
        self.checkOutButton.hidden = YES;
        self.checkInLabel.hidden = YES;
        self.checkOutLabel.hidden = YES;
        self.roomNameButton.hidden = YES;
        
        self.clientNameExpandableButton.enabled = YES;
        self.addMoreDetailButton.enabled = YES;
        self.checkInOutButton.enabled = YES;
    }];
}

- (IBAction)editButtonTouched:(id)sender {
    if (!self.isControlsUpdated)
        return;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonTouched:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    if (self.detailsTopConstraint.constant != 0) {
        __block BookingDetailViewController *_self = self;
        [UIView animateWithDuration:.3 animations:^{
            _self.detailsTopConstraint.constant = 0;
            _self.clientNameExpandableButton.downed = NO;
            
            [_self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [_self prepareForEditing];
        }];
    } else {
        [self prepareForEditing];
    }
}

- (void)doneButtonTouched:(id)sender {
    [self.view endEditing:YES];
    
    [self hideDatePicker];
    
    self.pickerType = NBPickerTypeNo;
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = NO;
    
    [self updateGuestInfoWithCompletion];
}

- (IBAction)checkInButtonTouched:(id)sender {
    if (self.datePickerView.hidden) {
        self.datePicker.date = self.checkInDate;
        
        [self showDatePicker];
    } else {
        if (self.pickerType == NBPickerTypeCheckOut) {
            self.checkOutDate = self.datePicker.date;
            
            self.datePicker.date = self.checkInDate;
            
            [self updateDateButtonTitles];
        }
    }
    
    self.pickerType = NBPickerTypeCheckIn;
    self.checkInButton.selected = YES;
    self.checkOutButton.selected = NO;
}

- (IBAction)checkOutButtonTouched:(id)sender {
    if (self.datePickerView.hidden) {
        self.datePicker.date = self.checkOutDate;
        
        [self showDatePicker];
    } else {
        if (self.pickerType == NBPickerTypeCheckIn) {
            self.checkInDate = self.datePicker.date;
            
            self.datePicker.date = self.checkOutDate;
            
            [self updateDateButtonTitles];
        }
    }
    
    self.pickerType = NBPickerTypeCheckOut;
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = YES;
}

- (IBAction)datePickerDoneButtonTouched:(id)sender {
    switch (self.pickerType) {
        case NBPickerTypeCheckIn:
            self.checkInDate = self.datePicker.date;
//TODO:            self.checkOutDate = [self.datePicker.date dateByAddingTimeInterval:24 * 60 * 60];
            break;
            
        case NBPickerTypeCheckOut:
            self.checkOutDate = self.datePicker.date;
            break;
            
        default:
            break;
    }
    
    self.pickerType = NBPickerTypeNo;
    [self updateDateButtonTitles];
    [self hideDatePicker];
    
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = NO;
}

- (IBAction)datePickerCancelButtonTouched:(id)sender {
    [self hideDatePicker];
    
    self.checkInButton.selected = NO;
    self.checkOutButton.selected = NO;
}

- (void)endUpdateGuestInfoWithNewBooking:(KMBNewBooking *)newBooking andName:(NSString *)name {
    NSMutableDictionary *guestFields = [NSMutableDictionary new];
    if (![self.phoneNumberTextField.text isEqualToString:self.bookingInfo.guest.phoneCell]) {
        [guestFields setObject:self.phoneNumberTextField.text forKey:@"phonecell"];
    }
    if (![self.mailTextField.text isEqualToString:self.bookingInfo.guest.email]) {
        [guestFields setObject:self.mailTextField.text forKey:@"email"];
    }
    
    NSMutableDictionary *summaryFields = [NSMutableDictionary new];
    if (![self.commentsTextView.text isEqualToString:self.bookingInfo.bookingSummary.note]) {
        [summaryFields setObject:self.commentsTextView.text forKey:@"note"];
    }
    if (self.numberOfGuestsStepper.currentValue != self.bookingInfo.bookingSummary.numberOfGuests) {
        [summaryFields setObject:@((long)self.numberOfGuestsStepper.currentValue) forKey:@"number_of_guest"];
    }
    if (![self.checkInDate isEqualToDate:self.bookingInfo.bookingSummary.checkIn.toLocalTime]) {
        [summaryFields setObject:[NSString stringWithFormat:@"%@", self.checkInDate.toGlobalTime.description] forKey:@"check_in"];
    }
    if (![self.checkOutDate isEqualToDate:self.bookingInfo.bookingSummary.checkOut.toLocalTime]) {
        [summaryFields setObject:[NSString stringWithFormat:@"%@", self.checkOutDate.toGlobalTime.description] forKey:@"check_out"];
    }
    
    NSMutableArray *rooms = [NSMutableArray new];
    if (!newBooking)
        newBooking = self.selectedRoom;
    
    if (name)
        [self.roomNameButton setTitle:name forState:UIControlStateNormal];
    
    if (newBooking) {
        KMBBookingRoomInfo *currentRoomInfo = [self getRoomFromBooking];
        [rooms addObject:@{
                           @"id":@(currentRoomInfo.identifier),
                           @"delete":@"true",
                           }];
        
        for (KMBAvailabilityRoom *room in newBooking.rooms) {
            [rooms addObject:@{
                               @"price":@(room.price),
                               @"price_type_id":@(room.priceTypeId),
                               @"room_id":@(room.roomId),
                               @"room_type":room.roomType,
                               @"period_type":room.periodType,
                               }];
        }
    }
    
    if (guestFields.count == 0 && summaryFields.count == 0 && rooms.count == 0) {
        [self completionEdit:nil];
        
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[KMBBookings bookings] updateBookingInfoForGuestInfo:guestFields andSummaryInfo:summaryFields andRooms:rooms forBookingWithId:self.bookingId withView:self.view withCompletionBlock:^(id result) {
        [self completionEdit:result];
    } withFailedBlock:^(NSError *error) {
        [self failedEdit:error];
    }];
}

- (void)updateGuestInfoWithCompletion {
    self.isUpdating = YES;
    
    if ((![self.checkInDate isEqualToDate:self.bookingInfo.bookingSummary.checkIn.toLocalTime] ||
        ![self.checkOutDate isEqualToDate:self.bookingInfo.bookingSummary.checkOut.toLocalTime]) &&
        !self.selectedRoom) {
        [self performSegueWithIdentifier:@"showAvailableRoomsBookingDetailsSegue" sender:nil];
    } else {
        [self endUpdateGuestInfoWithNewBooking:nil andName:nil];
    }
}

#pragma mark - Alert view delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        //NO
    } else {
        //YES
        [self checkOut];
    }
}

#pragma mark - Add more booking details delegate methods

- (void)addMoreBookingDetailsDidHidden:(AddMoreBookingDetailsTableViewController *)controller {
    if (controller.hasChanged) {
        self.bookingInfo = controller.bookingInfo;
        [self updateControlls];
    }
}

#pragma mark - Notifications

- (void)keyboardWillShowNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollViewBottomConstraint.constant = size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHideNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollViewBottomConstraint.constant = 0;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)keyboardWillChangeFrameNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollViewBottomConstraint.constant = size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)completionEdit:(id)result {
    if (result != nil) {
        KMBBookingUpdateSuccess *temp = [result firstObject];
        if (temp.booking != nil) {
            self.bookingInfo = temp.booking;
            
            if (![self.bookingInfo.bookingSummary.guestStatus isEqualToString:KMBBSGuestStatusCheckedIn]) {
                self.checkInOutButton.backgroundColor = [UIColor colorFromRed:136 green:192 blue:87 alpha:255];
                [self.checkInOutButton setTitle:@"CHECK IN" forState:UIControlStateNormal];
                self.isCheckedIn = NO;
            } else {
                self.checkInOutButton.backgroundColor = [UIColor colorFromRed:176 green:0 blue:0 alpha:255];
                [self.checkInOutButton setTitle:@"CHECK OUT" forState:UIControlStateNormal];
                self.isCheckedIn = YES;
            }
            
            [[[UIAlertView alloc] initWithTitle:@"Edit booking" message:temp.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            [[SEGAnalytics sharedAnalytics] screen:@"Edit booking" properties: @{}];
        }
    }
    
    [self prepareForCommit];
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editButtonTouched:)];
    self.navigationItem.rightBarButtonItem = editButton;
    
    self.isUpdating = NO;
}

- (void)failedEdit:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:@"Edit booking" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    self.isUpdating = NO;
}

#pragma mark - Booking edit available rooms delegate methods

- (void)bookingEditAvailableRooms:(BookingEditAvailableRoomsTableViewController *)controller didSelectRoomAvailability:(KMBNewBooking *)availabilityRoom andName:(NSString *)name {
    if (self.isRoomShowsFromDone)
        [self endUpdateGuestInfoWithNewBooking:availabilityRoom andName:name];
    else {
        self.selectedRoom = availabilityRoom;
        [self.roomNameButton setTitle:name forState:UIControlStateNormal];
    }
}

@end
