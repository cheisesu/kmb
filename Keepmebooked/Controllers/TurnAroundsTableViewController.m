//
//  TurnAroundsTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "TurnAroundsTableViewController.h"

#import "TurnaroundsTableViewCell.h"
#import "KMBBookingIdRoom.h"
#import "KMBDashboard.h"
#import "MBProgressHUD.h"
#import "KMBTurnaroundInfo.h"
#import "NewBookingViewController.h"
#import "BookingDetailViewController.h"

@interface TurnAroundsTableViewController () <NewBookingDelegate>

@end

@implementation TurnAroundsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Turnarounds" properties: @{}];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadTurnarounds{
    UIView *view = nil;
    if (!self.refreshControl.isRefreshing) {
        view = self.view;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSDate *date = [NSDate date];
    if (self.dashBoardPageIndex == 2) {
        date = [date dateByAddingTimeInterval:24 * 60 * 60];
    }
    __block TurnAroundsTableViewController *_self = self;
    [[KMBDashboard dashboard] loadTurnaroundsForDate:date withView:view withCompletionBlock:^(id result) {
        [self.turnarounds removeAllObjects];
        
        for (KMBTurnaroundInfo *info in result) {
            [self.turnarounds addObject:info];
        }
        [_self.tableView reloadData];
        [_self.refreshControl endRefreshing];
    } withFailedBlock:^(NSError *error) {
        [_self.refreshControl endRefreshing];
    }];
}

- (void)refreshAction {
    [self loadTurnarounds];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.turnarounds.count == 0) {
        return 1;
    }
    return self.turnarounds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.turnarounds.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No turnarounds";
        
        return cell;
    }
    
    TurnaroundsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TurnaroundCell" forIndexPath:indexPath];
    
    KMBTurnaroundInfo *info = self.turnarounds[indexPath.row];
    cell.roomLabel.text = info.name;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"turnAroundsAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
    } /*else if ([segue.identifier isEqualToString:@"showTurnaroundsDetailSegue"]) {
        BookingDetailViewController *dst = segue.destinationViewController;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        KMBTurnaroundInfo *info = self.turnarounds[indexPath.row];
        dst.bookingId = info.bookingId;
        dst.roomName = info.name;
    }*/
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
