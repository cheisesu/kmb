//
//  DashboardViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DashboardViewController.h"

#import "DashboardCollectionViewCell.h"
#import "DashBoardPageCollectionViewController.h"
#import "NewBookingViewController.h"
#import "DashboarSettingsViewController.h"
#import "SearchTableViewController.h"
#import "JMGModaly.h"
#import <Intercom/Intercom.h>

@interface DashboardViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, NewBookingDelegate>

@property UIPageViewController *daysPageViewController;
@property (weak, nonatomic) IBOutlet UIView *daysPageView;

@property NSMutableArray *daysControllers;

@property (nonatomic, strong) JMGModaly *modalSegue;

@end

@implementation DashboardViewController
{
    NSMutableArray *pagesTitles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.daysControllers = [NSMutableArray new];
    pagesTitles = @[@"Today", @"Tomorrow"].mutableCopy;
    
    [self resetController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.daysPageViewController.view.frame = self.daysPageView.frame;
}

- (void)resetController {
    [self.daysControllers removeAllObjects];
    
    self.daysPageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.daysPageViewController.dataSource = self;
    self.daysPageViewController.delegate = self;
    
    DashBoardPageCollectionViewController *pageContentViewController = [self viewControllerAtIndex:0];
    [self.daysControllers addObject:pageContentViewController];
    pageContentViewController = [self viewControllerAtIndex:1];
    [self.daysControllers addObject:pageContentViewController];
    
    [self.daysPageViewController setViewControllers:@[self.daysControllers.firstObject] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    [self addChildViewController:self.daysPageViewController];
    [self.daysPageView addSubview:self.daysPageViewController.view];
    [self.daysPageViewController didMoveToParentViewController:self];
}

- (DashBoardPageCollectionViewController *)viewControllerAtIndex:(NSInteger)index {
    if (index >= pagesTitles.count || index < 0)
        return nil;
    
    DashBoardPageCollectionViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardPageCollectionViewController"];
    [controller loadView];
    
    controller.pageIndex = index;
    controller.pageTitle = pagesTitles[index];
    
//    [controller loadInfo];
    
    return controller;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"dashboardAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"showSearchViewController"]) {
        JMGModaly *popupSegue = (JMGModaly *)segue;
        self.modalSegue = popupSegue;
        
        SearchTableViewController *dst = segue.destinationViewController;
        dst.caller = self;
        
        CGRect destinationBounds = self.view.bounds;
        destinationBounds.size.width -= 40;
        
        dst.view.frame = destinationBounds;
        [dst.view layoutSubviews];
        
    } else if ([segue.identifier isEqualToString:@"showDashboardSettingsSegue"]) {
        JMGModaly *popupSegue = (JMGModaly *)segue;
        self.modalSegue = popupSegue;
        
        CGRect destinationBounds = self.view.bounds;
        destinationBounds.size.width -= 40;
        
        UIViewController *dst = segue.destinationViewController;
        dst.view.frame = destinationBounds;
        [dst.view layoutSubviews];
    }
}

- (IBAction)supportButtonTouched:(id)sender {
    [Intercom presentMessageComposer];
}

- (IBAction)findButtonTouched:(id)sender {
    SearchTableViewController *popup = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchTableViewController"];
    [popup loadView];
    CGRect frame = self.view.frame;
    frame.origin.x = 20;
    frame.size.width -= 40;
    popup.view.frame = frame;
    
    [self presentViewController:popup animated:YES completion:nil];
}

#pragma mark - Page view controller datasource/delegate methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    DashBoardPageCollectionViewController *controller = (DashBoardPageCollectionViewController *)viewController;
    NSInteger index = controller.pageIndex;
    
    if (controller.pageIndex == NSNotFound)
        return nil;
    
    index++;
    if (index >= self.daysControllers.count)
        return nil;
    
    return self.daysControllers[index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    DashBoardPageCollectionViewController *controller = (DashBoardPageCollectionViewController *)viewController;
    NSInteger index = controller.pageIndex;
    
    if (controller.pageIndex == NSNotFound)
        return nil;
    
    index--;
    if (index < 0)
        return nil;
    
    return self.daysControllers[index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray/*<UIViewController *>*/ *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        DashBoardPageCollectionViewController *current = (DashBoardPageCollectionViewController *)pageViewController.viewControllers.firstObject;
        self.navigationItem.title = pagesTitles[current.pageIndex];
    }
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
