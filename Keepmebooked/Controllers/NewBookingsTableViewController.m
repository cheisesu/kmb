//
//  NewBookingsTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 27.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "NewBookingsTableViewController.h"

#import "ChecksTableViewCell.h"
#import "NewBookingViewController.h"
#import "KMBDashboard.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBBookingCheckInOut.h"
#import "KMBCheckInOut.h"
#import "BookingDetailViewController.h"

@interface NewBookingsTableViewController () <UITableViewDataSource, NewBookingDelegate>

@end

@implementation NewBookingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"New bookings" properties:@{}];
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"newBookingsAddSegue"]) {
        UINavigationController *navDst = segue.destinationViewController;
        NewBookingViewController *dstController = navDst.viewControllers.firstObject;
        dstController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"showNewBookingDetailSegue"]) {
        BookingDetailViewController *dst = segue.destinationViewController;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        KMBCheckInOut *info = self.bookings[indexPath.row];
        dst.bookingId = info.bookingId;
        NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
        if (range.length > 0) {
            dst.roomName = [info.text substringToIndex:range.location];
        }
    }
}

- (void)loadNewBookings {
    UIView *view = nil;
    if (!self.refreshControl.isRefreshing) {
        view = self.view;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSDate *date = [NSDate date];
    if (self.dashBoardPageIndex == 2) {
        date = [date dateByAddingTimeInterval:24 * 60 * 60];
    }
    __block NewBookingsTableViewController *_self = self;
    [[KMBDashboard dashboard] loadNewBookingsForDate:date withView:view withCompletionBlock:^(id result) {
        [_self.bookings removeAllObjects];
        for (KMBCheckInOut *checkinout in result) {
            [_self.bookings addObject:checkinout];
        }
        [_self.tableView reloadData];
        [_self.refreshControl endRefreshing];
    } withFailedBlock:^(NSError *error) {
        [_self.refreshControl endRefreshing];
    }];
}

- (void)refreshAction {
    [self loadNewBookings];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.bookings.count == 0) {
        return 1;
    }
    return self.bookings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.bookings.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No new bookings";
        
        return cell;
    }
    
    ChecksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChecksCell" forIndexPath:indexPath];
    
    KMBCheckInOut *info = self.bookings[indexPath.row];
    NSRange range = [info.text rangeOfString:@" - " options:NSBackwardsSearch];
    if (range.length > 0) {
        NSString *room = [info.text substringToIndex:range.location];
        NSString *price = [info.text substringFromIndex:range.location + range.length];
        
        cell.roomLabel.text = room;
        cell.nameLabel.text = price;
    } else {
        cell.roomLabel.text = info.text;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - New booking delegate methods

- (void)newBookingCancel:(NewBookingViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
