//
//  NewBookingViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

#define NBPickerTypeNo 0
#define NBPickerTypeCheckIn 1
#define NBPickerTypeCheckOut 2

@class NewBookingViewController;

@protocol NewBookingDelegate <NSObject>

- (void)newBookingCancel:(NewBookingViewController *)controller;

@end

@interface NewBookingViewController : UIViewController

@property (weak) id<NewBookingDelegate> delegate;

@end
