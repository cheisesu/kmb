//
//  DashboarSettingsViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "DashboarSettingsViewController.h"

#import <MBProgressHUD/MBProgressHUD.h>

#import "KMBUser.h"
#import "AppDelegate.h"
#import "DropDownButton.h"

@interface DashboarSettingsViewController() <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuTableViewTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet DropDownButton *currentHotelButton;

@property BOOL isMenuShown;
@end

@implementation DashboarSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    KMBLoginResponse *userInfo = [KMBUser currentUser].getUserInfo;
    KMBHotel *defaultHotel = [self findHotelById:userInfo.defaultHotelId];
    [self.currentHotelButton setTitle:defaultHotel.name forState:UIControlStateNormal];
    
    self.menuTableView.rowHeight = 47;
    self.menuTableViewHeightConstraint.constant = MIN(4 * self.menuTableView.rowHeight, self.menuTableView.rowHeight * [KMBUser currentUser].getUserInfo.hotels.count);
    self.isMenuShown = NO;
    self.backgroundView.hidden = YES;
    self.backgroundView.alpha = 0;
    self.currentHotelButton.downed = YES;
    self.menuTableViewTopConstraint.constant = -self.menuTableViewHeightConstraint.constant;
    [self.currentHotelButton layoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.layer.cornerRadius = 6;
    [[SEGAnalytics sharedAnalytics] screen:@"Dashboard settings" properties:@{}];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.topView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(3, 3)];
    shapeLayer.path = maskPath.CGPath;
    shapeLayer.frame = self.topView.bounds;
    
    self.topView.layer.mask = shapeLayer;
    [self.topView setNeedsDisplay];
}

- (IBAction)logOutButtonTouched:(UIButton *)sender {
    sender.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block DashboarSettingsViewController *_self = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        __block BOOL isLoggedOut = NO;
        
        if ([[KMBUser currentUser] logOut]) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            isLoggedOut = YES;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:_self.view animated:YES];
            
            if (isLoggedOut) {
                [appDelegate resetToInitialView:YES];
            }
            else {
                sender.enabled = YES;
            }
        });
    });
}

- (KMBHotel *)findHotelById:(NSInteger)hotelId {
    for (KMBHotel *hotel in [KMBUser currentUser].getUserInfo.hotels) {
        if (hotel.hotelId == hotelId)
            return hotel;
    }
    
    return nil;
}

- (void)showMenu {
    self.backgroundView.hidden = NO;
    [UIView animateWithDuration:.3 animations:^{
        self.backgroundView.alpha = .5;
        self.menuTableViewTopConstraint.constant = 0;
        self.currentHotelButton.downed = NO;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.isMenuShown = YES;
    }];
}

- (void)hideMenu {
    [UIView animateWithDuration:.3 animations:^{
        self.backgroundView.alpha = 0;
        self.menuTableViewTopConstraint.constant = -self.menuTableViewHeightConstraint.constant;
        self.currentHotelButton.downed = YES;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.backgroundView.hidden = YES;
        self.isMenuShown = NO;
    }];
}

- (IBAction)hotelButtonTouched:(id)sender {
    if (self.isMenuShown) {
        [self hideMenu];
    } else {
        [self showMenu];
    }
}

- (void)switchToHotel:(NSInteger)hotelId {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __block DashboarSettingsViewController *_self = self;
    [[KMBUser currentUser] changeHotel:hotelId withCompletionBlock:^(id result) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KMBSwitchHotelNotification object:nil];
        KMBSwitchHotel *switchHotel = [result firstObject];
        
        KMBHotel *defaultHotel = [_self findHotelById:switchHotel.defaultHotelId];
        [_self.currentHotelButton setTitle:defaultHotel.name forState:UIControlStateNormal];
        [_self hideMenu];
        [MBProgressHUD hideHUDForView:_self.view animated:YES];
    } andFailBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:_self.view animated:YES];
    }];
}

- (IBAction)tapGestureRecognizer:(UITapGestureRecognizer *)sender {
    [self hideMenu];
}

#pragma mark - Table view delegate/data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [KMBUser currentUser].getUserInfo.hotels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.menuTableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    
    KMBHotel *hotelInfo = [KMBUser currentUser].getUserInfo.hotels[indexPath.row];
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:1];
    nameLabel.text = hotelInfo.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    KMBHotel *hotelInfo = [KMBUser currentUser].getUserInfo.hotels[indexPath.row];
    
    [self switchToHotel:hotelInfo.hotelId];
}

@end
