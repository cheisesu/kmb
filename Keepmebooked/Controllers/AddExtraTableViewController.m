//
//  AddExtraTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 29.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "AddExtraTableViewController.h"
#import "ExtraDetailViewController.h"
#import "KMBOptions.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "KMBUser.h"
#import "KMBExtraInfo.h"
#import "KMBBookings.h"
#import "CheckedTableViewCell.h"

@interface AddExtraTableViewController() <ExtraDetailProtocol>
@property NSMutableArray *extras;
@end

@implementation AddExtraTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self loadExtras];
}

- (void)loadExtras {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.extras = [NSMutableArray new];
    
    __block AddExtraTableViewController *_self = self;
    KMBBookingRoomInfo *roomInfo = self.roomInfo;
    [[KMBOptions sharedOptions] loadExtrasForRoomsIds:@[@(roomInfo.roomId)] withView:self.view withCompletionBlock:^(id result) {
        _self.extras = [NSMutableArray new];
        for (KMBExtraInfo *extraInfo in result) {
            KMBExtraInfo *addExtraInfo = [self findExtraInAddById:extraInfo.object.extra_id];
            KMBExtraInfo *oldExtraInfo = [self findExtraInOldById:extraInfo.object.extra_id];
            KMBExtraInfo *editedExtraInfo = [self findExtraInEditedById:extraInfo.object.extra_id];
            KMBExtraInfo *deleteExtraInfo = [self findExtraInRemoveById:extraInfo.object.extra_id];
            
            if (editedExtraInfo) {
                [_self.extras addObject:editedExtraInfo];
            } else if (addExtraInfo)
                [_self.extras addObject:addExtraInfo];
            else if (deleteExtraInfo)
                [_self.extras addObject:deleteExtraInfo];
            else if (oldExtraInfo)
                [_self.extras addObject:oldExtraInfo];
            else
                [_self.extras addObject:extraInfo];
        }
        [_self.tableView reloadData];
    }];
}

- (void)backButtonTouched {
    if ([self.delegate respondsToSelector:@selector(addExtraDidHidden:)]) {
        [self.delegate addExtraDidHidden:self];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showExtraDetailSegue"]) {
        ExtraDetailViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.bookingInfo = self.bookingInfo;
        
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        KMBExtraInfo *info = self.extras[indexPath.row];
        dst.extraInfo = info;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showExtraDetailSegue"]) {
        UITableViewCell *cell = sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        KMBExtraInfo *extraInfo = self.extras[indexPath.row];
        for (KMBExtraAttributeInfo *attribute in extraInfo.attributes) {
            if ([attribute.name isEqualToString:KMBEANumberOfGuests] ||
                [attribute.name isEqualToString:KMBEANumberOfNights] ||
                [attribute.name isEqualToString:KMBEAPrice]) {
                return YES;
            }
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Inherited methods

- (KMBExtraInfo *)findExtraInOldById:(NSInteger)extraId {
    for (KMBExtraInfo *extraInfo in self.bookingInfo.extras) {
        if (extraInfo.object.extra_id == extraId)
            return extraInfo;
    }
    
    return nil;
}

- (KMBExtraInfo *)findExtraInRemoveById:(NSInteger)extraId {
    for (KMBExtraInfo *extraInfo in self.deletedExtras) {
        if (extraInfo.object.extra_id == extraId)
            return extraInfo;
    }
    
    return nil;
}

- (KMBExtraInfo *)findExtraInAddById:(NSInteger)extraId {
    for (KMBExtraInfo *extraInfo in self.addedExtras) {
        if (extraInfo.object.extra_id == extraId)
            return extraInfo;
    }
    
    return nil;
}

- (KMBExtraInfo *)findExtraInEditedById:(NSInteger)extraId {
    for (KMBExtraInfo *extraInfo in self.editedExtras) {
        if (extraInfo.object.extra_id == extraId)
            return extraInfo;
    }
    
    return nil;
}

#pragma mark - Table view data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.extras.count == 0) {
        return 1;
    }
    return self.extras.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.extras.count == 0) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoResultsCell"];
        
        cell.textLabel.text = @"No extras";
        
        return cell;
    }
    
    CheckedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExtraCell" forIndexPath:indexPath];
    
    KMBExtraInfo *info = self.extras[indexPath.row];
    cell.titleLabel.text = info.object.objectDescription;
    KMBExtraInfo *deleteExtraInfo = [self findExtraInRemoveById:info.object.extra_id];
    KMBExtraInfo *addExtraInfo = [self findExtraInAddById:info.object.extra_id];
    KMBExtraInfo *oldExtraInfo = [self findExtraInOldById:info.object.extra_id];
    KMBExtraInfo *editedExtraInfo = [self findExtraInEditedById:info.object.extra_id];
    cell.checked = !deleteExtraInfo && (addExtraInfo || oldExtraInfo || editedExtraInfo);
    
    return cell;
}

#pragma mark - Table view delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.extras.count == 0)
        return;
    
    CheckedTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    KMBExtraInfo *extraInfo = self.extras[indexPath.row];
    for (KMBExtraAttributeInfo *attribute in extraInfo.attributes) {
        if ([attribute.name isEqualToString:KMBEANumberOfGuests] ||
            [attribute.name isEqualToString:KMBEANumberOfNights] ||
            [attribute.name isEqualToString:KMBEAPrice]) {
            return;
        }
    }
    
    KMBExtraInfo *deleteExtraInfo = [self findExtraInRemoveById:extraInfo.object.extra_id];
    KMBExtraInfo *addExtraInfo = [self findExtraInAddById:extraInfo.object.extra_id];
    KMBExtraInfo *oldExtraInfo = [self findExtraInOldById:extraInfo.object.extra_id];
    if (cell.checked) {//do check = false - mark for deleting
        cell.checked = NO;
        
        if (oldExtraInfo) {
            if (addExtraInfo)
                [self.addedExtras removeObject:addExtraInfo];
            if (!deleteExtraInfo) {
                [self.deletedExtras addObject:oldExtraInfo];
            }
        } else {
            if (addExtraInfo)
                [self.addedExtras removeObject:addExtraInfo];
            if (deleteExtraInfo)
                [self.deletedExtras removeObject:deleteExtraInfo];
        }
    } else { //mark for adding
        cell.checked = YES;
        
        if (oldExtraInfo) {
            if (addExtraInfo)
                [self.addedExtras removeObject:addExtraInfo];
            if (deleteExtraInfo)
                [self.deletedExtras removeObject:deleteExtraInfo];
            
            [self.addedExtras removeObject:oldExtraInfo];
            [self.deletedExtras removeObject:oldExtraInfo];
        } else {
            if (!addExtraInfo) {
                [self.addedExtras addObject:extraInfo];
            }
            if (deleteExtraInfo)
               [self.deletedExtras removeObject:deleteExtraInfo];
        }
    }
}

#pragma mark - Extra detail delegate methods

- (void)extraDetailDone:(ExtraDetailViewController *)controller {
    KMBExtraInfo *extraInfo = controller.extraInfo;
    BOOL mustBeInAdded = NO;
    for (KMBExtraAttributeInfo *info in controller.extraInfo.attributes) {
        if (info.value.floatValue > 0) {
            mustBeInAdded = YES;
            
            break;
        }
    }
    
    KMBExtraInfo *deleteExtraInfo = [self findExtraInRemoveById:extraInfo.object.extra_id];
    KMBExtraInfo *addExtraInfo = [self findExtraInAddById:extraInfo.object.extra_id];
    KMBExtraInfo *oldExtraInfo = [self findExtraInOldById:extraInfo.object.extra_id];
    KMBExtraInfo *editedExtraInfo = [self findExtraInEditedById:extraInfo.object.extra_id];
    
    if (mustBeInAdded) {
        if (oldExtraInfo) {
            if (addExtraInfo)
                [self.addedExtras removeObject:addExtraInfo];
            if (deleteExtraInfo)
                [self.deletedExtras removeObject:deleteExtraInfo];
            if (editedExtraInfo) {
                [self.editedExtras removeObject:editedExtraInfo];
                [self.editedExtras addObject:oldExtraInfo];
            } else {
                [self.editedExtras addObject:oldExtraInfo];
            }
            
            [self.addedExtras removeObject:oldExtraInfo];
            [self.deletedExtras removeObject:oldExtraInfo];
        } else {
            if (!addExtraInfo) {
                [self.addedExtras addObject:extraInfo];
            }
            if (deleteExtraInfo)
                [self.deletedExtras removeObject:deleteExtraInfo];
            if (editedExtraInfo) {
                [self.editedExtras removeObject:editedExtraInfo];
            }
        }
    } else {
        if (oldExtraInfo) {
            if (addExtraInfo)
                [self.addedExtras removeObject:addExtraInfo];
            if (!deleteExtraInfo) {
                [self.deletedExtras addObject:oldExtraInfo];
            }
            if (editedExtraInfo) {
                [self.editedExtras removeObject:editedExtraInfo];
            }
        } else {
            if (addExtraInfo)
                [self.addedExtras removeObject:addExtraInfo];
            if (deleteExtraInfo)
                [self.deletedExtras removeObject:deleteExtraInfo];
            if (editedExtraInfo) {
                [self.editedExtras removeObject:editedExtraInfo];
            }
        }
    }
    
    [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
