//
//  BookingCalendarCollectionViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 14.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "CalendarCollectionViewController.h"
#import <FSCalendar/FSCalendar.h>

@class DropDownButton;

@interface BookingCalendarCollectionViewController : CalendarCollectionViewController <CalendarCollectionViewLayoutDataSource, FSCalendarDelegate, CalendarCollectionViewLayoutDelegate>

@property (weak, nonatomic) IBOutlet DropDownButton *monthSelectButton;

@end
