//
//  BookingContactViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMBAvailability.h"

@class BookingContactViewController;

@protocol BookingContactDelegate <NSObject>

- (void)bookingContactCancel:(BookingContactViewController *)controller;
- (void)bookingContactSave:(BookingContactViewController *)controller andBookingId:(NSInteger)bookingId;
- (void)bookingContactSaveAndView:(BookingContactViewController *)controller andBookingId:(NSInteger)bookingId andRoomName:(NSString *)roomName;

@end

@interface BookingContactViewController : UIViewController

@property (weak) id<BookingContactDelegate> delegate;
@property KMBAvailability *availability;

@end
