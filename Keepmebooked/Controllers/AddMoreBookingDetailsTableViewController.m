//
//  AddMoreBookingDetailsTableViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 18.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "AddMoreBookingDetailsTableViewController.h"

#import "DetailSectionHeaderView.h"
#import "AddExtraTableViewController.h"
#import "AddPaymentViewController.h"
#import "KMBOptions.h"
#import "KMBPaymentInfo.h"
#import "MBProgressHUD.h"
#import "KMBUser.h"
#import "AppDelegate.h"
#import "KMBExtraInfo.h"
#import "KMBBookings.h"
#import "KMBBookingUpdateSuccess.h"

@interface AddMoreBookingDetailsTableViewController () <DetailSectionHeaderViewDelegate, AddExtraDelegate, AddPaymentDelegate>
@property NSMutableArray *deletedExtras;
@property NSMutableArray *addedExtras;
@property NSMutableArray *editedExtras;

@property NSMutableArray *deletedPayments;
@property NSMutableArray *addedPayments;

@property NSMutableArray *extrasArray;
@property NSMutableArray *paymentsArray;
@end

@implementation AddMoreBookingDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.deletedExtras = [NSMutableArray new];
    self.addedExtras = [NSMutableArray new];
    self.editedExtras = [NSMutableArray new];
    self.deletedPayments = [NSMutableArray new];
    self.addedPayments = [NSMutableArray new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DetailSectionHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailSectionHeaderCell"];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self makeExtras];
    [self makePayments];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[SEGAnalytics sharedAnalytics] screen:@"Add more details" properties: @{}];
}

- (void)backButtonTouched {
    self.hasChanged = self.deletedExtras.count || self.addedExtras.count || self.deletedPayments.count || self.addedPayments.count || self.editedExtras.count;
    if (!self.hasChanged) {
        if ([self.delegate respondsToSelector:@selector(addMoreBookingDetailsDidHidden:)]) {
            [self.delegate addMoreBookingDetailsDidHidden:self];
        }
        [self.navigationController popViewControllerAnimated:YES];
        
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    __block AddMoreBookingDetailsTableViewController *_self = self;
    [[KMBBookings bookings] deleteExtras:self.deletedExtras addExtras:self.addedExtras editExtras:self.editedExtras deletePayments:self.deletedPayments addPayments:self.addedPayments forBookingWithId:self.bookingInfo.bookingSummary.identifier withView:self.view withCompletionBlock:^(id result) {
        
        KMBBookingUpdateSuccess *temp = [result firstObject];
        if (temp.booking != nil) {
            _self.bookingInfo = temp.booking;
            if ([_self.delegate respondsToSelector:@selector(addMoreBookingDetailsDidHidden:)]) {
                [_self.delegate addMoreBookingDetailsDidHidden:_self];
            }
        }
        [_self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Inherited methods

- (KMBPaymentInfo *)findPaymentInDeleted:(KMBPaymentInfo *)payment {
    for (KMBPaymentInfo *paymentInfo in self.deletedPayments) {
        if (paymentInfo.identifier == payment.identifier)
            return paymentInfo;
    }
    
    return nil;
}

- (KMBExtraInfo *)findExtraInDeleted:(KMBExtraInfo *)extra {
    for (KMBExtraInfo *extraInfo in self.deletedExtras) {
        if (extraInfo.object.extra_id == extra.object.extra_id)
            return extraInfo;
    }
    
    return nil;
}

- (KMBExtraInfo *)findExtraInEdited:(KMBExtraInfo *)extra {
    for (KMBExtraInfo *extraInfo in self.editedExtras) {
        if (extraInfo.object.extra_id == extra.object.extra_id)
            return extraInfo;
    }
    
    return nil;
}

- (void)makeExtras {
    self.extrasArray = [NSMutableArray new];
    
    for (KMBExtraInfo *extraInfo in self.bookingInfo.extras) {
        KMBExtraInfo *deleted = [self findExtraInDeleted:extraInfo];
        if (!deleted) {
            [self.extrasArray addObject:extraInfo];
        }
    }
    for (KMBExtraInfo *extraInfo in self.addedExtras) {
        [self.extrasArray addObject:extraInfo];
    }
    for (KMBExtraInfo *extraInfo in self.editedExtras) {
        KMBExtraInfo *edited = [self findExtraInEdited:extraInfo];
        if (!edited) {
            [self.extrasArray addObject:extraInfo];
        }
    }
}

- (void)makePayments {
    self.paymentsArray = [NSMutableArray new];
    
    for (KMBPaymentInfo *paymentInfo in self.bookingInfo.payments) {
        KMBPaymentInfo *deleted = [self findPaymentInDeleted:paymentInfo];
        if (!deleted) {
            [self.paymentsArray insertObject:paymentInfo atIndex:0];
        }
    }
    for (KMBPaymentInfo *paymentInfo in self.addedPayments) {
        [self.paymentsArray insertObject:paymentInfo atIndex:0];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return self.extrasArray.count;
            break;
            
        case 1:
            return self.paymentsArray.count;
            break;

            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ExtraCell" forIndexPath:indexPath];
            
            UILabel *title = (UILabel *)[cell viewWithTag:1];
            KMBExtraInfo *extraInfo = self.extrasArray[indexPath.row];
            title.text = extraInfo.object.objectDescription;
            
            break;
        }
            
        case 1: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
            KMBPaymentInfo *paymentInfo = self.paymentsArray[indexPath.row];
            
            UILabel *title = (UILabel *)[cell viewWithTag:1];
            title.text = paymentInfo.paymentDescription;
            
            UILabel *coastLabel = (UILabel *)[cell viewWithTag:2];
            coastLabel.text = [NSString stringWithFormat:@"%@%.2f", self.bookingInfo.currencyCode, paymentInfo.amount];
            
            break;
        }
            
        default:
            break;
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    DetailSectionHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetailSectionHeaderView" owner:self options:nil] firstObject];
    
    NSString *title;
    UIImage *icon;
    switch (section) {
        case 0:
            title = @"Extras";
            icon = [UIImage imageNamed:@"star"];
            view.separator.hidden = YES;
            view.fullSeparator.hidden = self.extrasArray.count == 0;
            break;
            
        case 1:
            title = @"Payment";
            icon = [UIImage imageNamed:@"credit-blue"];
            view.separator.hidden = self.paymentsArray.count == 0;
            view.fullSeparator.hidden = YES;
            break;
            
        default:
            break;
    }
    
    view.titleLabel.text = title;
    view.leftImageView.image = icon;
    view.identifier = section;
    view.delegate = self;
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];

    view.backgroundColor = [UIColor colorFromRed:232 green:232 blue:232 alpha:232];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

#pragma mark - Table view delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        switch (indexPath.section) {
            case 0: {
                KMBExtraInfo *extraInfo = self.extrasArray[indexPath.row];
                NSInteger inOldIndex = [self.bookingInfo.extras indexOfObject:extraInfo];
                NSInteger inAddedIndex = [self.addedExtras indexOfObject:extraInfo];
                NSInteger inEditedIndex = [self.editedExtras indexOfObject:extraInfo];
                if (inOldIndex != NSNotFound) {
                    [self.deletedExtras addObject:extraInfo];
                } else if (inAddedIndex != NSNotFound) {
                    [self.addedExtras removeObject:extraInfo];
                }
                if (inEditedIndex != NSNotFound) {
                    [self.editedExtras removeObject:extraInfo];
                }
                [self.extrasArray removeObject:extraInfo];
                
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                
                break;
            }
                
            case 1: {
                KMBPaymentInfo *paymentInfo = self.paymentsArray[indexPath.row];
                NSInteger inOldIndex = [self.bookingInfo.payments indexOfObject:paymentInfo];
                NSInteger inAddedIndex = [self.addedPayments indexOfObject:paymentInfo];
                if (inOldIndex != NSNotFound) {
                    [self.deletedPayments addObject:paymentInfo];
                } else if (inAddedIndex != NSNotFound) {
                    [self.addedPayments removeObject:paymentInfo];
                }
                [self.paymentsArray removeObject:paymentInfo];
                
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
                
                break;
            }
                
            default:
                break;
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showAddExtraSegue"]) {
        AddExtraTableViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.bookingInfo = self.bookingInfo;
        dst.roomInfo = self.roomInfo;
        dst.addedExtras = self.addedExtras;
        dst.deletedExtras = self.deletedExtras;
        dst.editedExtras = self.editedExtras;
    } else if ([segue.identifier isEqualToString:@"showAddPaymentSegue"]) {
        AddPaymentViewController *dst = segue.destinationViewController;
        dst.delegate = self;
        dst.bookingInfo = self.bookingInfo;
        dst.addedPayments = self.addedPayments;
        dst.deletedPayments = self.deletedPayments;
    }
}


#pragma mark - Detail section header view delegate methods

- (void)addButtonTouchedForDetailSectionHeaderView:(DetailSectionHeaderView *)view {
    if (view.identifier == 0) { //identifier = secion number
        //extra
        [self performSegueWithIdentifier:@"showAddExtraSegue" sender:self];
    } else {
        //payment
        [self performSegueWithIdentifier:@"showAddPaymentSegue" sender:self];
    }
}

#pragma mark - Add extra delegate methods

- (void)addExtraDidHidden:(AddExtraTableViewController *)controller {
    self.deletedExtras = controller.deletedExtras;
    self.addedExtras = controller.addedExtras;
    self.editedExtras = controller.editedExtras;
    [self makeExtras];
    
    [self.tableView reloadData];
}

#pragma mark - Add payment delegate methods

- (void)addPaymentDidAdded:(AddPaymentViewController *)controller {
    self.addedPayments = controller.addedPayments;
    [self makePayments];
    
    [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
