//
//  ExtraDetailViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 29.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "ExtraDetailViewController.h"

#import "ImagedStepper.h"

@interface ExtraDetailViewController() <ImagedStepperDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet ImagedStepper *numberOfGuestsStepper;
@property (weak, nonatomic) IBOutlet ImagedStepper *numberOfNightsStepper;
@property (weak, nonatomic) IBOutlet UIView *numbersView;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@end

@implementation ExtraDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateInfo];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotificationHandler:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotificationHandler:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotificationHandler:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (KMBExtraAttributeInfo *)findAttributeInfoForName:(NSString *)name {
    for (KMBExtraAttributeInfo *attribute in self.extraInfo.attributes) {
        if ([attribute.name isEqualToString:name]) {
            return attribute;
        }
    }
    
    return nil;
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    
}

- (void)updateInfo {
    self.nameLabel.text = self.extraInfo.object.objectDescription;
    KMBExtraAttributeInfo *info = [self findAttributeInfoForName:@"number_of_guest"];
    if (info != nil) {
        self.numberOfGuestsStepper.currentValue = info.value.integerValue;
        self.numberOfNightsStepper.currentValue = [self findAttributeInfoForName:@"number_of_night"].value.integerValue;
        
        self.numbersView.hidden = NO;
        self.priceView.hidden = YES;
        self.isPrice = NO;
    } else {
        info = [self findAttributeInfoForName:@"price"];
        self.priceTextField.text = [NSString stringWithFormat:@"%.2f", info.value.floatValue];
        
        self.numbersView.hidden = YES;
        self.priceView.hidden = NO;
        self.isPrice = YES;
    }
    
    [self.view layoutIfNeeded];
}

- (void)setExtraInfo:(KMBExtraInfo *)extraInfo {
    if (_extraInfo != extraInfo) {
        _extraInfo = extraInfo;
        
        [self updateInfo];
    }
}

#pragma mark - Actions

- (IBAction)doneButtonTouched:(id)sender {
    if (self.isPrice) {
        KMBExtraAttributeInfo *info = [self findAttributeInfoForName:@"price"];
        float value = self.priceTextField.text.floatValue;
        value = MAX(value, 0);
        info.value = @(value);
    } else {
        KMBExtraAttributeInfo *info = [self findAttributeInfoForName:@"number_of_guest"];
        info.value = @(self.numberOfGuestsStepper.currentValue);
        info = [self findAttributeInfoForName:@"number_of_night"];
        info.value = @(self.numberOfNightsStepper.currentValue);
    }
    
    if ([self.delegate respondsToSelector:@selector(extraDetailDone:)]) {
        [self.delegate extraDetailDone:self];
    }
}

#pragma mark - Imaged stepper delegate methods

- (void)imagedStepperDecrement:(ImagedStepper *)imagedStepper {
    
}

- (void)imagedStepperIncrement:(ImagedStepper *)imagedStepper {
    
}

- (void)imagedStepperTitleTouched:(ImagedStepper *)imagedStepper {
    
}

#pragma mark - Text field delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Notifications

- (void)keyboardWillShowNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.bottomConstraint.constant = 16 + size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHideNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.bottomConstraint.constant = 16;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillChangeFrameNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.bottomConstraint.constant = 16 + size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

@end
