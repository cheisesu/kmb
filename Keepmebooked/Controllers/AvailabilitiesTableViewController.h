//
//  AvailabilitiesTableViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 17.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvailabilitiesTableViewController : UITableViewController

@property NSMutableArray *availabilities;
//1 = today;
//2 = tomorrow
@property NSInteger dashBoardPageIndex;

@end
