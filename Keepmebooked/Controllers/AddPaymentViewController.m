//
//  AddPaymentViewController.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 29.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "AddPaymentViewController.h"
#import "MBProgressHUD.h"
#import "KMBBookings.h"
#import "KMBUser.h"
#import "AppDelegate.h"

@interface AddPaymentViewController() <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottomConstraint;
@property (weak, nonatomic) IBOutlet UITextField *amountTextView;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITableView *paymentHistoryTableView;
@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyTableHeightConstraint;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property NSMutableArray *paymentHistory;
@end

@implementation AddPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.paymentHistoryTableView.rowHeight = 44;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotificationHandler:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotificationHandler:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotificationHandler:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTouched)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self makePaymentHistory];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.paymentView.hidden = self.paymentHistory.count == 0;
    self.paymentHistoryTableView.hidden = self.paymentHistory.count == 0;
}

- (void)makePaymentHistory {
    self.paymentHistory = [NSMutableArray new];
    
    for (KMBPaymentInfo *paymentInfo in self.bookingInfo.payments) {
        KMBPaymentInfo *deleted = [self findPaymentInDeleted:paymentInfo];
        if (!deleted) {
            [self.paymentHistory insertObject:paymentInfo atIndex:0];
        }
    }
    for (KMBPaymentInfo *paymentInfo in self.addedPayments) {
        [self.paymentHistory insertObject:paymentInfo atIndex:0];
    }
}

- (void)backButtonTouched {
    [self.navigationController popViewControllerAnimated:YES];
}

- (float)getAmount {
    return [self.amountTextView.text floatValue];
}

- (NSString *)getDescription {
    return self.descriptionTextView.text;
}

#pragma mark - Inherited methods

- (KMBPaymentInfo *)findPaymentInDeleted:(KMBPaymentInfo *)payment {
    NSInteger index = [self.deletedPayments indexOfObject:payment];
    if (index != NSNotFound) {
        return payment;
    }
    
    return nil;
}

#pragma mark - Actions

- (IBAction)addPaymentButtonTouched:(id)sender {
    KMBPaymentInfo *paymentInfo = [KMBPaymentInfo new];
    paymentInfo.paymentDescription = self.descriptionTextView.text;
    paymentInfo.amount = [self.amountTextView.text floatValue];
    [self.addedPayments insertObject:paymentInfo atIndex:0];
    [self.paymentHistory insertObject:paymentInfo atIndex:0];
    [self.paymentHistoryTableView reloadData];
    
    if ([self.delegate respondsToSelector:@selector(addPaymentDidAdded:)]) {
        [self.delegate addPaymentDidAdded:self];
    }
}

- (void)doneButtonTouched:(id)sender {
    [self.view endEditing:YES];
    self.navigationItem.rightBarButtonItem = nil;
}

#pragma mark - Table view data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.paymentHistory.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
    
    KMBPaymentInfo *paymentInfo = self.paymentHistory[indexPath.row];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:1];
    textLabel.text = paymentInfo.paymentDescription;
    UILabel *detailTextLabel = (UILabel *)[cell viewWithTag:2];
    detailTextLabel.text = [NSString stringWithFormat:@"%@%.2f", self.bookingInfo.currencyCode, paymentInfo.amount];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    
    view.backgroundColor = [UIColor colorFromRGB:232 alpha:255];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

#pragma mark - Table view delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y < 0)
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, 0);
}

#pragma mark - Notifications

- (void)keyboardWillShowNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonTouched:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollBottomConstraint.constant = size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHideNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollBottomConstraint.constant = 0;
        
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillChangeFrameNotificationHandler:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize size = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:[info[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.scrollBottomConstraint.constant = size.height - self.tabBarController.tabBar.frame.size.height;
        
        [self.view layoutIfNeeded];
    }];
}

@end
