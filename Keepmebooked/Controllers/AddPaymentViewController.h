//
//  AddPaymentViewController.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 29.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMBPaymentInfo.h"
#import "KMBBookingInfo.h"

@class AddPaymentViewController;

@protocol AddPaymentDelegate <NSObject>

- (void)addPaymentDidAdded:(AddPaymentViewController *)controller;

@end

@interface AddPaymentViewController : UIViewController
@property (weak) id<AddPaymentDelegate> delegate;
@property NSMutableArray *deletedPayments;
@property NSMutableArray *addedPayments;

@property KMBBookingInfo *bookingInfo;

- (float)getAmount;
- (NSString *)getDescription;
@end
