//
//  NSString+Color.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "NSString+Color.h"
#import <UIKit/UIKit.h>

@implementation NSString (Color)

- (UIColor *)asColor {
    NSString *cleanString = [self stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue = 0;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    float r, g, b, a;
    
    r = ((baseValue >> 24) & 0xff) / 255.f;
    g = ((baseValue >> 16) & 0xff) / 255.f;
    b = ((baseValue >> 8) & 0xff) / 255.f;
    a = ((baseValue >> 0) & 0xff) / 255.f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

@end
