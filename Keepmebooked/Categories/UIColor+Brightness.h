//
//  UIColor+Brightness.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Brightness)

- (UIColor *)lighter:(float)value;
- (UIColor *)darker:(float)value;

@end
