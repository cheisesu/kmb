//
//  NSString+Color.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIColor;

@interface NSString (Color)

- (UIColor *)asColor;

@end
