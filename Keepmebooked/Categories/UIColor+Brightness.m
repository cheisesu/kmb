//
//  UIColor+Brightness.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 28.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "UIColor+Brightness.h"

@implementation UIColor (Brightness)

- (UIColor *)lighter:(float)value
{
    CGFloat r, g, b, a;
    if ([self getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MIN(r + r * value, 1.)
                               green:MIN(g + g * value, 1.)
                                blue:MIN(b + b * value, 1.)
                               alpha:a];
    return self;
}

- (UIColor *)darker:(float)value
{
    CGFloat r, g, b, a;
    if ([self getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r - r * value, 0.)
                               green:MAX(g - g * value, 0.)
                                blue:MAX(b - b * value, 0.)
                               alpha:a];
    return self;
}

@end
