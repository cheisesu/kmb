//
//  UIColor+RGBA.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 15.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGBA)
+ (instancetype)colorFromRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha;
+ (instancetype)colorFromRGB:(NSInteger)rgb alpha:(NSInteger)alpha;
@end
