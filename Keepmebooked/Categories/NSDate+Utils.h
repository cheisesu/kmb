//
//  NSDate+Utils.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 10.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

- (NSDate *)toLocalTime;
- (NSDate *)toGlobalTime;
@property (readonly, copy, nonatomic) NSString *description;

@end
