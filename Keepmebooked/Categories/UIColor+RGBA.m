//
//  UIColor+RGBA.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 15.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "UIColor+RGBA.h"

@implementation UIColor (RGBA)

+ (instancetype)colorFromRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha {
    return [UIColor colorWithRed:red / 255. green:green / 255. blue:blue / 255. alpha:alpha / 255.];
}

+ (instancetype)colorFromRGB:(NSInteger)rgb alpha:(NSInteger)alpha {
    return [UIColor colorWithRed:rgb / 255. green:rgb / 255. blue:rgb / 255. alpha:alpha / 255.];
}

@end
