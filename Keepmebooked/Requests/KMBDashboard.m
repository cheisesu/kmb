//
//  KMBDashboard.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBDashboard.h"

#import "KMBRequest.h"
#import "KMBAvailability.h"
#import "KMBStayingTonight.h"
#import "KMBTurnaroundInfo.h"
#import "KMBCheckInOut.h"

@implementation KMBDashboard

+ (instancetype)dashboard {
    static KMBDashboard *dashboard = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dashboard = [[KMBDashboard alloc] init];
    });
    
    return dashboard;
}

+ (void)setupMappings:(RKObjectManager *)om {
    RKObjectMapping *addBookingMapping = [RKObjectMapping mappingForClass:[KMBAddBooking class]];
    [addBookingMapping addAttributeMappingsFromDictionary:@{
                                                            @"number_of_guest":@"numberOfGuests",
                                                            @"start_date":@"startDate",
                                                            @"end_date":@"endDate",
                                                            @"room_id":@"roomId",
                                                            }];
    RKObjectMapping *availabilityMapping = [RKObjectMapping mappingForClass:[KMBAvailability class]];
    [availabilityMapping addAttributeMappingsFromDictionary:@{
                                                              @"text":@"text",
                                                              }];
    [availabilityMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"add_booking" toKeyPath:@"addBooking" withMapping:addBookingMapping]];
    RKResponseDescriptor *availabilityDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:availabilityMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/availabilities?date=:date" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:availabilityDescriptor];
    availabilityDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:availabilityMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/availabilities" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:availabilityDescriptor];
    
//====================
    
    RKObjectMapping *stayingTonightMapping = [RKObjectMapping mappingForClass:[KMBStayingTonight class]];
    [stayingTonightMapping addAttributeMappingsFromDictionary:@{
                                                              @"text":@"text",
                                                              @"booking_id":@"bookingId",
                                                              }];
    RKResponseDescriptor *stayingTonightDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:stayingTonightMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/staying_tonight?date=:date" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:stayingTonightDescriptor];
    stayingTonightDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:stayingTonightMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/staying_tonight" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:stayingTonightDescriptor];
    
//====================
    
    RKObjectMapping *roomInfoMapping = [RKObjectMapping mappingForClass:[KMBBookingRoomInfo class]];
    [roomInfoMapping addAttributeMappingsFromDictionary:@{
                                                          @"room_id":@"roomId",
                                                          @"period_type":@"periodType",
                                                          @"number_of_guest":@"numberOfGuests",
                                                          @"price":@"price",
                                                          @"name":@"name",
                                                          @"id":@"identifier",
                                                          @"room_type":@"roomType",
                                                          }];
    RKObjectMapping *turnaroundsMapping = [RKObjectMapping mappingForClass:[KMBTurnaroundInfo class]];
    [turnaroundsMapping addAttributeMappingsFromDictionary:@{
                                                             @"id":@"roomId",
                                                             @"name":@"name",
                                                             }];
    RKResponseDescriptor *turnaroundsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:turnaroundsMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/turnarounds?date=:date" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:turnaroundsDescriptor];
    turnaroundsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:turnaroundsMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/turnarounds" keyPath:@"rooms" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:turnaroundsDescriptor];
    
    //====================
    
    RKObjectMapping *checkInOutMapping = [RKObjectMapping mappingForClass:[KMBCheckInOut class]];
    [checkInOutMapping addAttributeMappingsFromDictionary:@{
                                                            @"booking_id":@"bookingId",
                                                            @"text":@"text",
                                                            }];
    RKResponseDescriptor *checkInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:checkInOutMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/check_in?date=:date" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:checkInOutDescriptor];
    checkInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:checkInOutMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/check_in" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:checkInOutDescriptor];
    checkInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:checkInOutMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/check_out?date=:date" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:checkInOutDescriptor];
    checkInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:checkInOutMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/check_out" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:checkInOutDescriptor];
    
    //====================
    
    checkInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:checkInOutMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/new_bookings?date=:date" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:checkInOutDescriptor];
    checkInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:checkInOutMapping method:RKRequestMethodAny pathPattern:@"api/dashboards/new_bookings" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:checkInOutDescriptor];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)loadAvailabilitiesForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        [[KMBRequest sharedRequest] GET:@"api/dashboards/availabilities" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
    }
}

- (void)loadStayingTonightForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        [[KMBRequest sharedRequest] GET:@"api/dashboards/staying_tonight" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
    }
}

- (void)loadTurnaroundsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        [[KMBRequest sharedRequest] GET:@"api/dashboards/turnarounds" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
    }
}

- (void)loadCheckInsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        [[KMBRequest sharedRequest] GET:@"api/dashboards/check_in" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
    }
}

- (void)loadCheckOutsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        [[KMBRequest sharedRequest] GET:@"api/dashboards/check_out" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
    }
}

- (void)loadNewBookingsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        [[KMBRequest sharedRequest] GET:@"api/dashboards/new_bookings" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
    }
}

#pragma mark - Sync requests

- (NSArray *)loadStayingTonightForDate:(NSDate *)date error:(NSError **)error {
    if (date) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setValue:date forKey:@"date"];
        
        NSArray *result = [[KMBRequest sharedRequest] GET:@"api/dashboards/staying_tonight" parameters:params error:error];
        
        return result;
    }
    
    return nil;
}

@end
