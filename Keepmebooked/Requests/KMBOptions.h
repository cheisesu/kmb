//
//  KMBOptions.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 24.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "KMBCommon.h"

@interface KMBOptions : NSObject

@property (readonly) NSMutableArray *paymentTypes;
@property (readonly) NSMutableArray *rooms;

+ (instancetype)sharedOptions;

+ (void)setupMappings:(RKObjectManager *)om;

- (void)loadPaymentTypes:(KMBCompletionBlockWithError)completion;
- (void)loadRooms:(KMBCompletionBlockWithError)completion;
- (NSMutableArray *)loadPaymentTypesSync;
- (void)loadExtrasForRoomsIds:(NSArray *)roomsIds withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;

@end
