//
//  KMBRequest.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

#import "KMBCommon.h"

@interface KMBRequest : NSObject

@property (strong, readonly) RKObjectManager *objectManager;

+ (instancetype)sharedRequest;

+ (KMBCompletionBlockWithError)getComletionBlock:(UIView *)view withSuccessCompletionBlock:(KMBCompletionBlock)completion;
+ (KMBCompletionBlockWithError)getComletionBlock:(UIView *)view withSuccessCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;

- (void)addMap:(NSDictionary *)mapDictionary forClass:(Class)class andIdentifier:(NSString *)identifier andPathPattern:(NSString *)pathPattern;
- (BOOL)hasMappingDescriptorForIdentifier:(NSString *)identifier;
- (void)addRelationshipMapping:(NSString *)srcIdentifier fromKey:(NSString *)fromKey toKey:(NSString *)toKey forIdentifier:(NSString *)identifier;
- (BOOL)updateToken;
- (void)GET:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion;
- (void)POST:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion;
- (void)PUT:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion;

- (id)GET:(NSString *)path parameters:(NSDictionary *)parameters error:(NSError **)error;

@end
