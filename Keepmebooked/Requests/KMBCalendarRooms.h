//
//  KMBCalendarRooms.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "KMBCommon.h"

@interface KMBCalendarRooms : NSObject

+ (instancetype)calendarRooms;

+ (void)setupMappings:(RKObjectManager *)om;

- (void)loadRoomsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;

@end
