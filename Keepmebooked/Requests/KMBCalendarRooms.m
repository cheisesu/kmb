//
//  KMBCalendarRooms.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 21.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBCalendarRooms.h"

#import "KMBRequest.h"
#import "KMBCalendarBookingStyle.h"
#import "KMBCalendarBooking.h"
#import "KMBCalendarRoom.h"

@implementation KMBCalendarRooms

+ (instancetype)calendarRooms {
    static KMBCalendarRooms *calendarRooms = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        calendarRooms = [[KMBCalendarRooms alloc] init];
    });
    
    return calendarRooms;
}

+ (void)setupMappings:(RKObjectManager *)om {
    RKObjectMapping *styleMapping = [RKObjectMapping mappingForClass:[KMBCalendarBookingStyle class]];
    [styleMapping addAttributeMappingsFromDictionary:@{
                                                       @"font_color":@"fontColor",
                                                       @"background_color":@"backgroundColor",
                                                       @"border_color":@"borderColor",
                                                       }];
    RKObjectMapping *bookingMapping = [RKObjectMapping mappingForClass:[KMBCalendarBooking class]];
    [bookingMapping addAttributeMappingsFromDictionary:@{
                                                         @"hover":@"hover",
                                                         @"check_out":@"checkOutDate",
                                                         @"label":@"label",
                                                         @"booking_id":@"bookingId",
                                                         @"check_in":@"checkInDate",
                                                         }];
    [bookingMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"style" toKeyPath:@"style" withMapping:styleMapping]];
    RKObjectMapping *roomMapping = [RKObjectMapping mappingForClass:[KMBCalendarRoom class]];
    [roomMapping addAttributeMappingsFromDictionary:@{
                                                      @"name":@"name",
                                                      @"room_order":@"roomOrder",
                                                      @"room_id":@"roomId",
                                                      @"bar_counter":@"barCounter",
                                                      }];
    [roomMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"bar_collection" toKeyPath:@"barCollection" withMapping:bookingMapping]];
    RKResponseDescriptor *roomDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:roomMapping method:RKRequestMethodAny pathPattern:@"api/calendars" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:roomDescriptor];
    roomDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:roomMapping method:RKRequestMethodAny pathPattern:@"api/calendars?start_date=:start" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:roomDescriptor];
    roomDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:roomMapping method:RKRequestMethodAny pathPattern:@"api/calendars?start_date=:start&end_date=:end" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:roomDescriptor];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)loadRoomsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (fromDate && toDate) {
        [params setValue:fromDate forKey:@"start_date"];
        [params setValue:toDate forKey:@"end_date"];
    }
    
    [[KMBRequest sharedRequest] GET:@"api/calendars" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

@end
