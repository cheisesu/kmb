//
//  KMBSearch.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBCommon.h"
#import "RestKit.h"
#import "KMBSearchResult.h"

@interface KMBSearch : NSObject
+ (instancetype)search;
+ (void)setupMappings:(RKObjectManager *)om;

- (KMBSearchResult *)searchByKeyword:(NSString *)searchText;
- (void)searchByKeyword:(NSString *)searchText withCompletionBlock:(KMBCompletionBlock)completion;
- (void)getResultWithPageNumber:(NSInteger)pageNumber forSearchResult:(KMBSearchResult *)searchResult withCompletionBlock:(KMBCompletionBlock)completion;
@end
