//
//  KMBOptions.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 24.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBOptions.h"

#import "KMBRequest.h"
#import "KMBPaymentType.h"
#import "KMBRoomInfo.h"
#import "KMBExtraInfo.h"

@implementation KMBOptions

+ (instancetype)sharedOptions {
    static KMBOptions *sharedOptions = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedOptions = [[KMBOptions alloc] init];
    });
    
    return sharedOptions;
}

+ (void)setupMappings:(RKObjectManager *)om {
    RKObjectMapping *paymentMapping = [RKObjectMapping mappingForClass:[KMBPaymentType class]];
    [paymentMapping addAttributeMappingsFromDictionary:@{
                                                       @"name":@"name",
                                                       }];
    RKResponseDescriptor *paymentDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:paymentMapping method:RKRequestMethodAny pathPattern:@"api/options/payment_types" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:paymentDescriptor];
    
    RKObjectMapping *roomInfoMapping = [RKObjectMapping mappingForClass:[KMBRoomInfo class]];
    [roomInfoMapping addAttributeMappingsFromDictionary:@{
                                                          @"order_number":@"orderNumber",
                                                          @"price":@"price",
                                                          @"room_type":@"roomType",
                                                          @"capacity":@"capacity",
                                                          @"price_type_id":@"priceTypeId",
                                                          @"period_type":@"periodType",
                                                          @"name":@"name",
                                                          @"id":@"identifier",
                                                          }];
    RKResponseDescriptor *roomInfoDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:roomInfoMapping method:RKRequestMethodAny pathPattern:@"api/options/rooms" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:roomInfoDescriptor];

    RKObjectMapping *extraAttributeMapping = [RKObjectMapping mappingForClass:[KMBExtraAttributeInfo class]];
    [extraAttributeMapping addAttributeMappingsFromDictionary:@{
                                                                @"label":@"label",
                                                                @"name":@"name",
                                                                @"value":@"value",
                                                                }];
    RKObjectMapping *extraObjectMapping = [RKObjectMapping mappingForClass:[KMBExtraObjectInfo class]];
    [extraObjectMapping addAttributeMappingsFromDictionary:@{
                                                             @"id":@"extra_id",
                                                             @"description":@"objectDescription",
                                                             }];
    RKObjectMapping *extraMapping = [RKObjectMapping mappingForClass:[KMBExtraInfo class]];
    [extraMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"attributes" toKeyPath:@"attributes" withMapping:extraAttributeMapping]];
    [extraMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"object" toKeyPath:@"object" withMapping:extraObjectMapping]];
    RKResponseDescriptor *extraDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:extraMapping method:RKRequestMethodAny pathPattern:@"api/options/extras" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:extraDescriptor];
}

- (instancetype)init {
    if (self = [super init]) {
        
    }
    
    return self;
}

- (void)loadPaymentTypes:(KMBCompletionBlockWithError)completion {
    __block KMBOptions *_self = self;
    [[KMBRequest sharedRequest] GET:@"api/options/payment_types" parameters:nil completion:^(NSError *error, id result) {
        if (error) {
            
        } else {
            [_self _setPaymentTypes:result];
        }
        
        if (completion) {
            completion(error, _self.paymentTypes);
        }
    }];
}

- (void)loadRooms:(KMBCompletionBlockWithError)completion {
    __block KMBOptions *_self = self;
    [[KMBRequest sharedRequest] GET:@"api/options/rooms" parameters:nil completion:^(NSError *error, id result) {
        if (error) {
            
        } else {
            [_self _setRooms:result];
        }
        
        if (completion) {
            completion(error, _self.rooms);
        }
    }];
}

- (void)loadExtrasForRoomsIds:(NSArray *)roomsIds withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    if (!completion)
        return;
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *rooms = [NSMutableArray new];
    for (NSNumber *roomId in roomsIds) {
        [rooms addObject:@{@"id":roomId}];
    }
    [params setObject:rooms forKey:@"rooms"];
    
    [[KMBRequest sharedRequest] POST:@"api/options/extras" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (NSMutableArray *)loadPaymentTypesSync {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/options/payment_types", KMBHost]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"GET"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    RKObjectRequestOperation *operation = [[KMBRequest sharedRequest].objectManager objectRequestOperationWithRequest:request success:nil failure:nil];
    [operation start];
    [operation waitUntilFinished];
    
    if (operation.error)
        return nil;
    
    _paymentTypes = operation.mappingResult.array[0];
    
    return self.paymentTypes;
}

- (void)_setPaymentTypes:(NSMutableArray *)types {
    if (_paymentTypes != types) {
        _paymentTypes = types;
    }
}

- (void)_setRooms:(NSMutableArray *)rooms {
    if (_rooms != rooms) {
        _rooms = rooms;
    }
}

@end
