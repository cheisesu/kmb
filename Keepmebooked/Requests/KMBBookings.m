//
//  KMBBookings.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBBookings.h"
#import "KMBRequest.h"
#import "KMBBookingInfo.h"
#import "KMBGuestInfo.h"
#import "KMBBookingRoomInfo.h"
#import "KMBBookingSummary.h"
#import "KMBPaymentInfo.h"
#import "KMBBookingCheckInOut.h"
#import "KMBExtraInfo.h"
#import "KMBAddBooking.h"
#import "KMBAvailability.h"
#import "KMBNewBooking.h"
#import "KMBNewBookingSuccess.h"
#import "KMBBookingUpdateSuccess.h"

@implementation KMBBookings

+ (instancetype)bookings {
    static KMBBookings *bookings = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        bookings = [[KMBBookings alloc] init];
    });
    
    return bookings;
}

+ (void)setupMappings:(RKObjectManager *)om {
    RKObjectMapping *extraAttributeMapping = [RKObjectMapping mappingForClass:[KMBExtraAttributeInfo class]];
    [extraAttributeMapping addAttributeMappingsFromDictionary:@{
                                                                @"label":@"label",
                                                                @"name":@"name",
                                                                @"value":@"value",
                                                                }];
    RKObjectMapping *extraObjectMapping = [RKObjectMapping mappingForClass:[KMBExtraObjectInfo class]];
    [extraObjectMapping addAttributeMappingsFromDictionary:@{
                                                             @"id":@"identifier",
                                                             @"description":@"objectDescription",
                                                             @"extra_id":@"extra_id",
                                                             }];
    RKObjectMapping *extraMapping = [RKObjectMapping mappingForClass:[KMBExtraInfo class]];
    [extraMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"attributes" toKeyPath:@"attributes" withMapping:extraAttributeMapping]];
    [extraMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"object" toKeyPath:@"object" withMapping:extraObjectMapping]];
    RKObjectMapping *paymentInfoMapping = [RKObjectMapping mappingForClass:[KMBPaymentInfo class]];
    [paymentInfoMapping addAttributeMappingsFromDictionary:@{
                                                             @"id":@"identifier",
                                                             @"description":@"paymentDescription",
                                                             @"payment_type":@"paymentType",
                                                             @"amount":@"amount",
                                                             }];
    RKObjectMapping *guestInfoMapping = [RKObjectMapping mappingForClass:[KMBGuestInfo class]];
    [guestInfoMapping addAttributeMappingsFromDictionary:@{
                                                           @"email":@"email",
                                                           @"full_name":@"fullName",
                                                           @"phonecell":@"phoneCell",
                                                           }];
    RKObjectMapping *roomInfoMapping = [RKObjectMapping mappingForClass:[KMBBookingRoomInfo class]];
    [roomInfoMapping addAttributeMappingsFromDictionary:@{
                                                           @"room_id":@"roomId",
                                                           @"period_type":@"periodType",
                                                           @"number_of_guest":@"numberOfGuests",
                                                           @"price":@"price",
                                                           @"name":@"name",
                                                           @"id":@"identifier",
                                                           @"room_type":@"roomType",
                                                           }];
    RKObjectMapping *bookingSummaryMapping = [RKObjectMapping mappingForClass:[KMBBookingSummary class]];
    [bookingSummaryMapping addAttributeMappingsFromDictionary:@{
                                                                @"total_cost":@"totalCoast",
                                                                @"check_in":@"checkIn",
                                                                @"note":@"note",
                                                                @"check_out":@"checkOut",
                                                                @"paid":@"paid",
                                                                @"number_of_guest":@"numberOfGuests",
                                                                @"guest_status":@"guestStatus",
                                                                @"balance_due":@"balanceDue",
                                                                @"id":@"identifier",
                                                                @"tax":@"tax",
                                                                @"status":@"status",
                                                                }];
    RKObjectMapping *bookingInfoMapping = [RKObjectMapping mappingForClass:[KMBBookingInfo class]];
    [bookingInfoMapping addAttributeMappingsFromDictionary:@{
                                                             @"currency_code":@"currencyCode",
                                                             @"adjustments":@"adjustments",
                                                             }];
    [bookingInfoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"summary" toKeyPath:@"bookingSummary" withMapping:bookingSummaryMapping]];
    [bookingInfoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"extras" toKeyPath:@"extras" withMapping:extraMapping]];
    [bookingInfoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"payments" toKeyPath:@"payments" withMapping:paymentInfoMapping]];
    [bookingInfoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"guest" toKeyPath:@"guest" withMapping:guestInfoMapping]];
    [bookingInfoMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"rooms" toKeyPath:@"rooms" withMapping:roomInfoMapping]];
    
    RKResponseDescriptor *bookingInfoDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bookingInfoMapping method:RKRequestMethodAny pathPattern:@"api/bookings/:id" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:bookingInfoDescriptor];
    
    
    RKObjectMapping *bookingUpdateMapping = [RKObjectMapping mappingForClass:[KMBBookingUpdateSuccess class]];
    [bookingUpdateMapping addAttributeMappingsFromDictionary:@{
                                                               @"message":@"message"}];
    [bookingUpdateMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"booking" toKeyPath:@"booking" withMapping:bookingInfoMapping]];
    RKResponseDescriptor *bookingUpdateDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bookingUpdateMapping method:RKRequestMethodPUT pathPattern:@"api/bookings/:id" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:bookingUpdateDescriptor];
    
    
    RKObjectMapping *bookingCheckInOutMapping = [RKObjectMapping mappingForClass:[KMBBookingCheckInOut class]];
    [bookingCheckInOutMapping addAttributeMappingsFromDictionary:@{
                                                                   @"guest_status":@"status",
                                                                   @"booking_id":@"bookingId",
                                                                   }];
    RKResponseDescriptor *bookingCheckInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bookingCheckInOutMapping method:RKRequestMethodPOST pathPattern:@"api/dashboards/do_check_out" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:bookingCheckInOutDescriptor];
    bookingCheckInOutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bookingCheckInOutMapping method:RKRequestMethodPOST pathPattern:@"api/dashboards/do_check_in" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:bookingCheckInOutDescriptor];
    
    
    RKObjectMapping *addBookingMapping = [RKObjectMapping mappingForClass:[KMBAddBooking class]];
    [addBookingMapping addAttributeMappingsFromDictionary:@{
                                                            @"number_of_guest":@"numberOfGuests",
                                                            @"start_date":@"startDate",
                                                            @"end_date":@"endDate",
                                                            @"room_id":@"roomId",
                                                            }];
    RKObjectMapping *availabilityMapping = [RKObjectMapping mappingForClass:[KMBAvailability class]];
    [availabilityMapping addAttributeMappingsFromDictionary:@{
                                                              @"text":@"text",
                                                              @"total":@"total",
                                                              }];
    [availabilityMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"add_booking" toKeyPath:@"addBooking" withMapping:addBookingMapping]];
    RKResponseDescriptor *availabilityDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:availabilityMapping method:RKRequestMethodAny pathPattern:@"api/bookings/check_availabilities" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:availabilityDescriptor];
    
    
    RKObjectMapping *chooseBookingMapping = [RKObjectMapping mappingForClass:[KMBAddBooking class]];
    [chooseBookingMapping addAttributeMappingsFromDictionary:@{
                                                               @"number_of_guest":@"numberOfGuests",
                                                               @"check_in":@"startDate",
                                                               @"check_out":@"endDate",
                                                               @"room_id":@"roomId",
                                                               }];
    RKObjectMapping *availabilityRoomMapping = [RKObjectMapping mappingForClass:[KMBAvailabilityRoom class]];
    [availabilityRoomMapping addAttributeMappingsFromDictionary:@{
                                                                  @"price":@"price",
                                                                  @"room_id":@"roomId",
                                                                  @"room_type":@"roomType",
                                                                  @"period_type":@"periodType",
                                                                  @"price_type_id":@"priceTypeId",
                                                                  }];
    RKObjectMapping *chooseAvailabilityMapping = [RKObjectMapping mappingForClass:[KMBNewBooking class]];
    [chooseAvailabilityMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"rooms" toKeyPath:@"rooms" withMapping:availabilityRoomMapping]];
    [chooseAvailabilityMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"booking" toKeyPath:@"booking" withMapping:chooseBookingMapping]];
    RKResponseDescriptor *chooseAvailabilityDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:chooseAvailabilityMapping method:RKRequestMethodAny pathPattern:@"api/bookings/choose_rooms" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:chooseAvailabilityDescriptor];
    
    
    RKObjectMapping *newBookingSuccessMapping = [RKObjectMapping mappingForClass:[KMBNewBookingSuccess class]];
    [newBookingSuccessMapping addAttributeMappingsFromDictionary:@{
                                                                   @"message":@"message",
                                                                   @"booking_id":@"bookingId",
                                                                   }];
    RKResponseDescriptor *newBookingSuccessDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:newBookingSuccessMapping method:RKRequestMethodPOST pathPattern:@"api/bookings" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:newBookingSuccessDescriptor];
    
    
    RKObjectMapping *bookingStatusMapping = [RKObjectMapping mappingForClass:[KMBBookingCheckInOut class]];
    [bookingStatusMapping addAttributeMappingsFromDictionary:@{
                                                               @"status":@"status",
                                                               }];
    RKResponseDescriptor *bookingStatusDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:bookingStatusMapping method:RKRequestMethodGET pathPattern:@"api/bookings/:id/check_status" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:bookingStatusDescriptor];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.unavailableRooms = [NSMutableArray new];
    }
    return self;
}

- (void)loadBookingById:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [[KMBRequest sharedRequest] GET:[NSString stringWithFormat:@"api/bookings/%ld", (long)bookingId] parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}


- (void)checkInBookingsByIds:(NSArray *)bookingsIds withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *bookings = [NSMutableArray new];
    for (NSNumber *number in bookingsIds) {
        [bookings addObject:@{@"id":number}];
    }
    [params setObject:bookings forKey:@"bookings"];
    
    [[KMBRequest sharedRequest] POST:@"api/dashboards/do_check_in" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)checkOutBookingsByIds:(NSArray *)bookingsIds withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *bookings = [NSMutableArray new];
    for (NSNumber *number in bookingsIds) {
        [bookings addObject:@{@"id":number}];
    }
    [params setObject:bookings forKey:@"bookings"];
    
    [[KMBRequest sharedRequest] POST:@"api/dashboards/do_check_out" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)deleteExtras:(NSArray *)deleteExtrasInfos addExtras:(NSArray *)addExtrasInfos forBookingById:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *extras = [NSMutableArray new];
    for (KMBExtraInfo *extraInfo in deleteExtrasInfos) {
        [extras addObject:@{
                            @"id":@(extraInfo.object.identifier),
                            @"delete":@"true",
                            }];
    }
    
    for (KMBExtraInfo *extraInfo in addExtrasInfos) {
        [extras addObject:@{
                            @"extra_id":@(extraInfo.object.extra_id),
                            }];
    }
    [params setObject:extras forKey:@"extras"];
    [params setObject:@{
                        @"continue_empty_room":@"true",
                        }forKey:@"prompt"];
    
    [[KMBRequest sharedRequest] PUT:[NSString stringWithFormat:@"api/bookings/%ld", (long)bookingId] parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)loadAvailableRoomsForNumberOfGuests:(NSInteger)numberOfGuests startDate:(NSDate *)startDate endDate:(NSDate *)endDate withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:@(numberOfGuests) forKey:@"number_of_guest"];
    [params setObject:startDate forKey:@"start_date"];
    [params setObject:endDate forKey:@"end_date"];
    
    [[KMBRequest sharedRequest] GET:@"api/bookings/check_availabilities" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)chooseRoomByAvailability:(KMBAvailability *)availability withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *rooms = [NSMutableArray new];
    [rooms addObject:@{
                       @"number_of_guest":@(availability.addBooking.numberOfGuests),
                       @"room_id":@(availability.addBooking.roomId),
                       @"start_date":[NSString stringWithFormat:@"%@", availability.addBooking.startDate.description],
                       @"end_date":[NSString stringWithFormat:@"%@", availability.addBooking.endDate.description],
                       }];
    [params setObject:rooms forKey:@"rooms"];
    
    NSError *error;
    [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
    
    [[KMBRequest sharedRequest] POST:@"api/bookings/choose_rooms" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)createBooking:(KMBNewBooking *)newBooking withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *rooms = [NSMutableArray new];
    for (KMBAvailabilityRoom *room in newBooking.rooms) {
        [rooms addObject:@{
                           @"price":@(room.price),
                           @"price_type_id":@(room.priceTypeId),
                           @"room_id":@(room.roomId),
                           @"room_type":room.roomType,
                           @"period_type":room.periodType,
                           }];
    }
    [params setObject:rooms forKey:@"rooms"];
    
    NSMutableDictionary *booking = [NSMutableDictionary new];
    
    [booking setObject:@(newBooking.booking.numberOfGuests) forKey:@"number_of_guest"];
    [booking setObject:[NSString stringWithFormat:@"%@", newBooking.booking.startDate.description] forKey:@"check_in"];
    [booking setObject:[NSString stringWithFormat:@"%@", newBooking.booking.endDate.description] forKey:@"check_out"];
    [params setObject:booking forKey:@"booking"];
    
    NSMutableDictionary *guest = [NSMutableDictionary new];
    [guest setObject:newBooking.guest.email forKey:@"email"];
    [guest setObject:newBooking.guest.fullName forKey:@"full_name"];
    [guest setObject:newBooking.guest.phoneCell forKey:@"phonecell"];
    [params setObject:guest forKey:@"guest"];
    
    [[KMBRequest sharedRequest] POST:@"api/bookings" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
}

- (void)deleteExtras:(NSArray *)deleteExtras addExtras:(NSArray *)addExtras editExtras:(NSArray *)editExtras deletePayments:(NSArray *)deletePayments addPayments:(NSArray *)addPayments forBookingWithId:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *extras = [NSMutableArray new];
    for (KMBExtraInfo *extraInfo in deleteExtras) {
        [extras addObject:@{
                            @"id":@(extraInfo.object.identifier),
                            @"delete":@"true",
                            }];
    }
    for (KMBExtraInfo *extraInfo in addExtras) {
        NSMutableDictionary *obj = [NSMutableDictionary new];
        [obj setObject:@(extraInfo.object.extra_id) forKey:@"extra_id"];
        for (KMBExtraAttributeInfo *attribute in extraInfo.attributes) {
            [obj setObject:[NSString stringWithFormat:@"%@", attribute.value] forKey:attribute.name];
        }
        
        [extras addObject:obj];
    }
    for (KMBExtraInfo *extraInfo in editExtras) {
        NSMutableDictionary *obj = [NSMutableDictionary new];
        [obj setObject:@(extraInfo.object.identifier) forKey:@"id"];
        for (KMBExtraAttributeInfo *attribute in extraInfo.attributes) {
            [obj setObject:[NSString stringWithFormat:@"%@", attribute.value] forKey:attribute.name];
        }
        
        [extras addObject:obj];
    }
    if (extras.count)
        [params setObject:extras forKey:@"extras"];
    
    NSMutableArray *payments = [NSMutableArray new];
    for (KMBPaymentInfo *paymentInfo in deletePayments) {
        [payments addObject:@{
                              @"id":@(paymentInfo.identifier),
                              @"delete":@"true",
                              }];
    }
    for (KMBPaymentInfo *paymentInfo in addPayments) {
        [payments addObject:@{
                              @"payment_type":paymentInfo.paymentType ? paymentInfo.paymentType : @"Transfer",
                              @"amount":@(paymentInfo.amount),
                              @"description":paymentInfo.paymentDescription,
                              }];
    }
    if (payments.count)
        [params setObject:payments forKey:@"payments"];
    
    [params setObject:@{
                        @"continue_empty_room":@"true",
                        }forKey:@"prompt"];
    
    [[KMBRequest sharedRequest] PUT:[NSString stringWithFormat:@"api/bookings/%ld", (long)bookingId] parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)loadBookingStatusForBookingWithId:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion {
    [[KMBRequest sharedRequest] GET:[NSString stringWithFormat:@"api/bookings/%ld/check_status", (long)bookingId] parameters:nil completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion]];
}

- (void)updateBookingInfoForGuestInfo:(NSDictionary *)guestInfo andSummaryInfo:(NSDictionary *)summaryInfo andRooms:(NSArray *)rooms forBookingWithId:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:guestInfo forKey:@"guest"];
    [params setObject:summaryInfo forKey:@"summary"];
    [params setObject:rooms forKey:@"rooms"];
    [params setObject:@{
                        @"continue_empty_room":@"true",
                        }forKey:@"prompt"];
    
    [[KMBRequest sharedRequest] PUT:[NSString stringWithFormat:@"api/bookings/%ld", (long)bookingId] parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
}

- (void)blockRooms:(NSArray *)roomsIds forStartDate:(NSDate *)startDate endDate:(NSDate *)endDate withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:roomsIds forKey:@"rooms"];
    [params setObject:@"close" forKey:@"status"];
    [params setObject:@{
                        @"start_date":[NSString stringWithFormat:@"%@", startDate.description],
                        @"end_date":[NSString stringWithFormat:@"%@", endDate.description],
                        }forKey:@"dates"];
    
    [[KMBRequest sharedRequest] POST:@"api/bookings/block_rooms_dates" parameters:params completion:[KMBRequest getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:failed]];
}

@end
