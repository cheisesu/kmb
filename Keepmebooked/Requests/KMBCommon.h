//
//  KMBCommon.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#ifndef KMBCommon_h
#define KMBCommon_h

#import <Foundation/Foundation.h>

typedef void (^KMBCompletionBlockWithError)(NSError *error, id result);
typedef void (^KMBCompletionBlock)(id result);
typedef void (^KMBFailedBlock)(NSError *error);

static NSString *KMBHost = @"http://hotels.keepmebooked.com/";
static NSString *KMBHostUser = @"kmb";
static NSString *KMBHostPassword = @"a5e4aaa5c2a4771cdc73952037c03371144c7321";
//static NSString *KMBHost = @"http://hotelsdev.keepmebooked.com/";
//static NSString *KMBHostUser = @"kmb";
//static NSString *KMBHostPassword = @"st4g1n6";

static const NSInteger KMBRequestErrorNo = 0;
static const NSInteger KMBRequestErrorConnection = -1;
static const NSInteger KMBRequestErrorLogin = -2;
static const NSInteger KMBRequestErrorBackEnd = -3;
static const NSInteger KMBRequestErrorNotLoggedIn = -4;

#endif /* KMBCommon_h */
