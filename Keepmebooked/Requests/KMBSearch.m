//
//  KMBSearch.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBSearch.h"
#import "KMBSearchResult.h"
#import "KMBUser.h"
#import "KMBLoginResponse.h"
#import "KMBRequest.h"
#import "AppDelegate.h"

@implementation KMBSearch

+ (instancetype)search {
    static KMBSearch *search = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        search = [[KMBSearch alloc] init];
    });
    
    return search;
}

+ (void)setupMappings:(RKObjectManager *)om {
    RKObjectMapping *searchPageMapping = [RKObjectMapping mappingForClass:[KMBSearchPage class]];
    [searchPageMapping addAttributeMappingsFromDictionary:@{
                                                            @"url":@"url",
                                                            @"label":@"label",
                                                            }];
    RKObjectMapping *searchInfoMapping = [RKObjectMapping mappingForClass:[KMBSearchInfo class]];
    [searchInfoMapping addAttributeMappingsFromDictionary:@{
                                                            @"guest_email":@"guestEmail",
                                                            @"booking_detail":@"bookingDetail",
                                                            @"status":@"status",
                                                            @"guest_name":@"guestName",
                                                            @"check_in":@"checkInDate",
                                                            @"check_out":@"checkOutDate",
                                                            @"booking_id":@"bookingId",
                                                            }];
    RKObjectMapping *searchResultMapping = [RKObjectMapping mappingForClass:[KMBSearchResult class]];
    [searchResultMapping addAttributeMappingsFromDictionary:@{
                                                              @"found":@"found",
                                                              @"message":@"message",
                                                              @"total_pages":@"totalPages",
                                                              }];
    [searchResultMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pages" toKeyPath:@"pages" withMapping:searchPageMapping]];
    [searchResultMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"result" toKeyPath:@"result" withMapping:searchInfoMapping]];
    RKResponseDescriptor *searchDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:searchResultMapping method:RKRequestMethodGET pathPattern:@"api/bookings/search" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:searchDescriptor];
    searchDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:searchResultMapping method:RKRequestMethodGET pathPattern:@"api/bookings/search?keyword=:word" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:searchDescriptor];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        ;
    }
    return self;
}

- (KMBSearchResult *)searchByKeyword:(NSString *)searchText {
    if (searchText.length < 3)
        return nil;
    
    NSDictionary *params = @{
                             @"keyword":searchText,
                             };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/bookings/search", KMBHost]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"GET"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    [request setValue:[KMBUser currentUser].getUserInfo.token forHTTPHeaderField:@"token"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    RKObjectRequestOperation *operation = [[KMBRequest sharedRequest].objectManager objectRequestOperationWithRequest:request success:nil failure:nil];
    [operation start];
    [operation waitUntilFinished];
    
    if (operation.error) {
        if (operation.error.code == KMBRequestErrorNotLoggedIn) {
            [[KMBUser currentUser] clear];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            dispatch_sync(dispatch_get_main_queue(), ^{
                [appDelegate resetToInitialView:NO];
            });
            
            return nil;
        }
    }
    
    return operation.mappingResult.array[0];
}

- (void)searchByKeyword:(NSString *)searchText withCompletionBlock:(KMBCompletionBlock)completion {
    if (searchText.length < 3) {
        completion(nil);
        
        return;
    }
    
    NSDictionary *params = @{
                             @"keyword":searchText,
                             };
    
    [[KMBRequest sharedRequest] GET:@"api/bookings/search" parameters:params completion:[KMBRequest getComletionBlock:nil withSuccessCompletionBlock:completion]];
}

- (void)getResultWithPageNumber:(NSInteger)pageNumber forSearchResult:(KMBSearchResult *)searchResult withCompletionBlock:(KMBCompletionBlock)completion {
    if (searchResult == nil) {
        completion(nil);
        
        return;
    }
    
    if (pageNumber >= searchResult.totalPages) {
        completion(nil);
        
        return;
    }
    
    NSDictionary *params = @{
                             @"keyword":searchResult.searchText,
                             @"page":@(pageNumber),
                             };
    
    [[KMBRequest sharedRequest] GET:@"api/bookings/search" parameters:params completion:[KMBRequest getComletionBlock:nil withSuccessCompletionBlock:completion]];
}

@end
