//
//  KMBBookings.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "KMBCommon.h"
#import "KMBAvailability.h"
#import "KMBNewBooking.h"

@interface KMBBookings : NSObject

@property NSMutableArray *unavailableRooms;

+ (instancetype)bookings;
+ (void)setupMappings:(RKObjectManager *)om;

- (void)loadBookingById:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)checkInBookingsByIds:(NSArray *)bookingsIds withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)checkOutBookingsByIds:(NSArray *)bookingsIds withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)deleteExtras:(NSArray *)deleteExtrasInfos addExtras:(NSArray *)addExtrasInfos forBookingById:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)loadAvailableRoomsForNumberOfGuests:(NSInteger)numberOfGuests startDate:(NSDate *)startDate endDate:(NSDate *)endDate withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)chooseRoomByAvailability:(KMBAvailability *)availability withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)createBooking:(KMBNewBooking *)newBooking withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;
- (void)deleteExtras:(NSArray *)deleteExtras addExtras:(NSArray *)addExtras editExtras:(NSArray *)editExtras deletePayments:(NSArray *)deletePayments addPayments:(NSArray *)addPayments forBookingWithId:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)loadBookingStatusForBookingWithId:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion;
- (void)updateBookingInfoForGuestInfo:(NSDictionary *)guestInfo andSummaryInfo:(NSDictionary *)summaryInfo andRooms:(NSArray *)rooms forBookingWithId:(NSInteger)bookingId withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;

- (void)blockRooms:(NSArray *)roomsIds forStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;

@end
