//
//  KMBUser.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "KMBLoginResponse.h"
#import "KMBSwitchHotel.h"

#import "KMBCommon.h"

static NSString *KMBSwitchHotelNotification = @"KMBSwitchHotelNotification";

@interface KMBUser : NSObject

+ (instancetype)currentUser;

+ (KMBUser *)logIn:(NSString *)userMail withPassword:(NSString *)password;
+ (void)logIn:(NSString *)userMail withPassword:(NSString *)password withCompletionBlock:(KMBCompletionBlockWithError)completion;

+ (void)setupMappings:(RKObjectManager *)om;

- (BOOL)logOut;
- (void)clear;
- (KMBLoginResponse *)getUserInfo;
- (void)changeHotel:(NSInteger)hotelId withCompletionBlock:(KMBCompletionBlock)completion andFailBlock:(KMBFailedBlock)failed;

@end
