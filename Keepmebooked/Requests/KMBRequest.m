//
//  KMBRequest.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBRequest.h"

#import "KMBCalendarRoom.h"
#import "KMBCalendarBooking.h"
#import "KMBCalendarBookingStyle.h"
#import "KMBLoginResponse.h"
#import "KMBErrorResponse.h"
#import "KMBCalendarRooms.h"
#import "KMBOptions.h"
#import "KMBDashboard.h"
#import "KMBBookings.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "KMBSearch.h"
#import "KMBCommon.h"

#import "KMBUser.h"

@interface KMBRequest()

@property NSMutableDictionary *mappingDescriptions;

@end

@implementation KMBRequest

+ (instancetype)sharedRequest {
    static KMBRequest *sharedRequest = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedRequest = [[KMBRequest alloc] init];
    });
    
    return sharedRequest;
}

+ (KMBCompletionBlockWithError)getComletionBlock:(UIView *)view withSuccessCompletionBlock:(KMBCompletionBlock)completion {
    return [self getComletionBlock:view withSuccessCompletionBlock:completion withFailedBlock:nil];
}

+ (KMBCompletionBlockWithError)getComletionBlock:(UIView *)view withSuccessCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed {
    KMBCompletionBlockWithError block = ^(NSError *error, id result) {
        BOOL isLoggedOut = NO;
        
        [MBProgressHUD hideHUDForView:view animated:YES];
        if (error) {
            if (failed) {
                failed(error);
            }
            
            if (error.code == KMBRequestErrorNotLoggedIn) {
                [[KMBUser currentUser] clear];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                isLoggedOut = YES;
            } else {
                if (!failed) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot load information. Try later or check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
        } else {
            if (completion)
                completion(result);
        }
        
        if (isLoggedOut) {
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            dispatch_sync(dispatch_get_main_queue(), ^{
                [appDelegate resetToInitialView:NO];
            });
        }
    };
    
    return block;
}

- (instancetype)init {
    if (self = [super init]) {
        self.mappingDescriptions = [NSMutableDictionary new];
        
        _objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:[KMBHost copy]]];
        [self.objectManager.HTTPClient setAuthorizationHeaderWithUsername:KMBHostUser password:KMBHostPassword];
        [self.objectManager setRequestSerializationMIMEType:RKMIMETypeJSON];
        [self.objectManager setAcceptHeaderWithMIMEType:RKMIMETypeJSON];
        [self.objectManager.HTTPClient.operationQueue setMaxConcurrentOperationCount:5];
        [RKObjectManager setSharedManager:_objectManager];
        
        [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/html"];
        
        [self setupMappings];
    }
    
    return self;
}

- (void)setupMappings {
    [self addErrorMapping];
    
    [KMBUser setupMappings:self.objectManager];
    [KMBCalendarRooms setupMappings:self.objectManager];
    [KMBOptions setupMappings:self.objectManager];
    [KMBDashboard setupMappings:self.objectManager];
    [KMBBookings setupMappings:self.objectManager];
    [KMBSearch setupMappings:self.objectManager];
}

- (void)addErrorMapping {
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    [errorMapping addAttributeMappingsFromDictionary:@{
                                                       @"message":@"errorMessage",
                                                       }];
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:statusCodes];

    [self.objectManager addResponseDescriptor:errorDescriptor];
    [self.mappingDescriptions setObject:errorDescriptor forKey:@"RKErrorMessage"];
}

- (void)addMap:(NSDictionary *)mapDictionary forClass:(Class)class andIdentifier:(NSString *)identifier andPathPattern:(NSString *)pathPattern {
    if ([self hasMappingDescriptorForIdentifier:identifier])
        return;
    
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[KMBLoginResponse class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"hotels":@"hotels",
                                                  @"email":@"email",
                                                  @"session_expired_at": @"sessionExpiriedAtDate",
                                                  }];
    
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:pathPattern keyPath:nil statusCodes:nil];
    
    [self.objectManager addResponseDescriptor:descriptor];
    [self.mappingDescriptions setObject:descriptor forKey:identifier];
}

- (void)addRelationshipMapping:(NSString *)srcIdentifier fromKey:(NSString *)fromKey toKey:(NSString *)toKey forIdentifier:(NSString *)identifier {
    RKResponseDescriptor *responseDesc = [self.mappingDescriptions objectForKey:identifier];
    RKObjectMapping *responseMapping = (RKObjectMapping *)responseDesc.mapping;
    
    RKResponseDescriptor *srcDesc = [self.mappingDescriptions objectForKey:srcIdentifier];
    RKObjectMapping *srcMapping = (RKObjectMapping *)srcDesc.mapping;
    
    [responseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:fromKey toKeyPath:toKey withMapping:srcMapping]];
}

- (BOOL)hasMappingDescriptorForIdentifier:(NSString *)identifier {
    return [self.mappingDescriptions objectForKey:identifier] != nil;
}

- (BOOL)updateToken {
    NSString *token = [KMBUser currentUser].getUserInfo.token;
    if (token) {
        [self.objectManager.HTTPClient setDefaultHeader:@"token" value:token];
        return YES;
    }
    
    [self.objectManager.HTTPClient setDefaultHeader:@"token" value:nil];
    
    return NO;
}

- (void)GET:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion {
    [self GET:path parameters:parameters completion:completion needLogin:YES];
}

- (void)GET:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion needLogin:(BOOL)needLogin {
    [[KMBRequest sharedRequest].objectManager getObjectsAtPath:path parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSArray *result = mappingResult.array;
        
        NSString *response = [[NSString alloc] initWithData:operation.HTTPRequestOperation.responseData encoding:NSUTF8StringEncoding];
        completion(nil, result);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        //check error
        NSData *body = operation.HTTPRequestOperation.request.HTTPBody;
        NSString *bodyString = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
        NSDictionary *headers = operation.HTTPRequestOperation.request.allHTTPHeaderFields;
        NSError *_error = error;
        if ([error.domain isEqualToString:NSURLErrorDomain]) {
            _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorConnection userInfo:@{
                                                                                               NSLocalizedDescriptionKey:@"Error connect",
                                                                                               }];
            
            completion(_error, nil);
        } else {
            if ([error.localizedDescription rangeOfString:@"login"].length > 0) {
                //login
                KMBLoginResponse *user = [KMBUser currentUser].getUserInfo;
                if (user && needLogin) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        if ([KMBUser logIn:user.email withPassword:user.password]) {
                            //success
                            [[KMBRequest sharedRequest] GET:path parameters:parameters completion:completion needLogin:NO];
                        } else {
                            //error not logged in
                            NSError *newError = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorNotLoggedIn userInfo:@{
                                                                                                                           NSLocalizedDescriptionKey:@"Not logged in",
                                                                                                                           }];
                            completion(newError, nil);
                        }
                    });
                } else {
                    //error not logged in
                    NSError *newError = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorNotLoggedIn userInfo:@{
                                                                                                                   NSLocalizedDescriptionKey:@"Not logged in",
                                                                                                                   }];
                    completion(newError, nil);
                }
            } else {
                _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorBackEnd userInfo:@{
                                                                                                NSLocalizedDescriptionKey:error.localizedDescription,
                                                                                                }];
                
                completion(_error, nil);
            }
        }
    }];
}

- (void)POST:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion {
    [self POST:path parameters:parameters completion:completion needLogin:YES];
}

- (void)POST:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion needLogin:(BOOL)needLogin {
    [[KMBRequest sharedRequest].objectManager postObject:nil path:path parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSArray *result = mappingResult.array;
        
        NSString *response = [[NSString alloc] initWithData:operation.HTTPRequestOperation.responseData encoding:NSUTF8StringEncoding];
        completion(nil, result);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        //check error
        NSData *body = operation.HTTPRequestOperation.request.HTTPBody;
        NSString *bodyString = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
        NSDictionary *headers = operation.HTTPRequestOperation.request.allHTTPHeaderFields;
        NSError *_error = error;
        if ([error.domain isEqualToString:NSURLErrorDomain]) {
            _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorConnection userInfo:@{
                                                                                               NSLocalizedDescriptionKey:@"Error connect",
                                                                                               }];
            
            completion(_error, nil);
        } else {
            if ([error.localizedDescription rangeOfString:@"login"].length > 0) {
                //login
                KMBLoginResponse *user = [KMBUser currentUser].getUserInfo;
                if (user && needLogin) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        if ([KMBUser logIn:user.email withPassword:user.password]) {
                            //success
                            [[KMBRequest sharedRequest] POST:path parameters:parameters completion:completion needLogin:NO];
                        } else {
                            //error not logged in
                            NSError *newError = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorNotLoggedIn userInfo:@{
                                                                                                                           NSLocalizedDescriptionKey:@"Not logged in",
                                                                                                                           }];
                            completion(newError, nil);
                        }
                    });
                } else {
                    //error not logged in
                    NSError *newError = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorNotLoggedIn userInfo:@{
                                                                                                                   NSLocalizedDescriptionKey:@"Not logged in",
                                                                                                                   }];
                    completion(newError, nil);
                }
            } else {
                _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorBackEnd userInfo:@{
                                                                                                NSLocalizedDescriptionKey:error.localizedDescription,
                                                                                                }];
                
                completion(_error, nil);
            }
        }
    }];
}

- (void)PUT:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion {
    [self PUT:path parameters:parameters completion:completion needLogin:YES];
}

- (void)PUT:(NSString *)path parameters:(NSDictionary *)parameters completion:(KMBCompletionBlockWithError)completion needLogin:(BOOL)needLogin {
    [[KMBRequest sharedRequest].objectManager putObject:nil path:path parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSArray *result = mappingResult.array;
        
        completion(nil, result);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSError *_error = error;
        NSData *body = operation.HTTPRequestOperation.request.HTTPBody;
        NSString *bodyString = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
        NSDictionary *headers = operation.HTTPRequestOperation.request.allHTTPHeaderFields;
        
        if ([error.domain isEqualToString:NSURLErrorDomain]) {
            _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorConnection userInfo:@{
                                                                                               NSLocalizedDescriptionKey:@"Error connect",
                                                                                               }];
            
            completion(_error, nil);
        } else {
            if ([error.localizedDescription rangeOfString:@"login"].length > 0) {
                //login
                KMBLoginResponse *user = [KMBUser currentUser].getUserInfo;
                if (user && needLogin) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        if ([KMBUser logIn:user.email withPassword:user.password]) {
                            //success
                            [[KMBRequest sharedRequest] PUT:path parameters:parameters completion:completion needLogin:NO];
                        } else {
                            //error not logged in
                            NSError *newError = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorNotLoggedIn userInfo:@{
                                                                                                                           NSLocalizedDescriptionKey:@"Not logged in",
                                                                                                                           }];
                            completion(newError, nil);
                        }
                    });
                } else {
                    //error not logged in
                    NSError *newError = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorNotLoggedIn userInfo:@{
                                                                                                                   NSLocalizedDescriptionKey:@"Not logged in",
                                                                                                                   }];
                    completion(newError, nil);
                }
            } else {
                _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorBackEnd userInfo:@{
                                                                                                NSLocalizedDescriptionKey:error.localizedDescription,
                                                                                                }];
                
                completion(_error, nil);
            }
        }
    }];
}

- (id)GET:(NSString *)path parameters:(NSDictionary *)parameters error:(NSError **)error {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", KMBHost, path]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"GET"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    RKObjectRequestOperation *operation = [[KMBRequest sharedRequest].objectManager objectRequestOperationWithRequest:request success:nil failure:nil];
    [operation start];
    [operation waitUntilFinished];
    
    *error = operation.error;
    if (operation.error) {
        return nil;
    }
    
    return operation.mappingResult.array;
}

@end
