//
//  KMBDashboard.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "KMBCommon.h"

@interface KMBDashboard : NSObject

+ (instancetype)dashboard;
+ (void)setupMappings:(RKObjectManager *)om;

- (void)loadAvailabilitiesForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;
- (void)loadStayingTonightForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;
- (void)loadTurnaroundsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;
- (void)loadCheckInsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;
- (void)loadCheckOutsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;
- (void)loadNewBookingsForDate:(NSDate *)date withView:(UIView *)view withCompletionBlock:(KMBCompletionBlock)completion withFailedBlock:(KMBFailedBlock)failed;

- (NSArray *)loadStayingTonightForDate:(NSDate *)date error:(NSError **)error;

@end
