//
//  KMBUser.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
// 318c858fa7dd84697f82d7ce05144deedd78415b

#import "KMBUser.h"

#import "KMBLoginResponse.h"
#import "KMBRequest.h"
#import "KMBCommon.h"
#import "KMBOptions.h"
#import "KMBLogout.h"

#import <RestKit/RestKit.h>
#import <Intercom/Intercom.h>
#import <Analytics/Analytics.h>

@interface KMBUser()

@property (strong, nonatomic) KMBLoginResponse *userInfo;

@end

@implementation KMBUser

static KMBUser *currentUser = nil;

+ (KMBUser *)logIn:(NSString *)userMail withPassword:(NSString *)password {
    if (currentUser) {
        [currentUser logOut];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/sessions/sign_in?login=%@&password=%@", KMBHost, [userMail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]], password]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    RKObjectRequestOperation *operation = [[KMBRequest sharedRequest].objectManager objectRequestOperationWithRequest:request success:nil failure:nil];
    [operation start];
    [operation waitUntilFinished];
    
    if (operation.error)
        return nil;
    
    currentUser = [[KMBUser alloc] init];
    currentUser.userInfo = operation.mappingResult.array[0];
    NSString *token = [operation.HTTPRequestOperation.response.allHeaderFields objectForKey:@"Token"];
    currentUser.userInfo.token = token;
    currentUser.userInfo.password = password.copy;
    
    if (currentUser.userInfo.email) {
        [[NSUserDefaults standardUserDefaults] setObject:currentUser.userInfo.email forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[KMBRequest sharedRequest] updateToken];
    
    [currentUser loadOptions];
    
    NSNumber *numberUserId = [NSNumber numberWithInteger:currentUser.userInfo.userId];
    NSString *userId = numberUserId.stringValue;
    [Intercom registerUserWithUserId:userId];
    [[SEGAnalytics sharedAnalytics] identify:userId traits:@{
                                                             @"name":userId,
                                                             @"email":currentUser.userInfo.email}];
    
    return currentUser;
}

+ (void)logIn:(NSString *)userMail withPassword:(NSString *)password withCompletionBlock:(KMBCompletionBlockWithError)completion {
    if (currentUser) {
        [currentUser logOut];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/sessions/sign_in?login=%@&password=%@", KMBHost, [userMail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]], password]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    RKObjectMapping *hotelMapping = [RKObjectMapping mappingForClass:[KMBHotel class]];
    [hotelMapping addAttributeMappingsFromDictionary:@{
                                                       @"hotel_id":@"hotelId",
                                                       @"name":@"name",
                                                       }];
    RKObjectMapping *loginMapping = [RKObjectMapping mappingForClass:[KMBLoginResponse class]];
    [loginMapping addAttributeMappingsFromDictionary:@{
                                                       @"email":@"email",
                                                       @"session_expired_at":@"sessionExpiriedAtDate",
                                                       @"default_hotel_id":@"defaultHotelId",
                                                       @"user_id":@"userId",
                                                       }];
    [loginMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"hotels" toKeyPath:@"hotels" withMapping:hotelMapping]];
    RKResponseDescriptor *loginDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:loginMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[loginDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSString *token = [operation.HTTPRequestOperation.response.allHeaderFields objectForKey:@"Token"];
        if (!token) {
            if (completion) {
                NSError *error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorLogin userInfo:@{
                                                                                                      NSLocalizedDescriptionKey:@"Error login",
                                                                                                      }];
                completion(error, nil);
            }
        } else {
            KMBLoginResponse *loginResponse = [mappingResult.array firstObject];
            if (!loginResponse) {
                if (completion) {
                    NSError *error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorLogin userInfo:@{
                                                                                                          NSLocalizedDescriptionKey:@"Error login",
                                                                                                          }];
                    completion(error, nil);
                }
            } else if (completion) { //success
                loginResponse.token = token;
                loginResponse.password = password.copy;
                
                currentUser = [[KMBUser alloc] init];
                currentUser.userInfo = loginResponse;
                
                if (currentUser.userInfo.email) {
                    [[NSUserDefaults standardUserDefaults] setObject:currentUser.userInfo.email forKey:@"email"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                [[KMBRequest sharedRequest] updateToken];
                
                [currentUser loadOptions];
                
                NSNumber *numberUserId = [NSNumber numberWithInteger:currentUser.userInfo.userId];
                NSString *userId = numberUserId.stringValue;
                [Intercom registerUserWithUserId:userId];
                [[SEGAnalytics sharedAnalytics] identify:userId traits:@{
                                                                         @"name":userId,
                                                                         @"email":currentUser.userInfo.email}];
                
                completion(nil, currentUser.getUserInfo);
            }
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (completion) {
            NSError *_error = error;
            
            if ([error.domain isEqualToString:NSURLErrorDomain]) {
                _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorConnection userInfo:@{
                                                                                                   NSLocalizedDescriptionKey:@"Error connect",
                                                                                                   }];
            } else {
                _error = [NSError errorWithDomain:@"KMB" code:KMBRequestErrorLogin userInfo:@{
                                                                                              NSLocalizedDescriptionKey:@"Error login",
                                                                                              }];
            }
            completion(_error, nil);
        }
    }];
    [operation start];
}

+ (instancetype)currentUser {
    if (currentUser.userInfo && [currentUser.userInfo.sessionExpiriedAtDate timeIntervalSinceNow] <= 0) {
        currentUser = nil;
    }
    
    return currentUser;
}

+ (void)setupMappings:(RKObjectManager *)om {
    RKObjectMapping *hotelMapping = [RKObjectMapping mappingForClass:[KMBHotel class]];
    [hotelMapping addAttributeMappingsFromDictionary:@{
                                                       @"hotel_id":@"hotelId",
                                                       @"name":@"name",
                                                       }];
    RKObjectMapping *loginMapping = [RKObjectMapping mappingForClass:[KMBLoginResponse class]];
    [loginMapping addAttributeMappingsFromDictionary:@{
                                                       @"email":@"email",
                                                       @"session_expired_at": @"sessionExpiriedAtDate",
                                                       @"default_hotel_id":@"defaultHotelId",
                                                       @"user_id":@"userId",
                                                       }];
    [loginMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"hotels" toKeyPath:@"hotels" withMapping:hotelMapping]];
    RKResponseDescriptor *loginDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:loginMapping method:RKRequestMethodAny pathPattern:@"api/sessions/sign_in?login=:login&password=:password" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:loginDescriptor];
    
    RKObjectMapping *logoutMapping = [RKObjectMapping mappingForClass:[KMBLogout class]];
    [logoutMapping addAttributeMappingsFromDictionary:@{
                                                        @"message":@"message",
                                                        }];
    RKResponseDescriptor *logoutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:logoutMapping method:RKRequestMethodAny pathPattern:@"api/sessions/sign_out" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:logoutDescriptor];
    
    RKObjectMapping *switchHotelMapping = [RKObjectMapping mappingForClass:[KMBSwitchHotel class]];
    [switchHotelMapping addAttributeMappingsFromDictionary:@{
                                                             @"message":@"message",
                                                             @"default_hotel_id":@"defaultHotelId",
                                                             }];
    RKResponseDescriptor *switchHotelDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:switchHotelMapping method:RKRequestMethodAny pathPattern:@"api/users/switch_hotel?hotel_id=:id" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [om addResponseDescriptor:switchHotelDescriptor];
}

- (BOOL)logOut {
    if (!currentUser.userInfo.token) {
        currentUser = nil;
        
        return true;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/sessions/sign_out", KMBHost]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    [request setValue:currentUser.userInfo.token forHTTPHeaderField:@"token"];
    
    RKObjectRequestOperation *operation = [[KMBRequest sharedRequest].objectManager objectRequestOperationWithRequest:request success:nil failure:nil];
    [operation start];
    [operation waitUntilFinished];
    
    if (operation.error)
        return false;
    
    currentUser = nil;
    
    [Intercom reset];
    
    return true;
}

- (void)clear {
    currentUser = nil;
}

- (KMBLoginResponse *)getUserInfo {
    return _userInfo.copy;
}

- (void)loadOptions {
    [[KMBOptions sharedOptions] loadPaymentTypes:^(NSError *error, id result) {
        ;
    }];
    
    [[KMBOptions sharedOptions] loadRooms:^(NSError *error, id result) {
        ;
    }];
}

- (void)changeHotel:(NSInteger)hotelId withCompletionBlock:(KMBCompletionBlock)completion andFailBlock:(KMBFailedBlock)failed{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/users/switch_hotel?hotel_id=%ld", KMBHost, (long)hotelId]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    //setting http auth params
    NSString *authString = [NSString stringWithFormat:@"%@:%@", KMBHostUser, KMBHostPassword];
    NSData *authData = [authString dataUsingEncoding:NSASCIIStringEncoding];
    authString = [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    authString = [NSString stringWithFormat:@"Basic %@", authString];
    [request setValue:authString forHTTPHeaderField:@"Authorization"];
    
    [request setValue:currentUser.userInfo.token forHTTPHeaderField:@"token"];
    
    RKObjectMapping *switchHotelMapping = [RKObjectMapping mappingForClass:[KMBSwitchHotel class]];
    [switchHotelMapping addAttributeMappingsFromDictionary:@{
                                                             @"message":@"message",
                                                             @"default_hotel_id":@"defaultHotelId",
                                                             }];
    RKResponseDescriptor *switchHotelDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:switchHotelMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[switchHotelDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        KMBSwitchHotel *switchHotel = operation.mappingResult.array[0];
        _userInfo.defaultHotelId = switchHotel.defaultHotelId;
        
        if (completion)
            completion(operation.mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (failed)
            failed(error);
    }];
    [operation start];
}

@end
