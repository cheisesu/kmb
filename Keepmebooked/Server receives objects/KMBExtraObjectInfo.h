//
//  KMBExtraObjectInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBExtraObjectInfo : NSObject
@property NSInteger identifier;
@property NSInteger extra_id;
@property NSString *objectDescription;
@end
