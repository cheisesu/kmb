//
//  KMBSearchResult.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBSearchPage.h"
#import "KMBSearchInfo.h"

@interface KMBSearchResult : NSObject
@property NSArray *pages; //<KMBSearchPage *>
@property NSMutableArray *result; //<KMBSearchInfo *>
@property NSInteger found;
@property NSString *message;
@property NSInteger totalPages;
@property NSString *searchText;
@end
