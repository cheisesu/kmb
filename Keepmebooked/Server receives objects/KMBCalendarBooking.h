//
//  KMBCalendarBooking.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KMBCalendarBookingStyle;

@interface KMBCalendarBooking : NSObject

@property NSString *hover;
@property NSDate *checkOutDate;
@property NSDate *checkInDate;
@property NSString *label;
@property NSInteger bookingId;

@property KMBCalendarBookingStyle *style;

@end
