//
//  KMBLoginResponse.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBHotel.h"

@interface KMBLoginResponse : NSObject

@property NSArray *hotels; //<KMBHotel *>
@property NSString *email;
@property NSString *password;
@property NSDate *sessionExpiriedAtDate;
@property NSString *token;
@property NSInteger defaultHotelId;
@property NSInteger userId;
@end
