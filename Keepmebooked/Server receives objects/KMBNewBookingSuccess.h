//
//  KMBNewBookingSuccess.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 02.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBNewBookingSuccess : NSObject
@property NSString *message;
@property NSInteger bookingId;
@end
