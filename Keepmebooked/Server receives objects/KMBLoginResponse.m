//
//  KMBLoginResponse.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBLoginResponse.h"

@implementation KMBLoginResponse

- (id)copy {
    KMBLoginResponse *result = [KMBLoginResponse new];
    result.hotels = self.hotels.copy;
    result.email = self.email.copy;
    result.password = self.password.copy;
    result.sessionExpiriedAtDate = self.sessionExpiriedAtDate.copy;
    result.token = self.token.copy;
    result.defaultHotelId = self.defaultHotelId;
    
    return result;
}

@end
