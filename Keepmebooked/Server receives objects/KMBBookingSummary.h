//
//  KMBBookingSummary.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *KMBBSGuestStatusCheckedIn = @"checked_in";
static NSString *KMBBSGuestStatusCheckedOut = @"checked_out";

static NSString *KMBBSBookingStatusProvisional = @"provisional";
static NSString *KMBBSBookingStatusConfirmed = @"confirmed";
static NSString *KMBBSBookingStatusFullyPaid = @"fully_paid";
static NSString *KMBBSBookingStatusCancelled = @"cancelled";

@interface KMBBookingSummary : NSObject
@property float totalCoast;
@property NSDate *checkIn;
@property NSString *note;
@property NSDate *checkOut;
@property float paid;
@property NSString *status;
@property NSInteger numberOfGuests;
@property NSString *guestStatus;
@property float balanceDue;
@property NSInteger identifier;
@property float tax;
@end
