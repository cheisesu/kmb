//
//  KMBGuestInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBGuestInfo : NSObject
@property NSString *email;
@property NSString *fullName;
@property NSString *phoneCell;
@end
