//
//  KMBAvailability.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBAvailability.h"

@implementation KMBAvailability
- (id)copy {
    KMBAvailability *result = [KMBAvailability new];
    
    result.text = self.text.copy;
    result.addBooking = self.addBooking.copy;
    
    return result;
}
@end
