//
//  KMBCalendarRoom.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KMBCalendarBooking;

@interface KMBCalendarRoom : NSObject

@property NSString *name;
@property NSInteger roomOrder;
@property NSInteger roomId;
@property NSInteger barCounter;

@property NSMutableArray *barCollection; //<KMBCalendarBooking *>

@end
