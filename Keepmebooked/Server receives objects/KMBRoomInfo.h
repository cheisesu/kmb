//
//  KMBRoomInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 24.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBRoomInfo : NSObject

@property NSInteger orderNumber;
@property float price;
@property NSString *roomType;
@property NSInteger capacity;
@property NSInteger priceTypeId;
@property NSString *periodType;
@property NSString *name;
@property NSInteger identifier;

@end
