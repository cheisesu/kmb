//
//  KMBNewBooking.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 01.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBAvailabilityRoom.h"
#import "KMBAddBooking.h"
#import "KMBGuestInfo.h"

@interface KMBNewBooking : NSObject
@property NSMutableArray *rooms; //<KMBAvailabilityRoom *>
@property KMBAddBooking *booking;
@property KMBGuestInfo *guest;
@end
