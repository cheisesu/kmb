//
//  KMBBookingInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBGuestInfo.h"
#import "KMBBookingRoomInfo.h"
#import "KMBBookingSummary.h"

@interface KMBBookingInfo : NSObject
@property NSMutableArray *extras;
@property NSString *currencyCode;
@property NSMutableArray *payments;
@property NSMutableArray *adjustments;
@property KMBGuestInfo *guest;
@property NSMutableArray *rooms;
@property KMBBookingSummary *bookingSummary;
@end
