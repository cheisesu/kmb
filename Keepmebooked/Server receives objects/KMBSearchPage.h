//
//  KMBSearchPage.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBSearchPage : NSObject
@property NSURL *url;
@property NSString *label;
@end
