//
//  KMBBookingStyle.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBCalendarBookingStyle.h"
#import "NSString+Color.h"

@implementation KMBCalendarBookingStyle

- (UIColor *)fontUIColor {
    return [self.fontColor asColor];
}

- (UIColor *)backgroundUIColor {
    return [self.backgroundColor asColor];
}

- (UIColor *)borderUIColor {
    return [self.borderColor asColor];
}

@end
