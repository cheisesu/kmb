//
//  KMBAvailabilityRoom.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 01.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBAvailabilityRoom : NSObject
@property float price;
@property NSInteger roomId;
@property NSString *roomType;
@property NSString *periodType;
@property NSInteger priceTypeId;
@end
