//
//  KMBSearchInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 03.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBSearchInfo : NSObject
@property NSString *guestEmail;
@property NSDictionary *bookingDetail;
@property NSString *status;
@property NSString *guestName;
@property NSDate *checkInDate;
@property NSDate *checkOutDate;
@property NSInteger bookingId;
@end
