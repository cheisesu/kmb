//
//  KMBBookingRoomInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBBookingRoomInfo : NSObject
@property NSInteger roomId;
@property NSString *periodType;
@property NSInteger numberOfGuests;
@property float price;
@property NSString *name;
@property NSInteger identifier;
@property NSString *roomType;
@end
