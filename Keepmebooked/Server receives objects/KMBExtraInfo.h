//
//  KMBExtraInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBExtraAttributeInfo.h"
#import "KMBExtraObjectInfo.h"

@interface KMBExtraInfo : NSObject
@property NSMutableArray *attributes; //<KMBExtraAttributeInfo *>
@property KMBExtraObjectInfo *object;
@end
