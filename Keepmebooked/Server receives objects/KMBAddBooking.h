//
//  KMBAddBooking.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBAddBooking : NSObject
@property NSInteger roomId;
@property NSInteger numberOfGuests;
@property NSDate *startDate;
@property NSDate *endDate;
@end
