//
//  KMBCalendarBookingStyle.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KMBCalendarBookingStyle : NSObject

@property NSString *fontColor;
@property NSString *backgroundColor;
@property NSString *borderColor;

- (UIColor *)fontUIColor;
- (UIColor *)backgroundUIColor;
- (UIColor *)borderUIColor;

@end
