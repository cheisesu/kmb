//
//  KMBTurnaroundInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 07.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBBookingRoomInfo.h"

@interface KMBTurnaroundInfo : NSObject
@property NSInteger roomId;
@property NSString *name;
@end
