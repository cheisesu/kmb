//
//  KMBStayingTonight.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBStayingTonight : NSObject
@property NSString *text;
@property NSInteger bookingId;
@end
