//
//  KMBAddBooking.m
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import "KMBAddBooking.h"

@implementation KMBAddBooking

- (id)copy {
    KMBAddBooking *result = [KMBAddBooking new];
    
    result.roomId = self.roomId;
    result.numberOfGuests = self.numberOfGuests;
    result.startDate = self.startDate.copy;
    result.endDate = self.endDate.copy;
    
    return result;
}

@end
