//
//  KMBSwitchHotel.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 10.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBSwitchHotel : NSObject
@property NSString *message;
@property NSInteger defaultHotelId;
@end
