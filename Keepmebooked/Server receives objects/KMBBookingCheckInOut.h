//
//  KMBBookingCheckInOut.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 01.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBBookingCheckInOut : NSObject
@property NSString *status;
@property NSInteger bookingId;
@end
