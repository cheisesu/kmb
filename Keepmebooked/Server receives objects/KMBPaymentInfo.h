//
//  KMBPaymentInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBPaymentInfo : NSObject
@property NSInteger identifier;
@property float amount;
@property NSString *paymentDescription;
@property NSString *paymentType;
@end
