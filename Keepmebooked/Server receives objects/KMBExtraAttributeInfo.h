//
//  KMBExtraAttributeInfo.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *KMBEANumberOfGuests = @"number_of_guest";
static NSString *KMBEANumberOfNights = @"number_of_night";
static NSString *KMBEAPrice = @"price";

@interface KMBExtraAttributeInfo : NSObject
@property NSString *label;
@property NSNumber *value;
@property NSString *name;
@end
