//
//  KMBPaymentType.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 24.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBPaymentType : NSObject

@property NSString *name;

@end
