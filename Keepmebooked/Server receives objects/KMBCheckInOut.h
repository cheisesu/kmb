//
//  KMBCheckInOut.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 07.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBCheckInOut : NSObject
@property NSInteger bookingId;
@property NSString *text;
@end