//
//  KMBErrorResponse.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 20.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBErrorResponse : NSObject

@property NSString *message;
@property NSDictionary *errors;

@end
