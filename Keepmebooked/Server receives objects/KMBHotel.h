//
//  KMBHotel.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 10.09.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMBHotel : NSObject
@property NSString *name;
@property NSInteger hotelId;
@end
