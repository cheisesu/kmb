//
//  KMBAvailability.h
//  Keepmebooked
//
//  Created by Shelonin Dmitry on 31.08.15.
//  Copyright © 2015 S Media Link. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KMBAddBooking.h"

@interface KMBAvailability : NSObject
@property NSString *text;
@property NSString *total;
@property KMBAddBooking *addBooking;
@end
